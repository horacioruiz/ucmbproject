'use strict'
app
        .controller('listComponenteCtrl', ['$state', '$scope', 'config', 'ComponenteService', '$cookies', '$base64', 'TipoComponenteService', 'MarcaService', '$http', '$rootScope', function ($state, $scope, config, ComponenteService, $cookies, $base64, TipoComponenteService, MarcaService, $http, $rootScope) {
//                console.log("HOLAAA");

                $scope.componentes = {};
                $scope.componenteAgregar = {};
                $scope.componenteModificar = {};
                $scope.componenteAgregar.tipoComponente = {};
                $scope.componenteModificar.tipoComponente = {};
                $scope.componenteAgregar.marca = {};
                $scope.componenteModificar.marca = {};
                $scope.aModificar = "";

                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };

                var myMap = new Map();
                var myMape = new Map();

                $scope.listar = function () {
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    ComponenteService.fetch(config.cantPaginacion, offSet).success(function (data) {
                        $scope.componentes = data;
                        $scope.paginar();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                //PAGINATION

                $scope.paginar = function () {
                    ComponenteService.count().success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $rootScope.$on("$locationChangeStart", function (event, data) {
                    var pag = parseInt(data.replace("http://" + config.ipFront + ":8017/menu.html#/:pagina=", ""));
                    if (!isNaN(pag)) {
                        $("#paginaActual").val(pag);
                        $scope.buscarSegunPagina();
                    }
                });

                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.tipo === "" || $scope.tipo === undefined) {
                        $scope.tip = null;
                    } else {
                        $scope.tip = $scope.tipo;
                    }
                    if ($scope.marca === "" || $scope.marca === undefined) {
                        $scope.marc = null;
                    } else {
                        $scope.marc = $scope.marca;
                    }
                    ComponenteService.countFiltro($scope.filt, $scope.marc, $scope.tip).success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
//                    $("#paginacionesComponente").html('<ul class="pagination"><li class="disabled"><a href="#">&laquo;</a></li><li class="active"><a href="#">1</a></li><li ><a href="#">2</a></li><li><a href="#">&raquo;</a></li></ul>');
                };

                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.tipo === "" || $scope.tipo === undefined) {
                        $scope.tip = null;
                    } else {
                        $scope.tip = $scope.tipo;
                    }
                    if ($scope.marca === "" || $scope.marca === undefined) {
                        $scope.marc = null;
                    } else {
                        $scope.marc = $scope.marca;
                    }
                    $http.get(config.backend + '/componente/generarReporte/' + $scope.filt + '/' + $scope.tip + '/' + $scope.marc + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                };

                $scope.listarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.tipo === "" || $scope.tipo === undefined) {
                        $scope.tip = null;
                    } else {
                        $scope.tip = $scope.tipo;
                    }
                    if ($scope.marca === "" || $scope.marca === undefined) {
                        $scope.marc = null;
                    } else {
                        $scope.marc = $scope.marca;
                    }
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    ComponenteService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt, $scope.marc, $scope.tip).success(function (data) {
                        $scope.componentes = data;
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.buscar = function () {
                    $scope.componentes = [];
                    location.href = "http://" + config.ipFront + ":8017/menu.html#/:pagina=1";
                    if ($scope.filterText !== "" || $scope.tipo !== "" || $scope.marca !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
//                    }
                };

                $scope.buscarSegunPagina = function () {
                    $scope.componentes = [];
                    if ($scope.filterText !== "" || $scope.tipo !== "" || $scope.marca !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };

                //FIN DE PAGINACION

                $scope.registrar = function () {
                    $scope.componenteAgregar.tipoComponente = {};
                    $scope.componenteAgregar.marca = {};
                    if ($scope.componenteAgregar.descriComponente === "" || $scope.componenteAgregar.descriComponente === undefined || $scope.componenteAgregar.descriComponente === null) {
                        swal("Mensaje del Sistema!", "Los datos no deben quedar vacío", "error");
                    } else {
                        $scope.componenteAgregar.tipoComponente.idTipoComp = myMap.get($scope.idTipoComp);
                        $scope.componenteAgregar.marca.idMarca = myMape.get($scope.idMarca);

                        $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                        $scope.componenteAgregar.usuAltaComp = $scope.jsonUsu.usuarioUsuario;
                        $scope.componenteAgregar.usuModComp = $scope.jsonUsu.usuarioUsuario;

                        ComponenteService.crear($scope.componenteAgregar).success(function (data) {
                            if (data === true) {
                                swal("Mensaje del Sistema!", "Datos Agregados exitosamente", "success");
                                $('#modalAgregar').modal('hide');
                                $scope.listar();
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Agregados, verique que los campos no estén vacíos", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };
                $scope.atras = function () {
                    location.reload();
                };

                //SEPARADOR DE MILES

                //FIN DE SEPARADOR DE MILES


                $scope.actualizar = function () {
//                    $scope.tarifaModificar.andeCategoria.idAndeCat = myMap.get($scope.idAndeCat);
//                    $scope.componenteModificar.tipoComponente.idTipoComp = {};
//                    $scope.componenteModificar.marca = {};
                    if ($scope.componenteModificar.descriComponente === "" || $scope.componenteModificar.descriComponente === undefined || $scope.componenteModificar.descriComponente === null) {
                        swal("Mensaje del Sistema!", "Los datos no deben quedar vacío", "error");
                    } else {
//                        $scope.componenteModificar.tipoComponente.idTipoComp = myMap.get($scope.idTipoComp);
//                        $scope.componenteModificar.marca.idMarca = myMape.get($scope.idMarca);

                        $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                        $scope.componenteModificar.usuModComp = $scope.jsonUsu.usuarioUsuario;
//                    $scope.tarifaModificar.tarifaGsKwh = respuesta;
                        ComponenteService.actualizar($scope.componenteModificar).success(function (data) {
                            if (data === true) {
                                swal("Mensaje del Sistema!", "Datos Modificados exitosamente", "success");
                                $('#modalModificar').modal('hide');
//                            $scope.componenteModificar = {};
                                $scope.listar();
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Modificados, verifique que los campos no estén vacíos", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };

                $scope.agregar = function () {

                    $scope.componenteAgregar = {};
                    $scope.idTipoComp = "";
                    $scope.idMarca = "";

                    //PERSONA FILTRO
                    $scope.nestedItemsLevel1 = [];
                    $scope.nestedItemsLevel2 = [];
//                    for (var i = 1; i <= 5; i++) {
//                        $scope.nestedItemsLevel1.push('Nested ' + i);
//                    }
//                    
//                    $scope.usuarioAgregar.persona.onSelect($scope.nestedItemsLevel1[0]);

                    TipoComponenteService.listar().success(function (d) {
                        angular.forEach(d, function (value, key) {
//                            $scope.nestedItemsLevel1.push(value.nombrePersona + " " + value.apellidoPersona + "-" + value.idPersona);
                            $scope.nestedItemsLevel1.push(value.descriTipoComp);
                            myMap.set(value.descriTipoComp, value.idTipoComp);
//                            console.log('-> ' + value.nombrePersona + " " + value.apellidoPersona);
                        });

                        //                    $scope.nestedItemsLevel1 = ['Item 1', 'Item 2', 'Item 3'];
//                        $scope.usuarioAgregar.persona.onSelect($scope.nestedItemsLevel1[0]);
                    });

                    MarcaService.listar().success(function (d) {

                        angular.forEach(d, function (value, key) {
//                            $scope.nestedItemsLevel1.push(value.nombrePersona + " " + value.apellidoPersona + "-" + value.idPersona);
                            $scope.nestedItemsLevel2.push(value.descriMarca);
                            myMape.set(value.descriMarca, value.idMarca);
//                            console.log('-> ' + value.nombrePersona + " " + value.apellidoPersona);
                        });
                        //                    $scope.nestedItemsLevel1 = ['Item 1', 'Item 2', 'Item 3'];
//                        $scope.usuarioAgregar.persona.onSelect($scope.nestedItemsLevel1[0]);
                    });

                };
                $scope.limpiar = function () {
                    $scope.filterText = "";
                    $scope.marca = "";
                    $scope.tipo = "";
                };
                $scope.modificar = function (componentes) {
                    $scope.componenteModificar.idComponente = componentes.idComponente;
                    $scope.componenteModificar.descriComponente = componentes.descriComponente;

                    TipoComponenteService.listar().success(function (d) {
                        $scope.nestedItemsLevel1 = d;
                        $scope.componenteModificar.tipoComponente = componentes.tipoComponente;
                    });
                    MarcaService.listar().success(function (d) {
                        $scope.nestedItemsLevel2 = d;
                        $scope.componenteModificar.marca = componentes.marca;
                    });


                    $scope.componenteModificar.estadoComp = componentes.estadoComp;
                    $("#modalModificar").modal("show");
                };

                $scope.eliminar = function (marc) {
                    $scope.datoEliminar = {};
                    $scope.datoEliminar.idComponente = marc.idComponente;
                    $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                    $scope.datoEliminar.usuModComp = $scope.jsonUsu.usuarioUsuario;
                    swal({
                        title: "Mensaje del Sistema",
                        text: "Seguro que quiere eliminar " + marc.descriComponente + "?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    ComponenteService.eliminar(marc.idComponente).success(function (data) {
//                                    ComponenteService.modEliminar($scope.datoEliminar).success(function (data) {
                                        if (data === true) {
                                            swal("Mensaje del Sistema!", "Datos Eliminados exitosamente", "success");
//                                $('#modalModificar').modal('hide');
                                            $scope.datoEliminar = {};
                                            $scope.listar();
                                        } else {
                                            swal("Mensaje del Sistema!", "No se pudo eliminar el Componente, verifique Dependencias", "error");
                                        }
                                    }).error(function (e) {
                                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                    });
                                } else {
                                    swal("Mensaje del Sistema", "Haz cancelado la operación!", "error");
                                }
                            });
                };
                $scope.$watch('filterText', function () {
                    $("#paginaActual").val("1");
                })
                $scope.$watch('tipo', function () {
                    $("#paginaActual").val("1");
                })
                $scope.$watch('marca', function () {
                    $("#paginaActual").val("1");
                })
                $scope.buscar();
            }]);