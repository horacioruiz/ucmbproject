'use strict'
app
        .controller('listMateriasCtrl', ['$state', '$scope', 'config', 'MateriaService', '$cookies', '$base64', "$filter", '$http', '$rootScope', function ($state, $scope, config, MateriaService, $cookies, $base64, $filter, $http, $rootScope) {
//                console.log("HOLAAA");

                $scope.materias = {};
                var mapCategorias = new Map();
                var mapCategoriasJSON = new Map();
                var mapCategoriasParent = new Map();
                var mapCate1 = new Map();
                var mapCate2 = new Map();
                var mapCate3 = new Map();
                var mapCate4 = new Map();
                var mapCate5 = new Map();

                var mapMatriculaciones = new Map();

                var mapUsuarioEnrol = new Map();
                var mapCateMod2 = new Map();
                var mapCateMod3 = new Map();
                var mapCateMod4 = new Map();
                var mapCateMod5 = new Map();
                $scope.personaAgregar = {};
                $scope.usuariosEnrol = [];
                $scope.enrol = {};
                $scope.personaModificar = {};

                $scope.nombreUsuario = "";

                $scope.people = [];

                $scope.salirData = function () {
                    $cookies.put("estado", null);
                    $cookies.putObject("token", null);
                    $cookies.putObject("facultad", null);
                    $cookies.putObject("idusuario", null);
                    $cookies.putObject("usuario", null);
                    $cookies.putObject("detalles", null);
                    $cookies.putObject("periodo", null);
                    location.href = "../#";
                };

                $scope.agregarPeriodo = function () {
                    var txt;
                    var person = prompt("Ingrese descripcion del Periodo", "");
                    if (person == null || person == "") {
                        txt = "Haz cancelado";
                    } else {
                        $scope.ex = {};
                        $scope.ex.fecha = null;
                        $scope.ex.descripcion = person;

                        MateriaService.cargarPeriodo($scope.ex).success(function (data) {
                            swal("Mensaje del Sistema!", "Periodo registrado correctamente!.", "success");
                            $scope.salirData();
                        }).error(function (e) {
//                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };

                $scope.listarUltimos = function () {
                    MateriaService.recuperarPeriodo().success(function (y) {
                        var mensaje1 = "";
                        var n = 0;
                        angular.forEach(y, function (value, key) {
                            var today = new Date(value.fecha);
                            var dd = today.getDate();

                            var mm = today.getMonth() + 1;
                            var yyyy = today.getFullYear();
                            if (dd < 10)
                            {
                                dd = '0' + dd;
                            }

                            if (mm < 10)
                            {
                                mm = '0' + mm;
                            }
                            today = dd + '-' + mm + '-' + yyyy;
                            mensaje1 += '\n* ' + value.descripcion + " fecha: " + today + "\n";



                        });
                        alert(mensaje1)
                    }).error(function (e) {
                        //swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                if ($cookies.getObject("estado") === null || $cookies.getObject("estado") === undefined) {
                    $scope.salirData();
                } else {
                    $scope.categoriaFilter = $cookies.getObject("facultad");
                    MateriaService.listaDeUsuariosPorUsername($cookies.getObject("usuario")).success(function (data) {
                        $scope.nombreUsuario = data[0].fullname;
                        $cookies.putObject("idusuario", data[0].id);
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }


                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };

                $("#primeraCategoria").change(function () {
                    $("#cateTwo").css("display", "none");
                    $("#cateTwoS").css("display", "none");
                    $scope.cate2 = [];
                    mapCate2 = new Map();
                    $scope.idCate2 = "";

                    $("#cateThree").css("display", "none");
                    $("#cateThreeS").css("display", "none");
                    $scope.cate3 = [];
                    mapCate3 = new Map();
                    $scope.idCate3 = "";

                    $("#cateFour").css("display", "none");
                    $("#cateFourS").css("display", "none");
                    $scope.cate4 = [];
                    mapCate4 = new Map();
                    $scope.idCate4 = "";

                    $("#cateFive").css("display", "none");
                    $("#cateFiveS").css("display", "none");
                    $scope.cate5 = [];
                    mapCate5 = new Map();
                    $scope.idCate5 = "";


                    MateriaService.listarCategoriasFilter($("#primeraCategoria").val()).success(function (data) {
                        $scope.cate2 = [];
                        angular.forEach(data, function (value, key) {

                            if (parseInt(value.parent) === parseInt($("#primeraCategoria").val())) {
                                $scope.cate2.push(value.name);
                                mapCate2.set(value.name, value.id);
                            }
                        });
                        $("#cateTwo").css("display", "inline");
                        $("#cateTwoS").css("display", "inline");

                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                });

                $("#primeraCategoriaMod").change(function () {
                    $("#cateTwoMod").css("display", "none");
                    $("#cateTwoSMod").css("display", "none");
                    $scope.cate2Mod = [];
                    mapCateMod2 = new Map();
                    $scope.idCate2Mod = "";

                    $("#cateThreeMod").css("display", "none");
                    $("#cateThreeSMod").css("display", "none");
                    $scope.cate3Mod = [];
                    mapCateMod3 = new Map();
                    $scope.idCate3Mod = "";

                    $("#cateFourMod").css("display", "none");
                    $("#cateFourSMod").css("display", "none");
                    $scope.cate4Mod = [];
                    mapCateMod4 = new Map();
                    $scope.idCate4Mod = "";

                    $("#cateFiveMod").css("display", "none");
                    $("#cateFiveSMod").css("display", "none");
                    $scope.cate5Mod = [];
                    mapCateMod5 = new Map();
                    $scope.idCate5Mod = "";


                    MateriaService.listarCategoriasFilter($("#primeraCategoriaMod").val()).success(function (data) {
                        $scope.cate2Mod = [];
                        angular.forEach(data, function (value, key) {

                            if (parseInt(value.parent) === parseInt($("#primeraCategoriaMod").val())) {
                                $scope.cate2Mod.push(value.name);
                                mapCateMod2.set(value.name, value.id);
                            }
                        });
                        $("#cateTwoMod").css("display", "inline");
                        $("#cateTwoSMod").css("display", "inline");

                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                });

                $scope.segundaCategoria = function () {
                    $("#cateThree").css("display", "none");
                    $("#cateThreeS").css("display", "none");
                    $scope.cate3 = [];
                    mapCate3 = new Map();
                    $scope.idCate3 = "";

                    $("#cateFour").css("display", "none");
                    $("#cateFourS").css("display", "none");
                    $scope.cate4 = [];
                    mapCate4 = new Map();
                    $scope.idCate4 = "";

                    $("#cateFive").css("display", "none");
                    $("#cateFiveS").css("display", "none");
                    $scope.cate5 = [];
                    mapCate5 = new Map();
                    $scope.idCate5 = "";

                    MateriaService.listarCategoriasFilter(mapCate2.get($scope.idCate2)).success(function (data) {
                        $scope.cate3 = [];
                        angular.forEach(data, function (value, key) {

                            if (parseInt(value.parent) === parseInt(mapCate2.get($scope.idCate2))) {
                                $scope.cate3.push(value.name);
                                mapCate3.set(value.name, value.id);
                            }
                        });
                        $("#cateThree").css("display", "inline");
                        $("#cateThreeS").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.segundaCategoriaMod = function () {
                    $("#cateThreeMod").css("display", "none");
                    $("#cateThreeSMod").css("display", "none");
                    $scope.cate3Mod = [];
                    mapCateMod3 = new Map();
                    $scope.idCate3Mod = "";

                    $("#cateFourMod").css("display", "none");
                    $("#cateFourSMod").css("display", "none");
                    $scope.cate4Mod = [];
                    mapCateMod4 = new Map();
                    $scope.idCate4Mod = "";

                    $("#cateFiveMod").css("display", "none");
                    $("#cateFiveSMod").css("display", "none");
                    $scope.cate5Mod = [];
                    mapCateMod5 = new Map();
                    $scope.idCate5Mod = "";

                    MateriaService.listarCategoriasFilter(mapCateMod2.get($scope.idCate2Mod)).success(function (data) {
                        $scope.cate3Mod = [];
                        angular.forEach(data, function (value, key) {

                            if (parseInt(value.parent) === parseInt(mapCateMod2.get($scope.idCate2Mod))) {
                                $scope.cate3Mod.push(value.name);
                                mapCateMod3.set(value.name, value.id);
                            }
                        });
                        $("#cateThreeMod").css("display", "inline");
                        $("#cateThreeSMod").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.terceraCategoria = function () {
                    $("#cateFour").css("display", "none");
                    $("#cateFourS").css("display", "none");
                    $scope.cate4 = [];
                    mapCate4 = new Map();
                    $scope.idCate4 = "";

                    $("#cateFive").css("display", "none");
                    $("#cateFiveS").css("display", "none");
                    $scope.cate5 = [];
                    mapCate5 = new Map();
                    $scope.idCate5 = "";

                    console.log(mapCate3.get($scope.idCate3));
                    MateriaService.listarCategoriasFilter(mapCate3.get($scope.idCate3)).success(function (data) {
                        $scope.cate4 = [];
                        angular.forEach(data, function (value, key) {

                            if (parseInt(value.parent) === parseInt(mapCate3.get($scope.idCate3)) && value.visible === 1) {
                                $scope.cate4.push(value.name);
                                mapCate4.set(value.name, value.id);
                            }
                            $("#cateFour").css("display", "inline");
                            $("#cateFourS").css("display", "inline");
                        });

                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.terceraCategoriaMod = function () {
                    $("#cateFourMod").css("display", "none");
                    $("#cateFourSMod").css("display", "none");
                    $scope.cate4Mod = [];
                    mapCateMod4 = new Map();
                    $scope.idCate4Mod = "";

                    $("#cateFiveMod").css("display", "none");
                    $("#cateFiveSMod").css("display", "none");
                    $scope.cate5Mod = [];
                    mapCateMod5 = new Map();
                    $scope.idCate5Mod = "";

                    MateriaService.listarCategoriasFilter(mapCateMod3.get($scope.idCate3Mod)).success(function (data) {
                        $scope.cate4Mod = [];
                        angular.forEach(data, function (value, key) {

                            if (parseInt(value.parent) === parseInt(mapCateMod3.get($scope.idCate3Mod)) && value.visible === 1) {
                                $scope.cate4Mod.push(value.name);
                                mapCateMod4.set(value.name, value.id);
                            }
                            $("#cateFourMod").css("display", "inline");
                            $("#cateFourSMod").css("display", "inline");
                        });

                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.cuartaCategoria = function () {

                    $("#cateFive").css("display", "none");
                    $("#cateFiveS").css("display", "none");
                    $scope.cate5 = [];
                    mapCate5 = new Map();
                    $scope.idCate5 = "";

                    MateriaService.listarCategoriasFilter(mapCate4.get($scope.idCate4)).success(function (data) {
                        $scope.cate5 = [];
                        angular.forEach(data, function (value, key) {

                            if (parseInt(value.parent) === parseInt(mapCate4.get($scope.idCate4)) && value.visible === 1) {
                                $scope.cate5.push(value.name);
                                mapCate5.set(value.name, value.id);
                            }
                            $("#cateFive").css("display", "inline");
                            $("#cateFiveS").css("display", "inline");
                        });

                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.cuartaCategoriaMod = function () {

                    $("#cateFiveMod").css("display", "none");
                    $("#cateFiveSMod").css("display", "none");
                    $scope.cate5Mod = [];
                    mapCateMod5 = new Map();
                    $scope.idCate5Mod = "";

                    MateriaService.listarCategoriasFilter(mapCateMod4.get($scope.idCate4Mod)).success(function (data) {
                        $scope.cate5Mod = [];
                        angular.forEach(data, function (value, key) {

                            if (parseInt(value.parent) === parseInt(mapCateMod4.get($scope.idCate4Mod)) && value.visible === 1) {
                                $scope.cate5Mod.push(value.name);
                                mapCateMod5.set(value.name, value.id);
                            }
                            $("#cateFiveMod").css("display", "inline");
                            $("#cateFiveSMod").css("display", "inline");
                        });

                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.ocultarCate2 = function () {
                    $("#cateTwo").css("display", "none");
                    $("#cateTwoS").css("display", "none");
                    $scope.cate2 = [];
                    mapCate2 = new Map();
                    $scope.idCate2 = "";

                    $("#cateThree").css("display", "none");
                    $("#cateThreeS").css("display", "none");
                    $scope.cate3 = [];
                    mapCate3 = new Map();
                    $scope.idCate3 = "";

                    $("#cateFour").css("display", "none");
                    $("#cateFourS").css("display", "none");
                    $scope.cate4 = [];
                    mapCate4 = new Map();
                    $scope.idCate4 = "";

                    $("#cateFive").css("display", "none");
                    $("#cateFiveS").css("display", "none");
                    $scope.cate5 = [];
                    mapCate5 = new Map();
                    $scope.idCate5 = "";
                }

                $scope.ocultarCate2Mod = function () {
                    $("#cateTwoMod").css("display", "none");
                    $("#cateTwoSMod").css("display", "none");
                    $scope.cate2Mod = [];
                    mapCateMod2 = new Map();
                    $scope.idCate2Mod = "";

                    $("#cateThreeMod").css("display", "none");
                    $("#cateThreeSMod").css("display", "none");
                    $scope.cate3Mod = [];
                    mapCateMod3 = new Map();
                    $scope.idCate3Mod = "";

                    $("#cateFourMod").css("display", "none");
                    $("#cateFourSMod").css("display", "none");
                    $scope.cate4Mod = [];
                    mapCateMod4 = new Map();
                    $scope.idCate4Mod = "";

                    $("#cateFiveMod").css("display", "none");
                    $("#cateFiveSMod").css("display", "none");
                    $scope.cate5Mod = [];
                    mapCateMod5 = new Map();
                    $scope.idCate5Mod = "";
                }

                $scope.ocultarCate3 = function () {
                    $("#cateThree").css("display", "none");
                    $("#cateThreeS").css("display", "none");
                    $scope.cate3 = [];
                    mapCate3 = new Map();
                    $scope.idCate3 = "";

                    $("#cateFour").css("display", "none");
                    $("#cateFourS").css("display", "none");
                    $scope.cate4 = [];
                    mapCate4 = new Map();
                    $scope.idCate4 = "";

                    $("#cateFive").css("display", "none");
                    $("#cateFiveS").css("display", "none");
                    $scope.cate5 = [];
                    mapCate5 = new Map();
                    $scope.idCate5 = "";
                }

                $scope.ocultarCate3Mod = function () {
                    $("#cateThreeMod").css("display", "none");
                    $("#cateThreeSMod").css("display", "none");
                    $scope.cate3Mod = [];
                    mapCateMod3 = new Map();
                    $scope.idCate3Mod = "";

                    $("#cateFourMod").css("display", "none");
                    $("#cateFourSMod").css("display", "none");
                    $scope.cate4Mod = [];
                    mapCateMod4 = new Map();
                    $scope.idCate4Mod = "";

                    $("#cateFiveMod").css("display", "none");
                    $("#cateFiveSMod").css("display", "none");
                    $scope.cate5Mod = [];
                    mapCateMod5 = new Map();
                    $scope.idCate5Mod = "";
                }

                $scope.ocultarCate4 = function () {
                    $("#cateFour").css("display", "none");
                    $("#cateFourS").css("display", "none");
                    $scope.cate4 = [];
                    mapCate4 = new Map();
                    $scope.idCate4 = "";

                    $("#cateFive").css("display", "none");
                    $("#cateFiveS").css("display", "none");
                    $scope.cate5 = [];
                    mapCate5 = new Map();
                    $scope.idCate5 = "";
                }

                $scope.ocultarCate4Mod = function () {
                    $("#cateFourMod").css("display", "none");
                    $("#cateFourSMod").css("display", "none");
                    $scope.cate4Mod = [];
                    mapCateMod4 = new Map();
                    $scope.idCate4Mod = "";

                    $("#cateFiveMod").css("display", "none");
                    $("#cateFiveSMod").css("display", "none");
                    $scope.cate5Mod = [];
                    mapCateMod5 = new Map();
                    $scope.idCate5Mod = "";
                }

                $scope.ocultarCate5 = function () {
                    $("#cateFive").css("display", "none");
                    $("#cateFiveS").css("display", "none");
                    $scope.cate5 = [];
                    mapCate5 = new Map();
                    $scope.idCate5 = "";
                }

                $scope.ocultarCate5Mod = function () {
                    $("#cateFiveMod").css("display", "none");
                    $("#cateFiveSMod").css("display", "none");
                    $scope.cate5Mod = [];
                    mapCateMod5 = new Map();
                    $scope.idCate5Mod = "";
                }


                //               pagination
                $scope.listar = function () {
                    $scope.people = [];
//                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
//                    MateriaService.listar().success(function (data) {
                    MateriaService.traerMatriculacionesPendientes($cookies.getObject("periodo")).success(function (data) {
                        angular.forEach(data, function (value, key) {

                            if (value.estado === true) {
                                value.enrolado = true;
                            } else {
                                value.enrolado = false;
                            }

                            $scope.people.push(value);

                            $scope.nameFilter = '';
                            $scope.ageFilter = '';
                            $scope.stateFilter = '';

                            $scope.itemsPerPage = 20;


                            $scope.currentPage = 1;
                            console.debug($scope.totalItems);
                            console.debug(Math.ceil($scope.people.length / $scope.itemsPerPage));
                            $scope.noOfPages = Math.ceil($scope.people.length / $scope.itemsPerPage);
                            console.debug($scope.noOfPages);
                        });
//                        $scope.paginar();
//                    }).error(function (e) {
//                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
//                    });
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });

                }
                $scope.paginar = function () {
                    MateriaService.count().success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $rootScope.$on("$locationChangeStart", function (event, data) {
                    var pag = parseInt(data.replace("http://" + config.ipFront + ":8017/menu.html#/:pagina=", ""));
                    if (!isNaN(pag)) {
                        $("#paginaActual").val(pag);
                        $scope.buscarSegunPagina();
                    }
                });

                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.apellido === "" || $scope.apellido === undefined) {
                        $scope.ape = null;
                    } else {
                        $scope.ape = $scope.apellido;
                    }
                    if ($scope.ci === "" || $scope.ci === undefined) {
                        $scope.ced = null;
                    } else {
                        $scope.ced = $scope.ci;
                    }
                    MateriaService.countFiltro($scope.filt, $scope.ape, $scope.ced).success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
//                    $("#paginacionesPersonas").html('<ul class="pagination"><li class="disabled"><a href="#">&laquo;</a></li><li class="active"><a href="#">1</a></li><li ><a href="#">2</a></li><li><a href="#">&raquo;</a></li></ul>');
                };

                $scope.listarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.apellido === "" || $scope.apellido === undefined) {
                        $scope.ape = null;
                    } else {
                        $scope.ape = $scope.apellido;
                    }
                    if ($scope.ci === "" || $scope.ci === undefined) {
                        $scope.ced = null;
                    } else {
                        $scope.ced = $scope.ci;
                    }

                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    MateriaService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt, $scope.ape, $scope.ced).success(function (data) {
                        $scope.materias = data;
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.buscar = function () {
                    $scope.materias = [];

                    $scope.listar();

                };

                $scope.buscarSegunPagina = function () {
                    $scope.materias = [];
                    $scope.listar();
                };
//                FIN DE PAGINACION

                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.apellido === "" || $scope.apellido === undefined) {
                        $scope.ape = null;
                    } else {
                        $scope.ape = $scope.apellido;
                    }
                    if ($scope.ci === "" || $scope.ci === undefined) {
                        $scope.ced = null;
                    } else {
                        $scope.ced = $scope.ci;
                    }

                    $http.get(config.backend + '/personas/generarReporte/' + $scope.ced + '/' + $scope.filt + '/' + $scope.ape + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });

                }

                $scope.limpiar = function () {
                    $scope.filterText = "";
                    $scope.apellido = "";
                    $scope.ci = "";
                };

                $scope.cargarNivel2 = function (n2, n3, n4, n5) {
                    $("#cateTwoMod").css("display", "none");
                    $("#cateTwoSMod").css("display", "none");
                    $scope.cate2Mod = [];
                    mapCateMod2 = new Map();
                    $scope.idCate2Mod = "";

                    $("#cateThreeMod").css("display", "none");
                    $("#cateThreeSMod").css("display", "none");
                    $scope.cate3Mod = [];
                    mapCateMod3 = new Map();
                    $scope.idCate3Mod = "";

                    $("#cateFourMod").css("display", "none");
                    $("#cateFourSMod").css("display", "none");
                    $scope.cate4Mod = [];
                    mapCateMod4 = new Map();
                    $scope.idCate4Mod = "";

                    $("#cateFiveMod").css("display", "none");
                    $("#cateFiveSMod").css("display", "none");
                    $scope.cate5Mod = [];
                    mapCateMod5 = new Map();
                    $scope.idCate5Mod = "";


                    MateriaService.listarCategoriasFilter($("#primeraCategoriaMod").val()).success(function (data) {
                        $scope.cate2Mod = [];
                        var num = 0;
                        angular.forEach(data, function (value, key) {
                            num++;
                            if (parseInt(value.parent) === parseInt($("#primeraCategoriaMod").val())) {
                                $scope.cate2Mod.push(value.name);
                                mapCateMod2.set(value.name, value.id);
                            }
                            if (num === data.length) {
                                $scope.idCate2Mod = JSON.parse(JSON.stringify(mapCategoriasJSON.get(n2))).name;

                                if (n3 > 0) {
                                    $scope.cargarNivel3(n3, n4, n5);
                                }
                            }
                        });
                        $("#cateTwoMod").css("display", "inline");
                        $("#cateTwoSMod").css("display", "inline");

                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }
                $scope.cargarNivel3 = function (n3, n4, n5) {
                    $("#cateThreeMod").css("display", "none");
                    $("#cateThreeSMod").css("display", "none");
                    $scope.cate3Mod = [];
                    mapCateMod3 = new Map();
                    $scope.idCate3Mod = "";

                    $("#cateFourMod").css("display", "none");
                    $("#cateFourSMod").css("display", "none");
                    $scope.cate4Mod = [];
                    mapCateMod4 = new Map();
                    $scope.idCate4Mod = "";

                    $("#cateFiveMod").css("display", "none");
                    $("#cateFiveSMod").css("display", "none");
                    $scope.cate5Mod = [];
                    mapCateMod5 = new Map();
                    $scope.idCate5Mod = "";

                    var num = 0;
                    MateriaService.listarCategoriasFilter(mapCateMod2.get($scope.idCate2Mod)).success(function (data) {
                        $scope.cate3Mod = [];
                        angular.forEach(data, function (value, key) {
                            num++;
                            if (parseInt(value.parent) === parseInt(mapCateMod2.get($scope.idCate2Mod))) {
                                $scope.cate3Mod.push(value.name);
                                mapCateMod3.set(value.name, value.id);
                            }
                            if (num === data.length) {
                                $scope.idCate3Mod = JSON.parse(JSON.stringify(mapCategoriasJSON.get(n3))).name;

                                if (n4 > 0) {
                                    $scope.cargarNivel4(n4, n5);
                                }
                            }
                        });
                        $("#cateThreeMod").css("display", "inline");
                        $("#cateThreeSMod").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }
                $scope.cargarNivel4 = function (n4, n5) {
                    $("#cateFourMod").css("display", "none");
                    $("#cateFourSMod").css("display", "none");
                    $scope.cate4Mod = [];
                    mapCateMod4 = new Map();
                    $scope.idCate4Mod = "";

                    $("#cateFiveMod").css("display", "none");
                    $("#cateFiveSMod").css("display", "none");
                    $scope.cate5Mod = [];
                    mapCateMod5 = new Map();
                    $scope.idCate5Mod = "";

                    var num = 0;
                    MateriaService.listarCategoriasFilter(mapCateMod3.get($scope.idCate3Mod)).success(function (data) {
                        $scope.cate4Mod = [];
                        angular.forEach(data, function (value, key) {
                            num++;
                            if (parseInt(value.parent) === parseInt(mapCateMod3.get($scope.idCate3Mod)) && value.visible === 1) {
                                $scope.cate4Mod.push(value.name);
                                mapCateMod4.set(value.name, value.id);
                            }
                            if (num === data.length) {
                                $scope.idCate4Mod = JSON.parse(JSON.stringify(mapCategoriasJSON.get(n4))).name;

                                if (n5 > 0) {
                                    $scope.cargarNivel4(n5);
                                }
                            }
                            $("#cateFourMod").css("display", "inline");
                            $("#cateFourSMod").css("display", "inline");
                        });

                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }
                $scope.cargarNivel5 = function (n5) {
                    $("#cateFiveMod").css("display", "none");
                    $("#cateFiveSMod").css("display", "none");
                    $scope.cate5Mod = [];
                    mapCateMod5 = new Map();
                    $scope.idCate5Mod = "";

                    var num = 0;
                    MateriaService.listarCategoriasFilter(mapCateMod4.get($scope.idCate4Mod)).success(function (data) {
                        $scope.cate5Mod = [];
                        angular.forEach(data, function (value, key) {
                            num++;
                            if (parseInt(value.parent) === parseInt(mapCateMod4.get($scope.idCate4Mod)) && value.visible === 1) {
                                $scope.cate5Mod.push(value.name);
                                mapCateMod5.set(value.name, value.id);
                            }
                            if (num === data.length) {
                                $scope.idCate5Mod = JSON.parse(JSON.stringify(mapCategoriasJSON.get(n5))).name;
                            }
                            $("#cateFiveMod").css("display", "inline");
                            $("#cateFiveSMod").css("display", "inline");
                        });

                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.buscarPadreHastaFacuOPost = function (categoryId) {
                    var cate5 = "";
                    var cate4 = "";
                    var cate3 = "";
                    var cate2 = "";
                    var cate1 = "";

                    var num = 0;

                    if (mapCategorias.get(categoryId) !== undefined) {
                        if (mapCategorias.get(categoryId) + "".toUpperCase() !== "FACULTADES" || mapCategorias.get(categoryId) + "".toUpperCase() !== "POSTGRADO") {
                            cate5 = categoryId;
                            num = num + 1;

                            if (mapCategoriasParent.get(cate5) !== undefined) {
                                if (mapCategorias.get(mapCategoriasParent.get(cate5)) + "".toUpperCase() !== "FACULTADES" || mapCategorias.get(mapCategoriasParent.get(cate5)) + "".toUpperCase() !== "POSTGRADO") {
                                    cate4 = mapCategoriasParent.get(cate5);
                                    if (cate4 > 0) {
                                        num = num + 1;
                                    }

                                    if (mapCategoriasParent.get(cate4) !== undefined) {
                                        if (mapCategorias.get(mapCategoriasParent.get(cate4)) + "".toUpperCase() !== "FACULTADES" || mapCategorias.get(mapCategoriasParent.get(cate4)) + "".toUpperCase() !== "POSTGRADO") {
                                            cate3 = mapCategoriasParent.get(cate4);
                                            if (cate3 > 0) {
                                                num = num + 1;
                                            }

                                            if (mapCategoriasParent.get(cate3) !== undefined) {
                                                if (mapCategorias.get(mapCategoriasParent.get(cate3)) + "".toUpperCase() !== "FACULTADES" || mapCategorias.get(mapCategoriasParent.get(cate3)) + "".toUpperCase() !== "POSTGRADO") {
                                                    cate2 = mapCategoriasParent.get(cate3);
                                                    if (cate2 > 0) {
                                                        num = num + 1;
                                                    }
                                                    if (mapCategoriasParent.get(cate2) !== undefined) {
                                                        if (mapCategorias.get(mapCategoriasParent.get(cate2)) + "".toUpperCase() !== "FACULTADES" || mapCategorias.get(mapCategoriasParent.get(cate2)) + "".toUpperCase() !== "POSTGRADO") {
                                                            cate1 = mapCategoriasParent.get(cate2);
                                                            if (cate1 > 0) {
                                                                num = num + 1;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (num === 1) {
                        console.log(mapCategorias.get(cate5))
                        $("#primeraCategoriaMod").val(cate5);
                    } else if (num === 2) {
                        console.log(mapCategorias.get(cate5) + " - " + mapCategorias.get(cate4))
                        $("#primeraCategoriaMod").val(cate4);
                        $scope.cargarNivel2(cate5, 0, 0, 0);
                    } else if (num === 3) {
                        console.log(mapCategorias.get(cate5) + " - " + mapCategorias.get(cate4) + " - " + mapCategorias.get(cate3))
                        $("#primeraCategoriaMod").val(cate3);
                        $scope.cargarNivel2(cate4, cate5, 0, 0);
                    } else if (num === 4) {
                        console.log(mapCategorias.get(cate5) + " - " + mapCategorias.get(cate4) + " - " + mapCategorias.get(cate3) + " - " + mapCategorias.get(cate2))
                        $("#primeraCategoriaMod").val(cate2);
                        $scope.cargarNivel2(cate3, cate4, cate5, 0);
                    } else if (num === 5) {
                        console.log(mapCategorias.get(cate5) + " - " + mapCategorias.get(cate4) + " - " + mapCategorias.get(cate3) + " - " + mapCategorias.get(cate2) + " - " + mapCategorias.get(cate1))
                        $("#primeraCategoriaMod").val(cate1);
                        $scope.cargarNivel2(cate2, cate3, cate4, cate5);
                    }
                };
                $scope.buscarPadreHastaFacuOPost2 = function (categoryId) {
                    var cate5 = "";
                    var cate4 = "";
                    var cate3 = "";
                    var cate2 = "";
                    var cate1 = "";

                    var num = 0;

                    if (mapCategorias.get(categoryId) !== undefined) {
                        if (mapCategorias.get(categoryId) + "".toUpperCase() !== "FACULTADES" || mapCategorias.get(categoryId) + "".toUpperCase() !== "POSTGRADO") {
                            cate5 = categoryId;
                            num = num + 1;

                            if (mapCategoriasParent.get(cate5) !== undefined) {
                                if (mapCategorias.get(mapCategoriasParent.get(cate5)) + "".toUpperCase() !== "FACULTADES" || mapCategorias.get(mapCategoriasParent.get(cate5)) + "".toUpperCase() !== "POSTGRADO") {
                                    cate4 = mapCategoriasParent.get(cate5);
                                    if (cate4 > 0) {
                                        num = num + 1;
                                    }

                                    if (mapCategoriasParent.get(cate4) !== undefined) {
                                        if (mapCategorias.get(mapCategoriasParent.get(cate4)) + "".toUpperCase() !== "FACULTADES" || mapCategorias.get(mapCategoriasParent.get(cate4)) + "".toUpperCase() !== "POSTGRADO") {
                                            cate3 = mapCategoriasParent.get(cate4);
                                            if (cate3 > 0) {
                                                num = num + 1;
                                            }

                                            if (mapCategoriasParent.get(cate3) !== undefined) {
                                                if (mapCategorias.get(mapCategoriasParent.get(cate3)) + "".toUpperCase() !== "FACULTADES" || mapCategorias.get(mapCategoriasParent.get(cate3)) + "".toUpperCase() !== "POSTGRADO") {
                                                    cate2 = mapCategoriasParent.get(cate3);
                                                    if (cate2 > 0) {
                                                        num = num + 1;
                                                    }
                                                    if (mapCategoriasParent.get(cate2) !== undefined) {
                                                        if (mapCategorias.get(mapCategoriasParent.get(cate2)) + "".toUpperCase() !== "FACULTADES" || mapCategorias.get(mapCategoriasParent.get(cate2)) + "".toUpperCase() !== "POSTGRADO") {
                                                            cate1 = mapCategoriasParent.get(cate2);
                                                            if (cate1 > 0) {
                                                                num = num + 1;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (num === 2) {
                        console.log(mapCategorias.get(cate5) + " - " + mapCategorias.get(cate4))
                        return mapCategorias.get(cate5);
                    } else if (num === 3) {
                        console.log(mapCategorias.get(cate5) + " - " + mapCategorias.get(cate4) + " - " + mapCategorias.get(cate3))
                        return mapCategorias.get(cate4);
                    } else if (num === 4) {
                        console.log(mapCategorias.get(cate5) + " - " + mapCategorias.get(cate4) + " - " + mapCategorias.get(cate3) + " - " + mapCategorias.get(cate2))
                        return mapCategorias.get(cate3);
                    } else if (num === 5) {
                        console.log(mapCategorias.get(cate5) + " - " + mapCategorias.get(cate4) + " - " + mapCategorias.get(cate3) + " - " + mapCategorias.get(cate2) + " - " + mapCategorias.get(cate1))
                        return mapCategorias.get(cate2);
                    }
                };

                $scope.registrar = function () {
                    if ($scope.personaAgregar.nombre === "" || $scope.personaAgregar.nombre === undefined || $scope.personaAgregar.nombre === null ||
                            $scope.personaAgregar.corto === "" || $scope.personaAgregar.corto === undefined || $scope.personaAgregar.corto === null ||
                            $scope.personaAgregar.visible === "" || $scope.personaAgregar.visible === undefined || $scope.personaAgregar.visible === null || $scope.personaAgregar.visible === "0") {
                        swal("Mensaje del Sistema!", "Los campos Nombre, Corto y Visible no debe quedar vacío", "error");
                    } else {
                        var idCategoria = 0;

                        mapCate2.get($scope.idCate2)
                        if ($scope.idCate4 !== "") {
                            idCategoria = mapCate4.get($scope.idCate4);
                        } else if ($scope.idCate3 !== "") {
                            idCategoria = mapCate3.get($scope.idCate3);
                        } else if ($scope.idCate2 !== "") {
                            idCategoria = mapCate2.get($scope.idCate2);
                        } else {
                            idCategoria = $("#primeraCategoria").val();
                        }

                        var fecInicio = "";
                        if (!isNaN(new Date($scope.personaAgregar.inicio).getTime())) {
                            fecInicio = "&courses[0][startdate]=" + (new Date($scope.personaAgregar.inicio).getTime() / 1000);
                        }

                        var fecFin = "";
                        if (!isNaN(new Date($scope.personaAgregar.fin).getTime())) {
                            fecFin = "&courses[0][enddate]=" + (new Date($scope.personaAgregar.fin).getTime() / 1000);
                        }

                        var urlParameter = "courses[0][fullname]=" + $scope.personaAgregar.nombre + "&courses[0][shortname]=" + $scope.personaAgregar.corto
                                + "&courses[0][categoryid]=" + idCategoria + "&courses[0][visible]=" + $scope.personaAgregar.visible +
                                "" + fecInicio + fecFin;

                        MateriaService.crear(urlParameter).success(function (data) {
                            if (data[0].shortname === $scope.personaAgregar.corto) {
                                swal("Mensaje del Sistema!", "Datos Agregados exitosamente", "success");
                                $('#modalAgregar').modal('hide');
                                $scope.listar();
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Agregados, " + data.message, "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };
                $scope.atras = function () {
                    location.reload();
                };
                $scope.actualizar = function () {
                    if ($scope.personaModificar.nombre === "" || $scope.personaModificar.nombre === undefined || $scope.personaModificar.nombre === null ||
                            $scope.personaModificar.corto === "" || $scope.personaModificar.corto === undefined || $scope.personaModificar.corto === null ||
                            $("#visibleMod").val() === "") {
                        swal("Mensaje del Sistema!", "Los campos Nombre, Corto y Visible no debe quedar vacío", "error");
                    } else {
                        var idCategoria = 0;

                        mapCateMod2.get($scope.idCate2Mod)
                        if ($scope.idCate4Mod !== "") {
                            idCategoria = mapCateMod4.get($scope.idCate4Mod);
                        } else if ($scope.idCate3Mod !== "") {
                            idCategoria = mapCateMod3.get($scope.idCate3Mod);
                        } else if ($scope.idCate2Mod !== "") {
                            idCategoria = mapCateMod2.get($scope.idCate2Mod);
                        } else {
                            idCategoria = $("#primeraCategoriaMod").val();
                        }

                        var fecInicio = "";
//                        alert(new Date($("#inicioMod").val()).getTime());
                        if (!isNaN(new Date($("#inicioMod").val()).getTime())) {
                            var myDate = new Date($("#inicioMod").val());
                            myDate.setDate(myDate.getDate() + 1);
                            fecInicio = "&courses[0][startdate]=" + (myDate.getTime() / 1000);
                        }

                        var fecFin = "";
                        if (!isNaN(new Date($("#finMod").val()).getTime())) {
                            var myDate = new Date($("#finMod").val());
                            myDate.setDate(myDate.getDate() + 1);
                            fecFin = "&courses[0][enddate]=" + (myDate.getTime() / 1000);
                        }

                        var urlParameter = "courses[0][id]=" + $scope.personaModificar.id + "&courses[0][fullname]=" + $scope.personaModificar.nombre + "&courses[0][shortname]=" + $scope.personaModificar.corto
                                + "&courses[0][categoryid]=" + idCategoria + "&courses[0][visible]=" + $("#visibleMod").val() +
                                "" + fecInicio + fecFin;

                        MateriaService.actualizar(urlParameter).success(function (data) {
                            if (data.errorcode !== "invalidparameter" ||  data.warnings !== undefined) {
                                swal("Mensaje del Sistema!", "Datos Actualizados exitosamente", "success");
                                $('#modalModificar').modal('hide');
                                $scope.listar();
                            } else if (data.errorcode === "invalidparameter") {
                                swal("Mensaje del Sistema!", "Datos no Agregados, " + data.message, "error");
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Agregados, " + data.warnings[0].message, "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };
                Date.prototype.dateToInput = function () {
                    return this.getFullYear() + '-' + ('0' + (this.getMonth() + 1)).substr(-2, 2) + '-' + ('0' + this.getDate()).substr(-2, 2);
                }
                $scope.modificar = function (personas) {
                    $("#visibleMod").val(personas.visible + "");
                    $scope.personaModificar.id = personas.id;
                    $scope.personaModificar.nombre = personas.displayname;
                    $scope.personaModificar.corto = personas.shortname;

                    var inic = personas.startdate.split('-');
                    $("#inicioMod").val(inic[2] + "-" + inic[1] + "-" + inic[0]);

                    var finic = personas.enddate.split('-');
                    $("#finMod").val(finic[2] + "-" + finic[1] + "-" + finic[0]);

                    $("#modalModificar").modal("show");
                    $scope.buscarPadreHastaFacuOPost(personas.categoryid);
                };
//                $scope.agregarEnrol = function (personas) {
//                    $scope.enrol.idusuario = personas.idusuario;
//                    $scope.enrol.idmateria = personas.idmateria;
//                    $scope.enrol.estado = personas.estado === true ? false : true;
//                    $scope.enrol.usuario = personas.usuario;
//                    $scope.enrol.materia = personas.materia;
//                    $scope.enrol.facultad = personas.facultad;
//                    $scope.enrol.periodo = personas.periodo;
//
//                    swal({
//                        title: "Mensaje del Sistema",
//                        text: "¿ Seguro que deseas matricularte a la materia " + personas.displayname + " ?",
//                        type: "warning",
//                        showCancelButton: true,
//                        cancelButtonText: "Cancelar",
//                        confirmButtonColor: "#DD6B55",
//                        confirmButtonText: "Confirmar",
//                        closeOnConfirm: false},
//                            function () {
//                                MateriaService.registrarLocalmente($scope.enrol).success(function (data) {
//                                    if (data.id > 0) {
//                                        swal("Mensaje del Sistema!", "Los datos han sido registrados correctamente!.", "success");
//                                        mapMatriculaciones.set(personas.id, personas);
//                                        $scope.listar();
//                                    }
//                                }).error(function (e) {
//                                    swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
//                                });
//                            });
//                };

                $scope.quitarEnrol = function (personas) {
                    $scope.enrol.id = personas.id;
                    $scope.enrol.idusuario = personas.idusuario;
                    $scope.enrol.idmateria = personas.idmateria;
                    $scope.enrol.estado = true;
                    $scope.enrol.usuario = personas.usuario;
                    $scope.enrol.materia = personas.materia;
                    $scope.enrol.facultad = personas.facultad;
                    $scope.enrol.periodo = $cookies.getObject("periodo");
                    $scope.enrol.procesado = true;

                    swal({
                        title: "Mensaje del Sistema",
                        text: "¿ Seguro que deseas cancelar la matriculación de " + personas.usuario + " para la materia " + personas.materia + " ?",
                        type: "warning",
                        showCancelButton: true,
                        cancelButtonText: "Cancelar",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Confirmar",
                        closeOnConfirm: false},
                            function () {
                                MateriaService.actualizarLocalmente($scope.enrol).success(function (data) {
                                    swal("Mensaje del Sistema!", "Los datos han sido actualizados correctamente!.", "success");
                                    $scope.listar();
                                }).error(function (e) {
                                    swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                });
                            });
                };

                $scope.agregar = function () {
                    swal({
                        title: "Mensaje del Sistema",
                        text: "¿ Seguro que deseas aprobar el listado de matriculaciones ?",
                        type: "warning",
                        showCancelButton: true,
                        cancelButtonText: "Cancelar",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Confirmar",
                        closeOnConfirm: false},
                            function () {
                                var num = 0;
                                var numerMax = 0;
                                angular.forEach($scope.people, function (value, key) {
                                    num++;
                                    var param = "enrolments[0][userid]=" + value.idusuario + "&enrolments[0][courseid]=" + value.idmateria + "&enrolments[0][roleid]=5";
                                    MateriaService.enrolar(param).success(function (data) {

                                    }).error(function (e) {
                                        console.log("EROROR CONEXION CON EL SERVIDOR...")
                                    });
                                    if (num === $scope.people.length) {
                                        angular.forEach($scope.people, function (valor, llave) {
                                            $scope.enrolado = {};
                                            $scope.enrolado.id = valor.id;
                                            $scope.enrolado.idusuario = valor.idusuario;
                                            $scope.enrolado.idmateria = valor.idmateria;
                                            $scope.enrolado.estado = true;
                                            $scope.enrolado.usuario = valor.usuario;
                                            $scope.enrolado.materia = valor.materia;
                                            $scope.enrolado.facultad = valor.facultad;
                                            $scope.enrolado.periodo = $cookies.getObject("periodo");
                                            $scope.enrolado.procesado = true;
                                            numerMax++;

                                            MateriaService.actualizarLocalmente($scope.enrolado).success(function (data) {
                                                if (numerMax === $scope.people.length) {
                                                    swal("Mensaje del Sistema!", "Los datos han sido procesados correctamente!.", "success");
                                                    $scope.listar();
                                                }
                                            }).error(function (e) {
                                                if (numerMax === $scope.people.length) {
                                                    swal("Mensaje del Sistema!", "Los datos han sido procesados correctamente!.", "success");
                                                    $scope.listar();
                                                }
                                            });
                                        });
                                    }
                                });
                            });

                };

                $scope.registrarEnrol = function () {
//                    alert(mapUsuarioEnrol.get($scope.idUsuario))
                    var param = "enrolments[0][userid]=" + mapUsuarioEnrol.get($scope.idUsuario) + "&enrolments[0][courseid]=" + $scope.enrol.id + "&enrolments[0][roleid]=5";
                    MateriaService.enrolar(param).success(function (data) {
                        console.log("REPONSE->", data)
                        if (data === null) {
                            swal('Registrado!',
                                    'Se ha registrado una nueva matriculación!.',
                                    'success'
                                    );
                            $scope.listar();
                        } else {
                            swal("Mensaje del Sistema!", "No se pudo a la Persona, verifique existencia en Usuarios", "error");
                        }
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.eliminar = function (roles) {
                    swal({
                        title: "Mensaje del Sistema",
                        text: "Seguro que quiere eliminar la Persona " + roles.nombrePersona + " " + roles.apellidoPersona + "?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    MateriaService.eliminar(roles.idPersona).success(function (data) {
                                        if (data === true) {
                                            swal('Eliminado!',
                                                    'Haz eliminado exitosamente.',
                                                    'success'
                                                    );
                                            $scope.listar();
                                        } else {
                                            swal("Mensaje del Sistema!", "No se pudo a la Persona, verifique existencia en Usuarios", "error");
                                        }
                                    }).error(function (e) {
                                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                    });
                                } else {
                                    swal("Mensaje del Sistema", "Haz cancelado la operación!", "error");
                                }
                            });
                };

                $scope.buscar();
            }]);