'use strict'
app
        .controller('listElementosCtrl', ['$state', '$scope', 'config', 'ElementoService', '$cookies', '$base64', function ($state, $scope, config, ElementoService, $cookies, $base64) {
//                console.log("HOLAAA");

                $scope.elementos = {};
                $scope.elementoAgregar = {};
                $scope.elementoModificar = {};

                $scope.page = 1;
                $scope.pageChanged = function () {
                    var startPos = ($scope.page - 1) * 10;
                    $scope.displayItems = $scope.totalItems.slice(startPos, startPos + 10);
                    console.log($scope.page);
                };
//                FIN DE PAGINACION

                $scope.listar = function () {
                    ElementoService.listar().success(function (data) {
                        $scope.elementos = data;
                        $scope.displayItems = $scope.elementos.slice(0, 5);
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.registrar = function () {
//                    alert($scope.rolAgregar.descripcionRolAgregar);
                    if ($scope.elementoAgregar.descriElemento === "" || $scope.elementoAgregar.descriElemento === undefined || $scope.elementoAgregar.descriElemento === null) {
                        swal("Mensaje del Sistema!", "Descripción no debe quedar vacío", "error");
                    } else {
                        ElementoService.crear($scope.elementoAgregar).success(function (data) {
                            if (data === true) {
                                swal("Mensaje del Sistema!", "Datos Agregados exitosamente", "success");
                                $('#modalAgregar').modal('hide');
                                $scope.elementoAgregar = {};
                                $scope.listar();
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Agregados", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };

                $scope.atras = function () {
                    location.reload();
                };

                $scope.agregar = function () {
                    $scope.elementoAgregar.descriElemento = "";
                };

                $scope.actualizar = function () {
//                    alert($scope.rolAgregar.descripcionRolAgregar);
                    if ($scope.elementoModificar.descriElemento === "" || $scope.elementoModificar.descriElemento === undefined || $scope.elementoAgregar.descriElemento === null) {
                        swal("Mensaje del Sistema!", "Descripción no debe quedar vacío", "error");
                    } else {
                        ElementoService.actualizar($scope.elementoModificar).success(function (data) {
                            if (data === true) {
                                swal("Mensaje del Sistema!", "Datos Modificados exitosamente", "success");
                                $('#modalModificar').modal('hide');
                                $scope.elementoModificar = {};
                                $scope.listar();
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Modificados", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };

                $scope.modificar = function (elementos) {
                    $scope.elementoModificar.descriElemento = elementos.descriElemento;
                    $scope.elementoModificar.idElemento = elementos.idElemento;
                    $("#modalModificar").modal("show");
                };

//                $scope.eliminar = function (elemento) {
//                    var r = confirm("Seguro que quiere eliminar el Elemento " + elemento.descriElemento + "?");
//                    if (r === true) {
//                        ElementoService.eliminar(elemento.idElemento).success(function (data) {
//                            if (data === true) {
//                                swal('Eliminado!',
//                                        'Haz eliminado exitosamente.',
//                                        'success'
//                                        );
//                                $scope.listar();
//                            }
//                        });
//                    } else {
//                        swal('Cancelado',
//                                'Haz cancelado la eliminación',
//                                'error'
//                                );
//                    }
//                };

                $scope.eliminar = function (elemento) {
                    swal({
                        title: "Mensaje del Sistema",
                        text: "Seguro que quiere eliminar el Elemento " + elemento.descriElemento + "?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    ElementoService.eliminar(elemento.idElemento).success(function (data) {
                                        if (data === true) {
                                            swal('Eliminado!',
                                                    'Haz eliminado exitosamente.',
                                                    'success'
                                                    );
                                            $scope.listar();
                                        }
                                    }).error(function (e) {
                                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                    });
                                } else {
                                    swal("Mensaje del Sistema", "Haz cancelado la operación!", "error");
                                }
                            });
                };
                $scope.listar();
            }]);