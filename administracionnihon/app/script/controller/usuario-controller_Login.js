'use strict'
appLogin
        .controller('loginCtrl', ['$state', '$scope', 'config', 'UsuarioService', 'PersonaService', '$cookies', '$base64', 'FallidoService', function ($state, $scope, config, UsuarioService, PersonaService, $cookies, $base64, FallidoService) {
                $scope.formData = {};
                $scope.usuario = {};
                $scope.cuenta = {};
                $scope.usuario.persona = {};
                $scope.idUsuario = 0;

//                $("#mensajeFallido").hide();
                $("#btnResetFaliido").css("display", "none");//hide();
                $("#user").focus();


//                $scope.contFallido = 0;
//                $("#pass").hide();
                //$("#btnIngresar").css("display", "none");//hide();
                $scope.ingresar = function () {
                    if ($scope.formData.usuarioUsuario === undefined || $scope.formData.usuarioUsuario === "") {
                        $("#mensajeError").html('<div class="alert alert-danger">El campo Usuario no debe quedar vacío</div>');
                        $("#user").focus();
                    } else if ($scope.formData.claveUsuario === undefined || $scope.formData.claveUsuario === "") {
                        $("#mensajeError").html('<div class="alert alert-danger">El campo Clave no debe quedar vacío</div>');
                        $("#pass").focus();
                    } else {
//                        UsuarioService.traerMaterias().success(function (detalles) {
                        UsuarioService.loginMoodle($scope.formData.usuarioUsuario, $scope.formData.claveUsuario).success(function (data) {
                            if (data.token === undefined || data.token === null) {
                                if (data.errorcode === "invalidlogin") {
                                    swal("Mensaje del Sistema!", "Usuario o Contraseña inválida", "error");
                                } else if (data.errorcode === "cannotcreatetoken") {
                                    $cookies.put("estado", true);
                                    $cookies.putObject("facultad", "");
                                    $cookies.putObject("usuario", $scope.formData.usuarioUsuario);

                                    UsuarioService.recuperarUltimoPeriodo().success(function (y) {
                                        $cookies.putObject("periodo", y.descripcion);
                                        location.href = "menu.html";
                                    }).error(function (e) {
                                        //swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                    });


                                }

                            } else {
                                $cookies.put("estado", true);
                                $cookies.putObject("token", data.token);
                                $cookies.putObject("facultad", "");
                                $cookies.putObject("usuario", $scope.formData.usuarioUsuario);

                                UsuarioService.recuperarUltimoPeriodo().success(function (y) {
                                    $cookies.putObject("periodo", y.descripcion);
                                    location.href = "menu.html?data=" + data.token;
                                }).error(function (e) {
                                    //swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                });

                            }
//                            }).error(function (e) {
//                                swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
//                            });
                        });
                    }
                };
//                };
                $scope.crearUsuarioCuenta = function (data) {
                    $scope.usuario.entrada = false;
                    $scope.usuario.estado = true;
                    $scope.usuario.persona.idPersona = data;
                    var usu1 = $base64.encode($scope.usuario.usuarioUsuario);
                    var usu2 = $base64.encode(usu1);
                    $scope.usuario.usuarioUsuario = usu2;
                    var cla1 = $base64.encode($scope.usuario.claveUsuario);
                    var cla2 = $base64.encode(cla1);
                    $scope.usuario.claveUsuario = cla2;
                    UsuarioService.crear($scope.usuario).success(function (data) {
                        if (data === false) {
                            swal("Mensaje del Sistema!", "No se pudo crear el Usuario! El usuario ingresado ya existe.", "error");
                            var usu1 = $base64.decode($scope.usuario.usuarioUsuario);
                            var usu2 = $base64.decode(usu1);
                            $scope.usuario.usuarioUsuario = usu2;
                        } else {
//                            swal("Mensaje del Sistema!", "Usuario creado exitosamente", "success");
//                            $state.reload();
//                            
//delay in angularJS
                            swal({
                                title: "Mensaje del Sistema!",
                                text: "Usuario creado exitosamente",
                                type: "success",
//                                showCancelButton: true,
                                confirmButtonColor: "#8cd4f5"
                            }, function () {
                                location.href = "#login";
                                location.reload();
                            });
                        }
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.siguiente = function () {
                    $("#tabsSalir").css("display", "inline");
                    $("#tabsOlvidoClave").css("display", "none");
                    $("#tabs").css("display", "none");
                    if ($scope.formData.usuarioUsuario === undefined || $scope.formData.usuarioUsuario === "") {
                        $("#mensajeError").html('<div class="alert alert-danger">El campo Usuario no debe quedar vacío</div>');
//                        swal("Mensaje del Sistema!", "El campo Usuario no debe quedar vacío", "error");
                        $("#user").focus();
                        $("#tabsSalir").css("display", "none");
                        $("#tabsOlvidoClave").css("display", "inline");
                        $("#tabs").css("display", "inline");
                    } else {
                        UsuarioService.getIdByUser($scope.formData.usuarioUsuario).success(function (data) {
                            if (data !== 0) {
                                $scope.idUsuario = data;
                                $scope.contador = 0;
                                FallidoService.listarUsuario($scope.idUsuario).success(function (d) {
                                    $scope.contador = parseInt(d.contador);
                                    if ($scope.contador === 10) {
                                        $("#mensajeFallido").show();
//                                        $("#btnResetFaliido").fadeIn();
                                        $("#btnResetFaliido").css("display", "inline");
//                                        $("#btnSiguiente").hide();
                                        $("#btnSiguiente").css("display", "none");
                                        $("#user").hide();
                                    } else if ($scope.contador === 0 && d.enviado === true) {
                                        $("#emailEnviado").show();
                                        $("#mensajeFallido").hide();
//                                        $("#btnResetFaliido").hide();
                                        $("#btnResetFaliido").css("display", "none");
//                                        $("#btnSiguiente").hide();
                                        $("#btnSiguiente").css("display", "none");
                                        $("#user").hide();
                                    } else {
                                        $("#pass").show();
//                                        $("#btnIngresar").show();
                                        $("#btnIngresar").css("display", "inline");
                                        $("#user").hide();
//                                        $("#btnSiguiente").hide();
                                        $("#btnSiguiente").css("display", "none");
                                        $("#pass").focus();
                                    }
                                });
                            } else {
                                $("#mensajeError").html('<div class="alert alert-danger">El Usuario ingresado no se encuentra...</div>');
//                        swal("Mensaje del Sistema!", "El campo Usuario no debe quedar vacío", "error");
                                $("#user").focus();
                                $("#tabsSalir").css("display", "none");
                                $("#tabsOlvidoClave").css("display", "inline");
                                $("#tabs").css("display", "inline");
                            }
                        }).error(function (error) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };
                $scope.volver = function () {
                    $("#emailEnviado").css("display", "none");
                    $("#mensajeFallido").css("display", "none");
                    $("#btnResetFaliido").css("display", "none");
                    $("#tabsSalir").css("display", "none");
                    $("#tabsOlvidoClave").css("display", "inline");
                    $("#tabs").css("display", "inline");


                    $("#pass").hide();
                    $("#user").show();
//                                        $("#btnIngresar").show();
                    $("#btnIngresar").css("display", "none");
                    $("#user").show();
//                                      $("#btnSiguiente").hide();
                    $("#btnSiguiente").css("display", "inline");
                    $("#user").focus();
                }
                $scope.reset = function () {
//                    $("#btnResetFaliido").fadeOut("slow");
                    $("#btnResetFaliido").css("display", "none");
                    FallidoService.listarUsuario($scope.idUsuario).success(function (d) {

                        if (d.enviado === true) {
                            $("#emailEnviado").show();
                        } else {
                            UsuarioService.restaurarPass($scope.idUsuario).success(function (data) {
                                if (data === true) {
//                                    swal("Mensaje del Sistema!", "Se ha enviado un correo a su dirección de correo electrónico", "success");
                                    alert("Se ha enviado un correo a su dirección de correo electrónico");
                                    location.reload();
                                }
                            }).error(function (e) {
                                swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                            });
                        }
                    });
                };
                $scope.crearCuenta = function () {
                    var valor = true;
                    if ($("#nombrePersona").val() === "") {
                        $("#nombrePersona").css('border', "red solid 1px");
                        $("#nomPer").html("Nombre es un campo requerido");
                        valor = false;
                    } else {
                        $("#nombrePersona").css('border', "#CCC solid 1px");
                        $("#nomPer").html("");
                    }

                    if ($("#apellidoPersona").val() === "") {
                        $("#apellidoPersona").css('border', "red solid 1px");
                        $("#apePer").html("Apellido es un campo requerido");
                        valor = false;
                    } else {
                        $("#apellidoPersona").css('border', "#CCC solid 1px");
                        $("#apePer").html("");
                    }

                    if ($("#emailPersona").val() === "") {
                        $("#emailPersona").css('border', "red solid 1px");
                        $("#emailPer").html("Email es un campo requerido");
                        valor = false;
                    } else {
                        $("#emailPersona").css('border', "#CCC solid 1px");
                        $("#emailPer").html("");
                    }

                    if ($("#usuarioUsuario").val() === "") {
                        $("#usuarioUsuario").css('border', "red solid 1px");
                        $("#usuUsu").html("Email es un campo requerido");
                        valor = false;
                    } else {
                        $("#usuarioUsuario").css('border', "#CCC solid 1px");
                        $("#usuUsu").html("");
                    }

                    if ($("#claveUsuario").val() === "") {
                        $("#claveUsuario").css('border', "red solid 1px");
                        $("#claUsu").html("Contraseña es un campo requerido");
                        valor = false;
                    } else {
                        $("#claveUsuario").css('border', "#CCC solid 1px");
                        $("#claUsu").html("");
                    }

                    if ($("#claveUsuarioConfirm").val() === "") {
                        $("#claveUsuarioConfirm").css('border', "red solid 1px");
                        $("#claUsuConf").html("Contraseña de confirmación es un campo requerido");
                        valor = false;
                    } else {
                        $("#claveUsuarioConfirm").css('border', "#CCC solid 1px");
                        $("#claUsuConf").html("");
                    }

                    var email = $("#emailPersona").val();
                    if (!email.includes("@")) {
                        $("#emailPersona").css('border', "red solid 1px");
                        $("#emailPer").html("Debe contener @");
                        valor = false;
                    } else {
                        $("#emailPersona").css('border', "#CCC solid 1px");
                        $("#emailPer").html("");
                    }

                    if (!valor) {
                        swal("Mensaje del Sistema!", "Complete los campos requeridos", "error");
                    } else {
                        if ($scope.usuario.claveUsuario !== $scope.usuario.claveUsuarioConfirm) {
                            swal("Mensaje del Sistema!", "Los campos Contraseña y confirmación no coinciden", "error");
                            $("#claveUsuario").val("");
                            $("#claveUsuarioConfirm").val("");
                            $("#claveUsuario").focus();
                            valor = false;
                        } else {
                            UsuarioService.verificarUsuarioExistencia($scope.usuario.usuarioUsuario).success(function (data) {
                                if (data === false) {
                                    PersonaService.crearReturnObj($scope.cuenta).success(function (data) {
                                        if (data === 0) {
                                            swal("Mensaje del Sistema!", "No se pudo crear persona! El email ingresado ya está siendo utilizado por otro usuario.", "error");
                                        } else {
                                            $scope.crearUsuarioCuenta(data);
                                        }
                                    }).error(function (e) {
                                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                    });
                                } else {
                                    swal("Mensaje del Sistema!", "El usuario ingresado ya existe!", "error");
                                }
                            }).error(function (e) {
                                swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                            });
                        }
                    }
                };
                $scope.resetClave = function () {
                    var valor = true;

                    if ($("#emailPerson").val() === "") {
                        $("#emailPerson").css('border', "red solid 1px");
                        $("#emailPerso").html("Email es un campo requerido");
                        valor = false;
                    } else {
                        $("#emailPerson").css('border', "#CCC solid 1px");
                        $("#emailPerso").html("");
                    }

                    var email = $("#emailPerson").val();
                    if (!email.includes("@")) {
                        $("#emailPerson").css('border', "red solid 1px");
                        $("#emailPerso").html("Debe contener @");
                        valor = false;
                    } else {
                        $("#emailPerson").css('border', "#CCC solid 1px");
                        $("#emailPerso").html("");
                    }

                    if (!valor) {
                        swal("Mensaje del Sistema!", "Complete los campos requeridos", "error");
                    } else {
//                        if ($scope.usuario.claveUsuario !== $scope.usuario.claveUsuarioConfirm) {
//                            swal("Mensaje del Sistema!", "Los campos Contraseña y confirmación no coinciden", "error");
//                            $("#claveUsuario").val("");
//                            $("#claveUsuarioConfirm").val("");
//                            $("#claveUsuario").focus();
//                            valor = false;
//                        } else {
                        UsuarioService.verificarEmailExistencia($("#emailPerson").val().replace(".", "$")).success(function (data) {
                            if (data !== 0) {
                                UsuarioService.restablecerPass(data).success(function (data) {
                                    if (data === true) {
//                                    swal("Mensaje del Sistema!", "Se ha enviado un correo a su dirección de correo electrónico", "success");
                                        alert("Se ha enviado un correo a su dirección de correo electrónico");
                                        location.reload();
                                    }
                                }).error(function (e) {
                                    swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                });
                            } else {
                                swal("Mensaje del Sistema!", "El Email ingresado no se encuentra registrado!", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
//                        }
                    }
                };
//                $scope.eliminarMensajeError = function () {
//                    $("#mensajeError").html("");
//                };
                $("#user").keyup(function () {
                    if ($scope.formData.usuarioUsuario !== "") {
                        $("#mensajeError").html("");
                    }
//                    console.log("OU -> "+());
                });
                $("#pass").keyup(function () {
                    if ($scope.formData.claveUsuario !== "") {
                        $("#mensajeError").html("");
                    }
//                    console.log("BYE -> "+($scope.formData.claveUsuario));
                });
//                var code = $base64.encode('123');
//                var code2 = $base64.encode(code);
//                alert(code2);
            }])
        .directive('myEnter', ['$cookies', '$base64', 'UsuarioService', 'FallidoService', function ($cookies, $base64, UsuarioService, FallidoService) {
                return function (scope, element, attrs) {
                    element.bind("keydown keypress", function (event) {
                        if (event.which === 13) {
                            scope.$apply(function () {
                                scope.$eval(attrs.myEnter);
                                scope.ingresar = function () {
                                    if (scope.formData.usuarioUsuario === undefined || scope.formData.usuarioUsuario === "") {
                                        $("#mensajeError").html('<div class="alert alert-danger">El campo Usuario no debe quedar vacío</div>');
                                        $("#user").focus();
                                    } else if (scope.formData.claveUsuario === undefined || scope.formData.claveUsuario === "") {
                                        $("#mensajeError").html('<div class="alert alert-danger">El campo Clave no debe quedar vacío</div>');
                                        $("#pass").focus();
                                    } else {

                                        UsuarioService.loginMoodle(scope.formData.usuarioUsuario, scope.formData.claveUsuario).success(function (data) {
                                            if (data.token === undefined || data.token === null) {
                                                if (data.errorcode === "invalidlogin") {
                                                    swal("Mensaje del Sistema!", "Usuario o Contraseña inválida", "error");
                                                } else if (data.errorcode === "cannotcreatetoken") {
                                                    $cookies.put("estado", true);
                                                    $cookies.putObject("facultad", "");
                                                    $cookies.putObject("usuario", scope.formData.usuarioUsuario);

                                                    UsuarioService.recuperarUltimoPeriodo().success(function (y) {
                                                        $cookies.putObject("periodo", y.descripcion);
                                                        location.href = "menu.html";
                                                    }).error(function (e) {
                                                        //swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                                    });
                                                }

                                            } else {
                                                $cookies.put("estado", true);
                                                $cookies.putObject("facultad", "");
                                                $cookies.putObject("usuario", scope.formData.usuarioUsuario);

                                                UsuarioService.recuperarUltimoPeriodo().success(function (y) {
                                                    $cookies.putObject("periodo", y.descripcion);
                                                    location.href = "menu.html?data=" + data.token;
                                                }).error(function (e) {
                                                    //swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                                });

                                            }
                                        }).error(function (e) {
                                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                        });
                                    }
                                };
                                scope.ingresar();
                            });
                            event.preventDefault();
                        }
                    });
                };
            }])
        .directive('myEnterRegister', ['$cookies', '$base64', 'UsuarioService', 'PersonaService', function ($cookies, $base64, UsuarioService, PersonaService) {
                return function (scope, element, attrs) {
                    element.bind("keydown keypress", function (event) {
                        if (event.which === 13) {
                            scope.$apply(function () {
                                scope.$eval(attrs.myEnter);
                                scope.crearCuenta = function () {
                                    var valor = true;
                                    if ($("#nombrePersona").val() === "") {
                                        $("#nombrePersona").css('border', "red solid 1px");
                                        $("#nomPer").html("Nombre es un campo requerido");
                                        valor = false;
                                    } else {
                                        $("#nombrePersona").css('border', "#CCC solid 1px");
                                        $("#nomPer").html("");
                                    }

                                    if ($("#apellidoPersona").val() === "") {
                                        $("#apellidoPersona").css('border', "red solid 1px");
                                        $("#apePer").html("Apellido es un campo requerido");
                                        valor = false;
                                    } else {
                                        $("#apellidoPersona").css('border', "#CCC solid 1px");
                                        $("#apePer").html("");
                                    }

                                    if ($("#emailPersona").val() === "") {
                                        $("#emailPersona").css('border', "red solid 1px");
                                        $("#emailPer").html("Email es un campo requerido");
                                        valor = false;
                                    } else {
                                        $("#emailPersona").css('border', "#CCC solid 1px");
                                        $("#emailPer").html("");
                                    }

                                    if ($("#usuarioUsuario").val() === "") {
                                        $("#usuarioUsuario").css('border', "red solid 1px");
                                        $("#usuUsu").html("Email es un campo requerido");
                                        valor = false;
                                    } else {
                                        $("#usuarioUsuario").css('border', "#CCC solid 1px");
                                        $("#usuUsu").html("");
                                    }

                                    if ($("#claveUsuario").val() === "") {
                                        $("#claveUsuario").css('border', "red solid 1px");
                                        $("#claUsu").html("Contraseña es un campo requerido");
                                        valor = false;
                                    } else {
                                        $("#claveUsuario").css('border', "#CCC solid 1px");
                                        $("#claUsu").html("");
                                    }

                                    if ($("#claveUsuarioConfirm").val() === "") {
                                        $("#claveUsuarioConfirm").css('border', "red solid 1px");
                                        $("#claUsuConf").html("Contraseña de confirmación es un campo requerido");
                                        valor = false;
                                    } else {
                                        $("#claveUsuarioConfirm").css('border', "#CCC solid 1px");
                                        $("#claUsuConf").html("");
                                    }

                                    var email = $("#emailPersona").val();
                                    if (!email.includes("@")) {
                                        $("#emailPersona").css('border', "red solid 1px");
                                        $("#emailPer").html("Debe contener @");
                                        valor = false;
                                    } else {
                                        $("#emailPersona").css('border', "#CCC solid 1px");
                                        $("#emailPer").html("");
                                    }

                                    if (!valor) {
                                        swal("Mensaje del Sistema!", "Complete los campos requeridos", "error");
                                    } else {
                                        if (scope.usuario.claveUsuario !== scope.usuario.claveUsuarioConfirm) {
//                if ($("#claveUsuario").val() !== $("#claveUsuarioConfirm").val()) {
                                            swal("Mensaje del Sistema!", "Los campos Contraseña y confirmación no coinciden", "error");
                                            $("#claveUsuario").val("");
                                            $("#claveUsuarioConfirm").val("");
                                            $("#claveUsuario").focus();
                                            valor = false;
                                        } else {
                                            UsuarioService.verificarUsuarioExistencia(scope.usuario.usuarioUsuario).success(function (data) {
                                                if (data === false) {
                                                    PersonaService.crear(scope.cuenta).success(function (data) {
                                                        if (data === 0) {
                                                            swal("Mensaje del Sistema!", "No se pudo crear persona! El email ingresado ya está siendo utilizado por otro usuario.", "error");
                                                        } else {
                                                            scope.crearUsuarioCuenta(data);
                                                        }
                                                    }).error(function (e) {
                                                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                                    });
                                                } else {
                                                    swal("Mensaje del Sistema!", "El usuario ingresado ya existe!", "error");
                                                }
                                            }).error(function (e) {
                                                swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                            });
                                        }
                                    }

                                };
                                scope.crearCuenta();
                            });
                            event.preventDefault();
                        }
                    });
                };
            }]);
//'use strict'
//app
//        .controller('loginMenu', ['$state', '$scope', 'config', 'UsuarioService', 'PersonaService', '$cookies', '$base64', function ($state, $scope, config, UsuarioService, PersonaService, $cookies, $base64) {
//
////                $scope.user = $cookies.getObject("usuarioLogueado");
//                $scope.user = config.userLog;
////                $scope.estado = $cookies.get("estado");
//
////                console.log(config.userLog.usuarioUsuario);
////                console.log("-->> "+config.userLog);
////                console.log("estado -->> "+config.estado);
//
//                $scope.salir = function () {
//                    $cookies.remove("usuarioLogueado");
//                    $cookies.remove("estado");
////                    $state.go("login-user");
////                    location.href = "" + config.ipFront + ":8017/login";
//                };
//
//                if (config.estado === "true") {
//                    console.log("LOGUEADO EXITOSAMENTE");
//                    $scope.nomUsu = $scope.user.persona.nombrePersona;
//                    $scope.apeUsu = $scope.user.persona.apellidoPersona;
//                    $scope.userLog = $scope.user.usuarioUsuario;
//                } else {
//                    $scope.salir();
//                }
//
//                UsuarioService.getAllConfirm().success(function (data) {
//                    $scope.registerCount = " " + data;
//                }).error(function (e) {
//                    swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
//                });
//            }]);