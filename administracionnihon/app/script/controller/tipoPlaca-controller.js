'use strict'
app
        .controller('listTipoPlacaCtrl', ['$state', '$scope', 'config', 'TipoPlacaService', '$cookies', '$base64', 'TipoPlacaPinService', '$http', function ($state, $scope, config, TipoPlacaService, $cookies, $base64, TipoPlacaPinService, $http) {
//                console.log("HOLAAA");

                $scope.tipoPlaca = [];
                $scope.tipoPlacaAgregar = {};
                $scope.tipoPlacaModificar = {};
                $scope.canti = 0;

                $scope.tipoPlacaAgregar = {};
//                var myMap = new Map();

//               pagination
                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
//                FIN DE PAGINACION

                $scope.listar = function () {
                    $scope.tipoPlaca = [];
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    TipoPlacaService.fetch(config.cantPaginacion, offSet).success(function (data) {
                        $scope.tipoPlaca = data;
//               pagination
                        $scope.paginar();
//               FIN DE pagination
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.paginar = function () {
                    TipoPlacaService.count().success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    TipoPlacaService.countFiltro($scope.filt).success(function (data) {
                        $scope.catidad = data;

                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    $http.get(config.backend + '/tipoPlaca/generarReporte/' + $scope.filt + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                };
                $scope.listarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }

                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    TipoPlacaService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt).success(function (data) {
                        $scope.tipoPlaca = data;
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.buscar = function () {
                    $scope.tipoPlaca = [];
                    if ($scope.filterText !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
//                    }
                };
                $scope.buscarSegunPagina = function () {
                    $scope.tipoPlaca = [];
                    if ($scope.filterText !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };

                $scope.listar();

                $scope.registrar = function () {
                    $scope.placaPin = {};
                    $scope.placaPin.tipoPlaca = {};


                    if ($scope.tipoPlacaAgregar.descriTipo === "" || $scope.tipoPlacaAgregar.descriTipo === undefined || $scope.tipoPlacaAgregar.descriTipo === null) {
                        swal("Mensaje del Sistema!", "Los datos no deben quedar vacío", "error");
                    } else {
                        TipoPlacaService.crear($scope.tipoPlacaAgregar).success(function (data) {
                            if (data !== 0) {
                                swal("Mensaje del Sistema!", "Datos Agregados exitosamente", "success");
                                $('#modalAgregar').modal('hide');
//                            $scope.tipoPlacaAgregar = {};

                                $scope.placaPin.tipoPlaca.idTipoPlaca = data;
                                $scope.listar();
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Agregados, verifique que los campos no esten vacíos", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };

                $scope.atras = function () {
                    location.reload();
                };

                $scope.eliminar = function (tipoPlaca) {
                    swal({
                        title: "Mensaje del Sistema",
                        text: "Seguro que quiere eliminar el Tipo de Placa " + tipoPlaca.descriTipo + "?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    TipoPlacaService.eliminar(tipoPlaca.idTipoPlaca).success(function (data) {
//                                    TipoPlacaService.modEliminar(tipoPlaca).success(function (data) {
                                        if (data === true) {
                                            swal('Eliminado!',
                                                    'Haz eliminado exitosamente.',
                                                    'success'
                                                    );
                                            $scope.listar();
                                        } else {
                                            swal("Mensaje del Sistema!", "Los datos no han podido ser eliminados, verifique en Placa la existencia de Tipo Placa", "error");
                                        }
                                    }).error(function (e) {
                                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                    });
                                } else {
                                    swal("Mensaje del Sistema", "Haz cancelado la operación!", "error");
                                }
                            });
                };

                $scope.agregar = function () {
                    $scope.tipoPlacaAgregar = {};
                };

            }]);