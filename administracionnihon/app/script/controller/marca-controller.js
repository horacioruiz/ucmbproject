'use strict'
app
        .controller('listMarcasCtrl', ['$state', '$scope', 'config', 'MarcaService', '$cookies', '$base64', '$http', function ($state, $scope, config, MarcaService, $cookies, $base64, $http) {
//                console.log("HOLAAA");

                $scope.marcas = [];
                $scope.marcaAgregar = {};
                $scope.marcaModificar = {};

                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
//                FIN DE PAGINACION

                $scope.listar = function () {
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    MarcaService.fetch(config.cantPaginacion, offSet).success(function (data) {
                        $scope.marcas = data;
                        $scope.paginar();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.paginar = function () {
                    MarcaService.count().success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    MarcaService.countFiltro($scope.filt).success(function (data) {
                        $scope.catidad = data;

                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.listarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }

                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    MarcaService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt).success(function (data) {
                        $scope.marcas = data;
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }
                
                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }

                    $http.get(config.backend + '/marca/generarReporte/' + $scope.filt + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                }

                $scope.buscar = function () {
                    $scope.marcas = [];
                    if ($scope.filterText !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
//                    }
                };
                $scope.buscarSegunPagina = function () {
                    $scope.marcas = [];
                    if ($scope.filterText !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };

                $scope.registrar = function () {
//                    alert($scope.rolAgregar.descripcionRolAgregar);
                    if ($scope.marcaAgregar.descriMarca === "" || $scope.marcaAgregar.descriMarca === undefined || $scope.marcaAgregar.descriMarca === null) {
                        swal("Mensaje del Sistema!", "Los datos no deben quedar vacío", "error");
                    } else {
                        $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                        $scope.marcaAgregar.usuAltaMarca = $scope.jsonUsu.usuarioUsuario;
                        $scope.marcaAgregar.usuModMarca = $scope.jsonUsu.usuarioUsuario;
                        MarcaService.crear($scope.marcaAgregar).success(function (data) {
                            if (data === true) {
                                swal("Mensaje del Sistema!", "Datos Agregados exitosamente", "success");
                                $('#modalAgregar').modal('hide');
                                $scope.marcaAgregar = {};
                                $scope.listar();
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Agregados", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };

                $scope.atras = function () {
                    location.reload();
                };

                $scope.agregar = function () {
                    $scope.marcaAgregar = {};
                };

                $scope.actualizar = function () {
//                    alert($scope.rolAgregar.descripcionRolAgregar);
                    if ($scope.marcaModificar.descriMarca === "" || $scope.marcaModificar.descriMarca === undefined || $scope.marcaModificar.descriMarca === null) {
                        swal("Mensaje del Sistema!", "Los datos no deben quedar vacío", "error");
                    } else {
                        $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                        $scope.marcaModificar.usuModMarca = $scope.jsonUsu.usuarioUsuario;
                        MarcaService.actualizar($scope.marcaModificar).success(function (data) {
                            if (data === true) {
                                swal("Mensaje del Sistema!", "Datos Modificados exitosamente", "success");
                                $('#modalModificar').modal('hide');
                                $scope.marcaModificar = {};
                                $scope.listar();
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Modificados", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };

                $scope.modificar = function (marc) {
//                    $scope.marcaModificar = marcas;
                    $scope.marcaModificar.descriMarca = marc.descriMarca;
                    $scope.marcaModificar.idMarca = marc.idMarca;
                    if (marc.estadoMarca === true) {
                        $("#estadoMarcaModificar").prop("checked", true);
                    } else {
                        $("#estadoMarcaModificar").prop("checked", false);
                    }
                    $scope.marcaModificar.descriMarca = marc.descriMarca;
                    $("#modalModificar").modal("show");
                };

                $scope.eliminar = function (marc) {
//                    $scope.datoEliminar = {};
//                    $scope.datoEliminar.idMarca = marc.idMarca;

                    swal({
                        title: "Mensaje del Sistema",
                        text: "Seguro que quiere eliminar al usuario " + marc.descriMarca + "?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    MarcaService.eliminar(marc.idMarca).success(function (data) {
//                                    MarcaService.modEliminar(marc).success(function (data) {
                                        if (data === true) {
                                            swal('Eliminado!',
                                                    'Haz eliminado exitosamente.',
                                                    'success'
                                                    );
                                            $scope.listar();
                                        } else {
                                            swal("Mensaje del Sistema!", "No se pudo eliminar Marca, verifique existencia en Componente", "error");
                                        }
                                    }).error(function (e) {
                                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                    });
                                } else {
                                    swal("Mensaje del Sistema", "Haz cancelado la operación!", "error");
                                }
                            });
                };

//                $scope.eliminar = function (marc) {
//
//                    $scope.datoEliminar = {};
//                    $scope.datoEliminar.idMarca = marc.idMarca;
//
//                    var r = confirm("Seguro que quiere eliminar " + marc.descriMarca + "?");
//                    if (r === true) {
//                        MarcaService.modEliminar($scope.datoEliminar).success(function (data) {
//                            if (data === true) {
//                                swal("Mensaje del Sistema!", "Datos Modificados exitosamente", "success");
////                                $('#modalModificar').modal('hide');
//                                $scope.marcaModificar = {};
//                                $scope.listar();
//                            } else {
//                                swal("Mensaje del Sistema!", "Datos no Modificados", "error");
//                            }
//                        }).error(function (e) {
//                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
//                        });
//                    } else {
//                        swal('Cancelado',
//                                'Haz cancelado la eliminación',
//                                'error'
//                                );
//                    }
//
//                };

                $scope.listar();
//                $scope.setImage = function (estado) {
//                    console.log("HOLAAA " + estado);
//                    $scope.estado = false;
//                };

            }]);