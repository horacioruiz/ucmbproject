'use strict'
app
        .controller('adminEdificioCtrl', ['$state', '$scope', 'config', 'UsuarioEdificioService', 'HumoService', 'MovimientoService', '$cookies', '$base64', '$filter', '$location', '$rootScope', '$http', 'UsuarioService', 'EdificioService', 'AdminService', function ($state, $scope, config, UsuarioEdificioService, HumoService, MovimientoService, $cookies, $base64, $filter, $location, $rootScope, $http, UsuarioService, EdificioService, AdminService) {
                $scope.usuario = {};
                $scope.usuarioAgregar = {};
                $scope.usuarioModificar = {};
                $scope.aModificar = "";
                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.nomUsu = "";
                $scope.apeUsu = "";
                $("#idUsu").val("");
                $scope.edificios = [];
                $scope.usuarioEdificio = {};
                $scope.usuarioEdificio.usuario = {};
                $scope.usuarioEdificio.edificio = {};
                $scope.usuNomApe = "";
                $scope.usuUser = "";
                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.listar = function () {
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    UsuarioService.fetch(config.cantPaginacion, offSet).success(function (data) {
                        angular.forEach(data, function (value, key) {
                            UsuarioEdificioService.getByIdUsu(value.idUsuario).success(function (datos) {
                                if (datos.length !== 0) {
                                    value.estado = true;
                                } else {
                                    value.estado = false;
                                }
                                $scope.usuario.push(value);
                            }).error(function (e) {
                                console.log("ERROR...");
                            });
                        });
                        $scope.paginar();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.limpiar = function () {
                    $scope.filterText = "";
                    $scope.nomUsu = "";
                    $scope.apeUsu = "";
                };
                $scope.paginar = function () {
                    UsuarioService.count().success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());
                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.nomUsu === "" || $scope.nomUsu === undefined) {
                        $scope.nom = null;
                    } else {
                        $scope.nom = $scope.nomUsu;
                    }
                    if ($scope.apeUsu === "" || $scope.apeUsu === undefined) {
                        $scope.ape = null;
                    } else {
                        $scope.ape = $scope.apeUsu;
                    }
                    UsuarioService.countFiltro($scope.filt, $scope.nom, $scope.ape).success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());
                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.listarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.nomUsu === "" || $scope.nomUsu === undefined) {
                        $scope.nom = null;
                    } else {
                        $scope.nom = $scope.nomUsu;
                    }
                    if ($scope.apeUsu === "" || $scope.apeUsu === undefined) {
                        $scope.ape = null;
                    } else {
                        $scope.ape = $scope.apeUsu;
                    }
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    UsuarioService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt, $scope.nom, $scope.ape).success(function (data) {
//                        $scope.usuario = data;
                        angular.forEach(data, function (value, key) {
                            UsuarioEdificioService.getByIdUsu(value.idUsuario).success(function (datos) {
                                if (datos.length !== 0) {
                                    value.estado = true;
                                } else {
                                    value.estado = false;
                                }
                                $scope.usuario.push(value);
                            }).error(function (e) {
                                console.log("ERROR...");
                            });
                        });
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.buscar = function () {
                    $scope.usuario = [];
                    if ($scope.filterText !== "" || $scope.nomUsu !== "" || $scope.apeUsu !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };
                $scope.buscarSegunPagina = function () {
                    $scope.usuario = [];
                    if ($scope.filterText !== "" || $scope.nomUsu !== "" || $scope.apeUsu !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };

                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.nomUsu === "" || $scope.nomUsu === undefined) {
                        $scope.nom = null;
                    } else {
                        $scope.nom = $scope.nomUsu;
                    }
                    if ($scope.apeUsu === "" || $scope.apeUsu === undefined) {
                        $scope.ape = null;
                    } else {
                        $scope.ape = $scope.apeUsu;
                    }
                    $http.get(config.backend + '/usuarioEdificio/generarReporte/' + $scope.filt + '/' + $scope.nom + '/' + $scope.ape + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                };

                $scope.detalle = function (ca) {
                    $("#idUsu").val(ca.idUsuario);
                    $scope.edificios = [];
                    $("#tablaDetalle").html("");
                    var dataTable = "";
                    $scope.usuNomApe = ca.persona.nombrePersona + " " + ca.persona.apellidoPersona;
                    $scope.usuUser = ca.usuarioUsuario;
                    UsuarioEdificioService.getByIdUsu(ca.idUsuario).success(function (data) {
//                        if (parseInt(data.length) !== 0) {
                        EdificioService.listar().success(function (retorno) {
                            var j = 0;
                            angular.forEach(retorno, function (valor, llave) {
                                $("#numMax").val(j);
                                j++;
                                dataTable += "<tr><td>" + j + "</td>";
                                dataTable += "<td style='text-align: center'>" + valor.descriEdificio + "</td>";
                                var keepGoing = true;
                                var estadoTabla = "";
                                if (parseInt(data.length) !== 0) {
                                    for (var i = 0; i < data.length; i++) {
                                        if (keepGoing) {
                                            if (valor.descriEdificio === data[i].edificio.descriEdificio) {
//                                              
                                                estadoTabla = "<td style='text-align: center'><input type='checkbox' onclick='cambioChkUsuEdi(\"" + valor.idEdificio + "_" + $("#idUsu").val() + "\")' id='" + valor.idEdificio + "_" + $("#idUsu").val() + "' value='true' checked='true'> " +
                                                        " <span style='display: none'>" + valor.idEdificio + "_" + $("#idUsu").val() + "</span></td>";
                                                keepGoing = false;
                                            } else {
                                                estadoTabla = "<td style='text-align: center'><input type='checkbox' onclick='cambioChkUsuEdi(\"" + valor.idEdificio + "_" + $("#idUsu").val() + "\")' id='" + valor.idEdificio + "_" + $("#idUsu").val() + "' value='false' > " +
                                                        " <span style='display: none'>" + valor.idEdificio + "_" + $("#idUsu").val() + "</span></td>";
                                            }
                                        }
                                    }
                                } else {
                                    estadoTabla = "<td style='text-align: center'><input type='checkbox' onclick='cambioChkUsuEdi(\"" + valor.idEdificio + "_" + $("#idUsu").val() + "\")' id='" + valor.idEdificio + "_" + $("#idUsu").val() + "' value='false' > " +
                                            " <span style='display: none'>" + valor.idEdificio + "_" + $("#idUsu").val() + "</span></td>";
                                }
                                dataTable += estadoTabla + "</tr>";
                            });
                            $("#tablaDetalle").append(dataTable);
                            $('#modalDetalles').modal('show');
                        }).error(function (e) {

                        });
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $("#cb1").click(function () {
                    if ($(this).is(':checked'))
                        alert("checked");
                });
                $scope.registrarUsuarioEdificio = function () {
                    var l = 0;
                    UsuarioEdificioService.deleteByIdUsuario($("#idUsu").val()).success(function (data) {
                        if (data === true) {
                            console.log("DATOS ELIMINADOS EXITOSAMENTE");
                            $("#tablaDetalle").find('tr').each(function (i, el) {
                                $scope.usuarioEdificio = {};
                                $scope.usuarioEdificio.usuario = {};
                                $scope.usuarioEdificio.edificio = {};
                                var $tds = $(this).find('td');
//                        var fields = $tds.find("input").val().split('_');
                                var fields = $tds.eq(2).find("input").val() + "-" + $tds.eq(2).find("span").html(); //find("input").val().replace(" ", "");

//                        true - 16_1
                                var data = fields.split('-');
                                var status = data[0];
                                var dataReturn = data[1].split('_');
                                var edi = dataReturn[0];
                                var user = dataReturn[1];
                                if (status === "true") {
                                    $scope.usuarioEdificio.usuario.idUsuario = user;
                                    $scope.usuarioEdificio.edificio.idEdificio = edi;
                                    UsuarioEdificioService.crear($scope.usuarioEdificio).success(function (data) {
                                        if (data === true) {
                                            console.log("DATOS INSERTADOS EXITOSAMENTE..." + fields);
                                        }
                                    }).error(function (e) {

                                    });
                                }
                                console.log("esto -> " + $("#numMax").val());
                                if (parseInt($("#numMax").val()) === parseInt(l)) {
                                    $scope.usuario = [];
                                    swal("Mensaje del Sistema!", "Datos actualizados exitosamente!", "success");
                                    $scope.listar();
                                    $('#modalDetalles').modal('hide');
                                }
                                l++;
                            });
                        }
                    }).error(function (e) {

                    })
                };
                $scope.atras = function () {
                    location.reload();
                };
                $scope.salir = function () {
                    $('#modalDetalles').modal('hide');
                };
                $scope.$watch('filterText', function () {
                    $("#paginaActual").val("1");
                })
                $scope.$watch('nomUsu', function () {
                    $("#paginaActual").val("1");
                })
                $scope.$watch('apeUsu', function () {
                    $("#paginaActual").val("1");
                })
                $scope.buscar();
            }])
        .controller('usuarioEdificioCtrl', ['$state', '$scope', 'config', 'UsuarioEdificioService', 'HumoService', 'MovimientoService', 'SensoresService', 'TemperaturaService', 'PlanService', '$cookies', '$base64', '$filter', '$location', '$rootScope', '$http', 'UsuarioService', 'EdificioService', 'NodoService', 'AdminService', '$timeout', '$interval', '$compile', function ($state, $scope, config, UsuarioEdificioService, HumoService, MovimientoService, SensoresService, TemperaturaService, PlanService, $cookies, $base64, $filter, $location, $rootScope, $http, UsuarioService, EdificioService, NodoService, AdminService, $timeout, $interval, $compile) {
//                $(".alert").fadeOut("fast");
                $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                $scope.userLog = $scope.jsonUsu.usuarioUsuario;
                var datos = "";
                $scope.dependencia = [];
                $scope.nomEdifi = "";
                $scope.humo = [];
                $scope.movimientos = [];
                $("#senHumo").hide();
                $("#senMov").hide();

//                $('.popovers').popover();
//                window.setTimeout(function () {
//                    $(".alerta").fadeTo(5000, 500).slideUp(500, function () {
//                        $(this).remove();
//                    });
//                    // 500 : Time will remain on the screen
//                }, 500);
//                
//                $scope.mapeoDatos = 0;

                $scope.cargarAdministracion = function () {
                    UsuarioEdificioService.getByIdUsu($scope.jsonUsu.idUsuario).success(function (data) {
                        var val = 1;
                        angular.forEach(data, function (valor, llave) {
                            datos += '<div class="col-xs-12 col-md-6 col-lg-3">';
                            datos += '<a href="./edificio02.html?id=' + valor.edificio.idEdificio + '">';
                            datos += '<div class="panel panel-blue panel-widget ">';
                            datos += '<div class="row no-padding">';
                            datos += '<div class="col-sm-3 col-lg-5 widget-left">';
                            datos += '<svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg>';
                            datos += '</div>';
                            datos += '<div class="col-sm-9 col-lg-7 widget-right">';
                            datos += '<div class="large">' + val++ + '</div>';
                            datos += '<div class="text-muted">' + valor.edificio.descriEdificio + '</div>';
                            datos += '</div>';
                            datos += '</div>';
                            datos += '</div>';
                            datos += '</a>';
                            datos += '</div>';
                        });
                        $("#edificios").append(datos);
                    }).error(function () {
                    });
                };
                $interval(function () {
                    var cantidad = 0;
                    var myMapUsuEdi = new Map();
//                    console.log("URL: " + document.URL);
                    //alert(config.ipFront);
                    var pag = parseInt(document.URL.replace("http://" + config.ipFront + ":8017/partial/monitoreo/admin/edificio02.html?id=", ""));
                    if (isNumeric(pag)) {
                        $("#detalleDatos").css("display", "inline");
                        $("#edificios").css("display", "none");
                        var panel = '';
                        NodoService.getByCantidadDependencia(pag).success(function (dato) {
                            if (dato !== 0) {
                                cantidad = dato;
                                var valor = Math.ceil(cantidad / 2);
                                NodoService.getByEdificio(pag).success(function (elemento) {
                                    if (elemento.length !== 0) {
                                        angular.forEach(elemento, function (value, key) {
                                            var mapeo = myMapUsuEdi.get(value.dependencia.descriDependencia);
                                            if (mapeo === undefined) {
                                                myMapUsuEdi.set(value.dependencia.descriDependencia, pag + "-" + value.dependencia.idDependencia);
                                            }
                                        });
                                    }
                                    var num = 1;
                                    var num = 1;
                                    var depe = "";
                                    var idEdi = "";
                                    myMapUsuEdi.forEach(function (item, key, mapObj) {
                                        var fields = item.toString().split('-');
                                        idEdi = fields[0];
                                        var idDepe = fields[1];
                                        depe += idDepe + "-";
                                    });
                                    NodoService.listarPorEdiDepe(depe, idEdi).success(function (d) {
//                                        console.log("***-->> " + d. + "<<--***");
                                        $scope.nomEdifi = "- " + d[0].edificio.descriEdificio;
                                        $("#nomEdifi").css("display", "inline");
                                        var myMapDepe = new Map();
                                        var numAnterior = 0;
                                        var valorNumerico = 0;
                                        angular.forEach(d, function (value, key) {
                                            valorNumerico++;
//                                            var mapeo = myMapDepe.get(value.dependencia.descriDependencia);
//                                            console.log("***-->> " + value.idNodo + " - " + value.dependencia.idDependencia + "<<--***");
//                                            console.log("");
                                            if (numAnterior !== value.dependencia.idDependencia) {
                                                if (numAnterior !== 0) {
                                                    panel += '</div></div></div></div></div></div></div></div>';
                                                }
                                                panel += '<div class="col-sm-6">';
                                                panel += '<div class="container">';
                                                panel += '<div class="row">';
                                                panel += '<div class="col-md-6">';
                                                panel += '<div class="panel panel-primary">';
                                                panel += '<div class="panel-heading">';
                                                panel += '<h4 class="panel-title">';
                                                panel += '<span class="glyphicon glyphicon-home"></span>' + value.dependencia.descriDependencia + '</h4>';
                                                panel += '</div>';
                                                panel += '<div class="panel-body">';
                                                panel += '<div class="row">';
                                                panel += '<div class="col-xs-12 col-md-12">';
                                                numAnterior = value.dependencia.idDependencia;
                                            }
                                            var btn = "btn-default";
                                            // value.estadoComponente: //LE SETEO UN VALOR QUE RECUPERA DE ADMINISTRACION PARA SABER EN QUE ESTADO ESTA EL COMPONENTE,
                                            //SOLO UTILIZO estadoComponente COMO VARIABLE PARA SABER SU ESTADO< NADA TIENE QUE VER CON EL VALOR QUE ESTA EN LA BD
                                            if (value.estadoComponente === true) {
                                                btn = "btn-primary";
                                            }
//                                                   
                                            if (value.estadoNodo === false) {
                                                panel += '<a href="#" class="btn btn-default btn-lg" role="button" disabled style="color: #C0C0C0"><span>' + value.placa.ipPlaca + '</span><br><img src="../../../img/admin/' + value.componente.tipoComponente.descriTipoComp + '.png" alt="" style="width: 22px; height: 22px"/> <br/>' + value.descriNodo + '</a>&nbsp;&nbsp;';
                                            } else {
                                                panel += '<a href="#" ng-click="activarOperacion(' + value.idNodo + ',' + value.edificio.idEdificio + ')" class="btn ' + btn + ' btn-lg" role="button"><span>' + value.placa.ipPlaca + '</span><br><img src="../../../img/admin/' + value.componente.tipoComponente.descriTipoComp + '.png" alt="" style="width: 22px; height: 22px"/> <br/>' + value.descriNodo + '</a>&nbsp;&nbsp;';
                                            }
                                            if (d.length === valorNumerico) {
                                                panel += '</div></div></div></div></div></div></div></div>';
                                                console.log("**** FINISH ****");
//                                                console.log(panel);
                                                $("#detalleDatos").html("");
                                                angular.element(document.getElementById('detalleDatos')).append($compile(panel)($scope));
                                                $("#detalleDatos").css("display", "inline");
                                                $("#edificios").css("display", "none");
                                                $scope.cargarSensores(idEdi);
                                                $scope.cargarTemperaturas(idEdi);
                                            }
                                        });
                                    });
//                                    console.log("");
                                }).error(function (e) {
                                });
                            }
                        }).error(function (e) {
                        });
//                        console.log("ES NUMERICO");
                    } else {
                        console.log("NO ES NUMERICO");
                    }
                }, 3000);
                function isNumeric(n) {
                    return !isNaN(parseFloat(n)) && isFinite(n);
                }

                $scope.cargarTemperaturas = function (idEdi) {
                    var datosComp = "";
                    SensoresService.listarSensores(idEdi).success(function (data) {
                        var val = 0;
                        var nod = "";
                        angular.forEach(data, function (value, key) {
                            val++;
                            datosComp += '<center><div class="col-sm-3">';
                            datosComp += '<div class="col-item">';
                            datosComp += '<div class="photo">';
                            datosComp += '<b><span style="font-size: 13px"> ' + value.nodo.dependencia.descriDependencia + '</span></b><br>';
                            datosComp += '<b><span style="font-size: 20px"><button type="button" disabled="disabled">TEMP ACTUAL: <span id="' + value.nodo.idNodo + '"> - ºC</span></button></span></b><br><br>';
                            datosComp += '<b><span style="font-size: 25px"><button type="button" disabled="disabled">TEMP MIN: <span id="' + value.nodo.idNodo + 'C"> - ºC</span></button></span></b><br>';
                            datosComp += '<b><span style="font-size: 14px">' + value.descripcion + '</span></b><br>';
                            if (value.estado === false) {
                                datosComp += '<b><span style="color: red">SENSOR DESACTIVADO</span></b><br>';
                            } else {
                                datosComp += '<b><span style="color: green">SENSOR ACTIVADO</span></b><br>';
                                nod += value.nodo.idNodo + "-";
                            }
                            datosComp += '</div></div></div></center>';
//                            datosComp += '</div>';
//                            datosComp += '';
//                            datosComp += '';
                        });
                        if (data.length === val && data.length !== 0) {
                            $("#sensoresTemp").html(datosComp);
                            $("#sens").show();
                            TemperaturaService.getLastIdNodo(parseInt(nod)).success(function (d) {
                                angular.forEach(d, function (value, key) {
                                    console.log("1) " + parseInt(Math.round(value.temperatura)) + " 2) " + (value.valorAsignado));
                                    if (Math.round(value.temperatura) >= Math.round(value.valorAsignado)) {
                                        $("#" + value.nodo.idNodo).html("<span style='color: green'>" + Math.round(value.temperatura) + "ºC </span>");
                                        $("#" + value.nodo.idNodo + "C").html("<span style='color: green'>" + Math.round(value.valorAsignado) + "ºC </span>");
                                    } else {
                                        $("#" + value.nodo.idNodo).html("<span style='color: red'>" + Math.round(value.temperatura) + "ºC </span>");
                                        $("#" + value.nodo.idNodo + "C").html("<span style='color: red'>" + Math.round(value.valorAsignado) + "ºC </span>");
                                    }
                                });
                            });
                        }
                    });
                };

                $scope.cargarSensores = function (idEdi) {
                    $("#principal").html('<span class="label label-default"><span class="glyphicon glyphicon-envelope"></span></span>');
                    $("#sensorHumo").html('<span class="label label-default"><span class="glyphicon glyphicon-envelope"></span></span>');
                    $("#sensorMov").html('<span class="label label-default"><span class="glyphicon glyphicon-envelope"></span></span>');
                    HumoService.ultimosRegistrados(idEdi).success(function (data) {
                        if (data.length !== 0) {
                            $scope.humo = data;
                            $("#sensorHumo").html('<span class="label label-danger"><span class="glyphicon glyphicon-envelope"></span></span>');
                            $("#principal").html('<span class="label label-danger"><span class="glyphicon glyphicon-envelope"></span></span>');
                        }
                    });
                    MovimientoService.ultimosRegistrados(idEdi).success(function (data) {
                        if (data.length !== 0) {
                            $scope.movimientos = data;
                            $("#sensorMov").html('<span class="label label-danger"><span class="glyphicon glyphicon-envelope"></span></span>');
                            $("#principal").html('<span class="label label-danger"><span class="glyphicon glyphicon-envelope"></span></span>');
                        }
                    });
                    $("#senHumo").show();
                    $("#senMov").show();
                };

                $scope.modalHumo = function () {
                    if ($scope.humo.length === 0) {
                        $("#detalleTablaHumo").html("<tr><td colspan='6'><center> NINGUN SENSOR DE HUMO DETECTADO... </center></td></tr>");
                    }
                    $("#modalDetallesHumo").modal("show");
                };

                $scope.modalMov = function () {
                    if ($scope.movimientos.length === 0) {
                        $("#detalleTablaMovimiento").html("<tr><td colspan='6'><center> NINGUN SENSOR DE MOVIMIENTO DETECTADO... </center></td></tr>");
                    }
                    $("#modalDetallesMovimiento").modal("show");
                };

                $scope.salirModal = function () {
                    $("#modalDetallesMovimiento").modal("hide");
                    $("#modalDetallesHumo").modal("hide");
                };

                $scope.actualizarOperaciones = function (idNodo) {
                    var cantidad = 0;
                    var myMapUsuEdi = new Map();
//                    console.log("URL: " + document.URL);
                    var pag = parseInt(idNodo);
                    if (isNumeric(pag)) {
                        $("#detalleDatos").css("display", "inline");
                        $("#edificios").css("display", "none");
                        var panel = '';
                        NodoService.getByCantidadDependencia(pag).success(function (dato) {
                            if (dato !== 0) {
                                cantidad = dato;
                                var valor = Math.ceil(cantidad / 2);
                                NodoService.getByEdificio(pag).success(function (elemento) {
                                    if (elemento.length !== 0) {
                                        angular.forEach(elemento, function (value, key) {
                                            var mapeo = myMapUsuEdi.get(value.dependencia.descriDependencia);
                                            if (mapeo === undefined) {
                                                myMapUsuEdi.set(value.dependencia.descriDependencia, pag + "-" + value.dependencia.idDependencia);
                                            }
                                        });
                                    }
                                    var num = 1;
                                    var num = 1;
                                    var depe = "";
                                    var idEdi = "";
                                    myMapUsuEdi.forEach(function (item, key, mapObj) {
                                        var fields = item.toString().split('-');
                                        idEdi = fields[0];
                                        var idDepe = fields[1];
                                        depe += idDepe + "-";
                                    });
                                    NodoService.listarPorEdiDepe(depe, idEdi).success(function (d) {
                                        $scope.nomEdifi = "- " + d[0].edificio.descriEdificio;
                                        $("#nomEdifi").css("display", "inline");
                                        var myMapDepe = new Map();
                                        var numAnterior = 0;
                                        var valorNumerico = 0;
                                        angular.forEach(d, function (value, key) {
                                            valorNumerico++;
                                            var mapeo = myMapDepe.get(value.dependencia.descriDependencia);
                                            if (numAnterior !== value.dependencia.idDependencia) {
                                                if (numAnterior !== 0) {
                                                    panel += '</div></div></div></div></div></div></div></div>';
                                                }
                                                panel += '<div class="col-sm-6">';
                                                panel += '<div class="container">';
                                                panel += '<div class="row">';
                                                panel += '<div class="col-md-6">';
                                                panel += '<div class="panel panel-primary">';
                                                panel += '<div class="panel-heading">';
                                                panel += '<h4 class="panel-title">';
                                                panel += '<span class="glyphicon glyphicon-home"></span>' + value.dependencia.descriDependencia + '</h4>';
                                                panel += '</div>';
                                                panel += '<div class="panel-body">';
                                                panel += '<div class="row">';
                                                panel += '<div class="col-xs-12 col-md-12">';
                                                numAnterior = value.dependencia.idDependencia;
                                            }
                                            var btn = "btn-default";
                                            // value.estadoComponente: //LE SETEO UN VALOR QUE RECUPERA DE ADMINISTRACION PARA SABER EN QUE ESTADO ESTA EL COMPONENTE,
                                            //SOLO UTILIZO estadoComponente COMO VARIABLE PARA SABER SU ESTADO< NADA TIENE QUE VER CON EL VALOR QUE ESTA EN LA BD
                                            if (value.estadoComponente === true) {
                                                btn = "btn-primary";
                                            }
//                                                   
                                            if (value.estadoNodo === false) {
                                                panel += '<a href="#" class="btn btn-default btn-lg" role="button" disabled style="color: #C0C0C0"><span>' + value.placa.ipPlaca + '</span><br><img src="../../../img/admin/' + value.componente.tipoComponente.descriTipoComp + '.png" alt="" style="width: 22px; height: 22px"/> <br/>' + value.descriNodo + '</a>&nbsp;&nbsp;';
                                            } else {
                                                panel += '<a href="#" ng-click="activarOperacion(' + value.idNodo + ',' + value.edificio.idEdificio + ')" class="btn ' + btn + ' btn-lg" role="button"><span>' + value.placa.ipPlaca + '</span><br><img src="../../../img/admin/' + value.componente.tipoComponente.descriTipoComp + '.png" alt="" style="width: 22px; height: 22px"/> <br/>' + value.descriNodo + '</a>&nbsp;&nbsp;';
                                            }
                                            if (d.length === valorNumerico) {
                                                panel += '</div></div></div></div></div></div></div></div>';
                                                console.log("**** FINISH ****");
//                                                console.log(panel);
                                                $("#detalleDatos").html("");
                                                angular.element(document.getElementById('detalleDatos')).append($compile(panel)($scope));
                                                $("#detalleDatos").css("display", "inline");
                                                $("#edificios").css("display", "none");
                                                $scope.cargarSensores(idEdi);
                                                $scope.cargarTemperaturas(idEdi);
                                            }
                                        });
                                    });
                                }).error(function (e) {
                                });
                            }
                        }).error(function (e) {
                        });
//                        console.log("ES NUMERICO");
                    } else {
                        console.log("NO ES NUMERICO...");
                    }
                };
                function cerrar() {
                    $(".alerta").css("display", "none");
                }
                $scope.activarOperacion = function (idNodo, idEdi) {
//                    alert("HOLA " + idNodo + " - " + idEdi);
                    cerrar();
                    $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                    var user = $scope.jsonUsu.usuarioUsuario;
                    AdminService.cambEstadoAct(idNodo, 9999, 9999, 9999, 9999, user).success(function (data) {
//                        location.href = "http://" + config.ipFront + ":8017/partial/monitoreo/admin/edificio02.html?id=" + idEdif;
//                        $("#edificios").fadeOut("fast");
//                        $("#detalleDatos").fadeIn("slow");
//                        console.log("IMPRESION: ");
//                        console.log(data);
                        if (parseInt(data) === -1) {
//                            $(".alert").slideUp(300).delay(800).fadeIn(400);
                            $("#alertJS").html('<div style="display: none" class="popupunder alerta alert-danger"><strong>Mensaje del Sistema! : </strong> Conexión Perdida!!</div>')
//                            $("#mensajeAlert").html("<strong>Mensaje del Sistema! : </strong> Conexión Perdida!!")
                            $(".alerta").css("display", "inline");
                            setTimeout(cerrar, 1500);
//                            $(".alert").fadeTo(2000, 500).slideUp(500, function () {
//                                $(this).remove();
//                            });
//                            $('.popovers').popover();
//                            swal("Mensaje del Sistema!", "Problemas de conexión con el Arduino!", "error");
                        } else if (parseInt(data) === 9090) {
                            PlanService.recuperarPlan(idNodo).success(function (d) {
                                if (d.horaFinPlan !== null) {
                                    $("#alertJS").html('<div style="display: none" class="popupunder alerta alert-success"><strong>Mensaje del Sistema! </strong>Parámetro activo. Hasta: ' + d.fechaFinPlan + ' ' + d.horaFinPlan + '</div>')
//                                    $("#mensajeAlert").html("<strong>Mensaje del Sistema! </strong>Parámetro activo. Hasta: " + d.fechaFinPlan + " " + d.horaFinPlan);
                                    $(".alerta").css("display", "inline");
                                    setTimeout(cerrar, 2000);
//                                    swal("Mensaje del Sistema!", "No es posible realizar acciones! Existe un plan seleccionado en este momento. Hasta la fecha: " + d.fechaFinPlan + " " + d.horaFinPlan, "error");
                                }
                            });
                        } else {
                            $scope.actualizarOperaciones(idEdi);
                        }

                    });
                };
                $rootScope.$on("$locationChangeStart", function (event, data) {
                    var cantidad = 0;
                    var myMapUsuEdi = new Map();
                    //alert(config.ipFront)
                    var pag = parseInt(data.replace("http://" + config.ipFront + ":8017/partial/monitoreo/admin/edificio02.html?id=", ""));
                    var pag2 = data.replace("http://" + config.ipFront + ":8017/partial/monitoreo/admin/edificio02.html?id_evento=", "");
//                    var idEdif = 0;
//                    if (pag2.match("_")) {
//                        var array = pag2.split('_');
//                        pag2 = array[0];
//                        idEdif = array[1];
//                    }

                    if (!isNaN(pag)) {
                        $("#detalleDatos").css("display", "inline");
                        $("#edificios").css("display", "none");
                        var panel = '';
                        NodoService.getByCantidadDependencia(pag).success(function (dato) {
                            if (dato !== 0) {
                                cantidad = dato;
                                var valor = Math.ceil(cantidad / 2);
                                NodoService.getByEdificio(pag).success(function (elemento) {
                                    if (elemento.length !== 0) {
                                        angular.forEach(elemento, function (value, key) {
                                            var mapeo = myMapUsuEdi.get(value.dependencia.descriDependencia);
                                            if (mapeo === undefined) {
                                                myMapUsuEdi.set(value.dependencia.descriDependencia, pag + "-" + value.dependencia.idDependencia);
                                            }
                                        });
                                    }
                                    var num = 1;
                                    var num = 1;
                                    var depe = "";
                                    var idEdi = "";
                                    myMapUsuEdi.forEach(function (item, key, mapObj) {
                                        var fields = item.toString().split('-');
                                        idEdi = fields[0];
                                        var idDepe = fields[1];
                                        depe += idDepe + "-";
                                    });
                                    NodoService.listarPorEdiDepe(depe, idEdi).success(function (d) {
                                        $scope.nomEdifi = "- " + d[0].edificio.descriEdificio;
                                        $("#nomEdifi").css("display", "inline");
                                        var myMapDepe = new Map();
                                        var numAnterior = 0;
                                        var valorNumerico = 0;
                                        angular.forEach(d, function (value, key) {
                                            valorNumerico++;
                                            var mapeo = myMapDepe.get(value.dependencia.descriDependencia);
                                            if (numAnterior !== value.dependencia.idDependencia) {
                                                if (numAnterior !== 0) {
                                                    panel += '</div></div></div></div></div></div></div></div>';
                                                }
                                                panel += '<div class="col-sm-6">';
                                                panel += '<div class="container">';
                                                panel += '<div class="row">';
                                                panel += '<div class="col-md-6">';
                                                panel += '<div class="panel panel-primary">';
                                                panel += '<div class="panel-heading">';
                                                panel += '<h4 class="panel-title">';
                                                panel += '<span class="glyphicon glyphicon-home"></span>' + value.dependencia.descriDependencia + '</h4>';
                                                panel += '</div>';
                                                panel += '<div class="panel-body">';
                                                panel += '<div class="row">';
                                                panel += '<div class="col-xs-12 col-md-12">';
                                                numAnterior = value.dependencia.idDependencia;
                                            }
                                            var btn = "btn-default";
                                            // value.estadoComponente: //LE SETEO UN VALOR QUE RECUPERA DE ADMINISTRACION PARA SABER EN QUE ESTADO ESTA EL COMPONENTE,
                                            //SOLO UTILIZO estadoComponente COMO VARIABLE PARA SABER SU ESTADO< NADA TIENE QUE VER CON EL VALOR QUE ESTA EN LA BD
                                            if (value.estadoComponente === true) {
                                                btn = "btn-primary";
                                            }
//                                                   
                                            if (value.estadoNodo === false) {
                                                panel += '<a href="#" class="btn btn-default btn-lg" role="button" disabled style="color: #C0C0C0"><span>' + value.placa.ipPlaca + '</span><br><img src="../../../img/admin/' + value.componente.tipoComponente.descriTipoComp + '.png" alt="" style="width: 22px; height: 22px"/> <br/>' + value.descriNodo + '</a>&nbsp;&nbsp;';
                                            } else {
                                                panel += '<a href="#" ng-click="activarOperacion(' + value.idNodo + ',' + value.edificio.idEdificio + ')" class="btn ' + btn + ' btn-lg" role="button"><span>' + value.placa.ipPlaca + '</span><br><img src="../../../img/admin/' + value.componente.tipoComponente.descriTipoComp + '.png" alt="" style="width: 22px; height: 22px"/> <br/>' + value.descriNodo + '</a>&nbsp;&nbsp;';
                                            }
                                            if (d.length === valorNumerico) {
                                                panel += '</div></div></div></div></div></div></div></div>';
                                                console.log("**** FINISH ****");
//                                                console.log(panel);
                                                $("#detalleDatos").html("");
//                                                console.log("-> " + panel)
                                                angular.element(document.getElementById('detalleDatos')).append($compile(panel)($scope));
                                                $("#detalleDatos").css("display", "inline");
                                                $("#edificios").css("display", "none");
                                                $scope.cargarSensores(idEdi);
                                                $scope.cargarTemperaturas(idEdi);
                                            }
                                        });
                                    });
                                }).error(function (e) {
                                });
                            }
                        }).error(function (e) {
                        });
                    } else {
                        $scope.cargarAdministracion();
                    }
                });
                $scope.verDetalle = function (id) {
                    alert(id);
                }

            }])
        .controller('UsuarioEdificioDetallesCtrl', ['$state', '$scope', 'config', 'UsuarioEdificioService', '$cookies', '$base64', '$filter', '$location', '$rootScope', '$http', 'UsuarioService', 'EdificioService', function ($state, $scope, config, UsuarioEdificioService, $cookies, $base64, $filter, $location, $rootScope, $http, UsuarioService, EdificioService) {
//                $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
//                $scope.userLog = $scope.jsonUsu.usuarioUsuario;
//                var datos = "";
//                UsuarioEdificioService.getByIdUsu($scope.jsonUsu.idUsuario).success(function (data) {
//                    var val = 0;
//                    angular.forEach(data, function (valor, llave) {
//                        datos += '<div class="col-xs-12 col-md-6 col-lg-3">';
//                        datos += '<a href="./detalleControles.html?id=' + valor.edificio.idEdificio + '">';
//                        datos += '<div class="panel panel-blue panel-widget ">';
//                        datos += '<div class="row no-padding">';
//                        datos += '<div class="col-sm-3 col-lg-5 widget-left">';
//                        datos += '<svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg>';
//                        datos += '</div>';
//                        datos += '<div class="col-sm-9 col-lg-7 widget-right">';
//                        datos += '<div class="large">' + val++ + '</div>';
//                        datos += '<div class="text-muted">' + valor.edificio.descriEdificio + '</div>';
//                        datos += '</div>';
//                        datos += '</div>';
//                        datos += '</div>';
//                        datos += '</a>';
//                        datos += '</div>';
//                    });
//                    $("#edificios").html(datos);
//                }).error(function () {
//                });
//
//                $scope.verDetalle = function (id) {
//                    alert(id);
//                }
                $rootScope.$on("$locationChangeStart", function (event, data) {
                    //alert(config.ipFront)
                    var pag = parseInt(data.replace("http://" + config.ipFront + ":8017/partial/monitoreo/admin/detalleControles.html?id", ""));
                    console.log("-> " + pag)
                    if (!isNaN(pag)) {
                        $("#paginaActual").val(pag);
                        console.log("->pag " + pag)
                        $scope.buscarSegunPagina();
                    }
                });
            }]);

