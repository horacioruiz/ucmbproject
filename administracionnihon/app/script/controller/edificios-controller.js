'use strict'
app
        .controller('listEdificioCtrl', ['$state', '$scope', 'config', 'EdificioService', '$cookies', '$base64', 'AndeCategoriaService', 'PersonaService', 'TipoEdificioService', 'CiudadService', '$http', '$rootScope', function ($state, $scope, config, EdificioService, $cookies, $base64, AndeCategoriaService, PersonaService, TipoEdificioService, CiudadService, $http, $rootScope) {
                $scope.edificios = {};
                $scope.edificioAgregar = {};
                $scope.edificioModificar = {};

                $scope.edificioAgregar.andeCategoria = {};
                $scope.edificioAgregar.tipoEdificio = {};
                $scope.edificioAgregar.persona = {};
                $scope.edificioAgregar.ciudad = {};


                $scope.edificioModificar.andeCategoria = {};
                $scope.edificioModificar.tipoEdificio = {};
                $scope.edificioModificar.persona = {};
                $scope.edificioModificar.ciudad = {};

                var myMapPlaca = new Map();

                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };

                $scope.aModificar = "";

//                $scope.limpiar = function () {
//                    $scope.filterText = "";
//                    $scope.tipo = "";
//                    $scope.ciudad = "";
//                };

//               pagination

                $scope.paginar = function () {
                    EdificioService.count().success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $rootScope.$on("$locationChangeStart", function (event, data) {
                    var pag = parseInt(data.replace("http://" + config.ipFront + ":8017/menu.html#/:pagina=", ""));
                    if (!isNaN(pag)) {
                        $("#paginaActual").val(pag);
                        $scope.buscarSegunPagina();
                    }
                });

                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.tipo === "" || $scope.tipo === undefined) {
                        $scope.tip = null;
                    } else {
                        $scope.tip = $scope.tipo;
                    }
                    if ($scope.ciudad === "" || $scope.ciudad === undefined) {
                        $scope.ciu = null;
                    } else {
                        $scope.ciu = $scope.ciudad;
                    }
                    EdificioService.countFiltro($scope.filt, $scope.tip, $scope.ciu).success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
//                    $("#paginacionesEdificios").html('<ul class="pagination"><li class="disabled"><a href="#">&laquo;</a></li><li class="active"><a href="#">1</a></li><li ><a href="#">2</a></li><li><a href="#">&raquo;</a></li></ul>');
                };

                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.tipo === "" || $scope.tipo === undefined) {
                        $scope.tip = null;
                    } else {
                        $scope.tip = $scope.tipo;
                    }
                    if ($scope.ciudad === "" || $scope.ciudad === undefined) {
                        $scope.ciu = null;
                    } else {
                        $scope.ciu = $scope.ciudad;
                    }
                    $http.get(config.backend + '/edificio/generarReporte/' + $scope.filt + '/' + $scope.tip + '/' + $scope.ciu + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                };

                $scope.listarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.tipo === "" || $scope.tipo === undefined) {
                        $scope.tip = null;
                    } else {
                        $scope.tip = $scope.tipo;
                    }
                    if ($scope.ciudad === "" || $scope.ciudad === undefined) {
                        $scope.ciu = null;
                    } else {
                        $scope.ciu = $scope.ciudad;
                    }
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    EdificioService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt, $scope.tip, $scope.ciu).success(function (data) {
                        $scope.edificios = data;
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.buscar = function () {
                    $scope.edificios = [];
                    location.href = "http://" + config.ipFront + ":8017/menu.html#/:pagina=1";
                    if ($scope.filterText !== "" || $scope.tipo !== "" || $scope.ciudad !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
//                    }
                };

                $scope.buscarSegunPagina = function () {
                    $scope.edificios = [];
                    if ($scope.filterText !== "" || $scope.tipo !== "" || $scope.ciudad !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };

//                FIN DE PAGINACION

                var myMap = new Map();
                var myMapPersona = new Map();
                var myMapTipo = new Map();
                var myMapCiudad = new Map();

                $scope.listar = function () {
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    EdificioService.fetch(config.cantPaginacion, offSet).success(function (data) {
                        $scope.edificios = data;
                        $scope.paginar();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.registrar = function () {
                    $scope.edificioAgregar.andeCategoria.idAndeCat = myMap.get($scope.idAndeCat);
                    $scope.edificioAgregar.tipoEdificio.idTipoEdi = myMapTipo.get($scope.idTipoEdi);
                    $scope.edificioAgregar.persona.idPersona = myMapPersona.get($scope.idPersona);
                    $scope.edificioAgregar.ciudad.idCiudad = myMapCiudad.get($scope.idCiudad);

                    $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                    $scope.edificioAgregar.usuAltaEdificio = $scope.jsonUsu.usuarioUsuario;
                    $scope.edificioAgregar.usuModEdificio = $scope.jsonUsu.usuarioUsuario;

                    if ($scope.edificioAgregar.descriEdificio === "" ||
                            $scope.edificioAgregar.direccionEdificio === "" ||
                            $scope.edificioAgregar.telefEdificio === "") {
//                    if ($scope.edificioAgregar.descriEdificio === "" || $scope.edificioAgregar.descriEdificio === undefined || $scope.edificioAgregar.descriEdificio === null ||
//                            $scope.edificioAgregar.direccionEdificio === "" || $scope.edificioAgregar.direccionEdificio === undefined || $scope.edificioAgregar.direccionEdificio === null ||
//                            $scope.edificioAgregar.telefEdificio === "" || $scope.edificioAgregar.telefEdificio === undefined || $scope.edificioAgregar.telefEdificio === null) {
                        swal("Mensaje del Sistema!", "Los campos no deben quedar vacío", "error");
                    } else {
                        if (myMapPlaca.has($scope.ipAddress1)) {
                            swal("Mensaje del Sistema!", "La ip ingresada ya esta siendo utilizada", "error");
                        } else {
                            $scope.edificioAgregar.ipConsumo = $scope.ipAddress1;
                            EdificioService.crear($scope.edificioAgregar).success(function (data) {
                                if (data === true) {
                                    swal("Mensaje del Sistema!", "Datos Agregados exitosamente", "success");
                                    $('#modalAgregar').modal('hide');
//                            $scope.edificioAgregar = {};
                                    $scope.listar();
                                } else {
                                    swal("Mensaje del Sistema!", "Datos no Agregados, verifique que los campos no estén vacíos", "error");
                                }
                            }).error(function (e) {
                                swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                            });
                        }
                    }
                };
                $scope.atras = function () {
                    location.reload();
                };

                //SEPARADOR DE MILES

                $('input.number').keyup(function (event) {
                    // skip for arrow keys
                    if (event.which >= 37 && event.which <= 40) {
                        event.preventDefault();
                    }

                    $(this).val(function (index, value) {
                        return value
                                .replace(/\D/g, "")
                                //.replace(/([0-9])([0-9]{2})$/, '$1.$2')  
                                .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
                    });
                });

                //FIN DE SEPARADOR DE MILES


                $scope.actualizar = function () {

                    $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                    $scope.edificioModificar.usuModEdificio = $scope.jsonUsu.usuarioUsuario;


                    if ($scope.edificioModificar.descriEdificio === "" || $scope.edificioModificar.descriEdificio === undefined || $scope.edificioModificar.descriEdificio === null ||
                            $scope.edificioModificar.direccionEdificio === "" || $scope.edificioModificar.direccionEdificio === undefined || $scope.edificioModificar.direccionEdificio === null ||
                            $scope.edificioModificar.telefEdificio === "" || $scope.edificioModificar.telefEdificio === undefined || $scope.edificioModificar.telefEdificio === null) {
                        swal("Mensaje del Sistema!", "Los campos no deben quedar vacío", "error");
                    } else {
                        if (myMapPlaca.has($scope.ipAddress1)) {
                            if ($scope.edificioModificar.ipConsumo === $scope.ipAddress1) {
                                $scope.edificioModificar.ipConsumo = $scope.ipAddress1;
                                EdificioService.actualizar($scope.edificioModificar).success(function (data) {
                                    if (data === true) {
                                        swal("Mensaje del Sistema!", "Datos Modificados exitosamente", "success");
                                        $('#modalModificar').modal('hide');
//                            $scope.edificioModificar = {};
                                        $scope.listar();
                                    } else {
                                        swal("Mensaje del Sistema!", "Datos no Modificados", "error");
                                    }
                                }).error(function (e) {
                                    swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                });
                            } else {
                                swal("Mensaje del Sistema!", "La ip ingresada ya esta siendo utilizada", "error");
                            }
                        } else {
                            $scope.edificioModificar.ipConsumo = $scope.ipAddress1;
                            EdificioService.actualizar($scope.edificioModificar).success(function (data) {
                                if (data === true) {
                                    swal("Mensaje del Sistema!", "Datos Modificados exitosamente", "success");
                                    $('#modalModificar').modal('hide');
//                            $scope.edificioModificar = {};
                                    $scope.listar();
                                } else {
                                    swal("Mensaje del Sistema!", "Datos no Modificados", "error");
                                }
                            }).error(function (e) {
                                swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                            });
                        }
                    }
                };

                $scope.limpiar = function () {
                    $scope.edificioAgregar.descriEdificio = "";
                    $scope.edificioAgregar.direccionEdificio = "";
                    $scope.edificioAgregar.telefEdificio = "";
                    $scope.edificioAgregar.ipConsumo = "";
                    $scope.ipAddress1 = "";
                };

                $scope.agregar = function () {
                    //PERSONA FILTRO
                    $scope.limpiar();
                    $scope.idTipoEdi = "";
                    $scope.idPersona = "";
                    $scope.idCiudad = "";
                    $scope.idAndeCat = "";
                    $scope.ipAddress1 = "";
                    EdificioService.listar().success(function (data) {
                        angular.forEach(data, function (value, key) {
                            myMapPlaca.set(value.ipConsumo, value.ipConsumo);
                        });
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
//                    alert(myMapPlaca.has("192.168.0.40"));

                    $scope.cargarCombo();
                };

                $scope.cargarCombo = function () {
                    $scope.nestedItemsLevel1 = [];
                    $scope.nestedItemsLevelCiudad = [];
                    $scope.nestedItemsLevelPersona = [];
                    $scope.nestedItemsLevelTipo = [];

                    AndeCategoriaService.listar().success(function (d) {
                        angular.forEach(d, function (value, key) {
                            $scope.nestedItemsLevel1.push(value.descriAndeCat);
                            myMap.set(value.descriAndeCat, value.idAndeCat);
                        });
                    });
                    PersonaService.listar().success(function (d) {
                        angular.forEach(d, function (value, key) {
                            $scope.nestedItemsLevelPersona.push(value.nombrePersona + " " + value.apellidoPersona);
                            myMapPersona.set(value.nombrePersona + " " + value.apellidoPersona, value.idPersona);
                        });
                    });
                    CiudadService.listar().success(function (d) {
                        angular.forEach(d, function (value, key) {
                            $scope.nestedItemsLevelCiudad.push(value.descriCiudad);
                            myMapCiudad.set(value.descriCiudad, value.idCiudad);
                        });
                    });
                    TipoEdificioService.listar().success(function (d) {
                        angular.forEach(d, function (value, key) {
                            $scope.nestedItemsLevelTipo.push(value.descriTipoEdi);
                            myMapTipo.set(value.descriTipoEdi, value.idTipoEdi);
                        });
                    });
                };

                $scope.modificar = function (edificios) {

                    AndeCategoriaService.listar().success(function (d) {
                        $scope.categoriasMod = d;
                        $scope.edificioModificar.andeCategoria = edificios.andeCategoria;
                    });
                    PersonaService.listar().success(function (d) {

                        $scope.personaMod = d;
                        $scope.edificioModificar.persona = edificios.persona;
                    });
                    CiudadService.listar().success(function (d) {

                        $scope.ciudadMod = d;
                        $scope.edificioModificar.ciudad = edificios.ciudad;
                    });
                    TipoEdificioService.listar().success(function (d) {

                        $scope.tipoEdiMod = d;
                        $scope.edificioModificar.tipoEdificio = edificios.tipoEdificio;
                    });
                    $scope.edificioModificar.idEdificio = edificios.idEdificio;
                    $scope.edificioModificar.descriEdificio = edificios.descriEdificio;
                    $scope.edificioModificar.direccionEdificio = edificios.direccionEdificio;
                    $scope.edificioModificar.telefEdificio = edificios.telefEdificio;
                    $scope.edificioModificar.ipConsumo = edificios.ipConsumo;
                    $scope.ipAddress1 = edificios.ipConsumo;
                    EdificioService.listar().success(function (data) {
                        angular.forEach(data, function (value, key) {
                            myMapPlaca.set(value.ipConsumo, value.ipConsumo);
                        });
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
//                    alert(myMapPlaca.has("192.168.0.40"));

                    $("#modalModificar").modal("show");
                };

                $scope.eliminar = function (edi) {
                    var r = confirm("Seguro que quiere eliminar el edificio " + edi.descriEdificio + "?");
                    if (r === true) {
                        EdificioService.bajas(edi).success(function (data) {
                            if (data === true) {
                                swal('Eliminado!',
                                        'Haz eliminado exitosamente.',
                                        'success'
                                        );
                                $scope.listar();
                            }
                        });
                    } else {
                        swal('Cancelado',
                                'Haz cancelado la eliminación',
                                'error'
                                );
                    }
                };

                $scope.eliminar = function (edi) {
                    swal({
                        title: "Mensaje del Sistema",
                        text: "Seguro que quiere eliminar el edificio " + edi.descriEdificio + "?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    EdificioService.eliminar(edi.idEdificio).success(function (data) {
//                                    EdificioService.bajas(edi).success(function (data) {
                                        if (data === true) {
                                            swal('Eliminado!',
                                                    'Haz eliminado exitosamente.',
                                                    'success'
                                                    );
                                            $scope.listar();
                                        } else {
                                            swal("Mensaje del Sistema!", "No se pudo eliminar el Edificio, verifique Dependencias", "error");
                                        }
                                    }).error(function (e) {
                                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                    });
                                } else {
                                    swal("Mensaje del Sistema", "Haz cancelado la operación!", "error");
                                }
                            });
                };
                $scope.$watch('filterText', function () {
                    $("#paginaActual").val("1");
                })
                $scope.$watch('tipo', function () {
                    $("#paginaActual").val("1");
                })
                $scope.$watch('ciudad', function () {
                    $("#paginaActual").val("1");
                })
                $scope.limpiar = function () {
                    $scope.filterText = "";
                    $scope.tipo = "";
                    $scope.ciudad = "";
                };
                $scope.buscar();
            }]);