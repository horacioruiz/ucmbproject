'use strict'
app
        .factory('ConsumoGralService', function ($http, config) {
            var url = config.backend + "/consumoGeneral";
            return {
                listar: function () {
                    return $http.get(url);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                listarPendientes: function (idEdi) {
                    return $http.get(url + "/listarPendientes/" + idEdi);
                },
                actualizarDatos: function (id, potencia) {
                    return $http.get(url + "/actualizar/" + id + "/" + potencia);
                },
                arduinoIniciar: function (ip) {
//                    return $http.get(url + "/arduinoIniciar");
                    return $http.get("http://" + ip + "/LED=T");
                },
                arduinoGetPotencia: function (ip) {
//                    return $http.get(url + "/arduinoGetPotencia");
                    return $http.get("http://" + ip + "/LED=F");
                },
                listarPorIdNodo: function (id) {
                    return $http.get(url + "/listarPorIdNodo/" + id);
                },
                listarPorIdNodoYFecha: function (id, fecha) {
                    return $http.get(url + "/listarPorIdNodoYFecha/" + id + "/" + fecha);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                bajas: function (data) {
                    return $http.put(url + "/bajas", data);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                },
                fetch: function (limit, offset) {
                    return $http.get(url + "/fetch/" + limit + "/" + offset);
                },
                fetchFiltro: function (limit, offset, descri, fecha) {
                    return $http.get(url + "/fetchFiltro/" + limit + "/" + offset + "/" + descri + "/" + fecha);
                },
                count: function () {
                    return $http.get(url + "/count/");
                },
                countFiltro: function (descri, fecha) {
                    return $http.get(url + "/countFiltro/" + descri + "/" + fecha);
                },
                reportarPorDescripcion: function (desccri, fecha, user) {
                    return $http.get(url + "/reportar/" + fecha + "/" + desccri + "/" + user);
                }
            };
        });