'use strict'
appLogin
        .factory('FallidoService', function ($http, config) {
            var url = config.backend + "/fallido";
            return {
                listar: function () {
                    return $http.get(url);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                listarUsuario: function (id) {
                    return $http.get(url + "/listarUsuario/" + id);
                },
                listarPorIdUsuario: function (id) {
                    return $http.get(url + "/idUsuario/" + id);
                },
                restauraPassPorIdUsuario: function (id) {
                    return $http.get(url + "/restauraPassPorIdUsuario/" + id);
                },
                restauraEmailPorIdUsuario: function (id) {
                    return $http.get(url + "/restauraEmailPorIdUsuario/" + id);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                crearPorIdUsuario: function (id) {
                    return $http.get(url + "/crearPorIdUsuario/" + id);
                },
                actualizarPorIdUsuario: function (id) {
                    return $http.get(url + "/actualizarPorIdUsuario/" + id);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                }
            };
        });