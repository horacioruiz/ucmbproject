'use strict'
app
        .factory('SensorNodoService', function ($http, config) {
            var url = config.backend + "/sensorNodo";
            return {
                listar: function () {
                    return $http.get(url);
                },
                listarPorNodo: function (idNodo) {
                    return $http.get(url + "/listarPorNodo/" + idNodo);
                },
                deleteData: function (id) {
                    return $http.get(url + "/deleteData/" + id);
                },
                listarPorSensor: function (idSensor) {
                    return $http.get(url + "/listarPorSensor/" + idSensor);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                }
//                ,
//                deleteUpdate: function (id) {
//                    return $http.get(url + "/deleteUpdate/" + id);
//                },
//                eliminar: function (id) {
//                    return $http.delete(url + "/" + id);
//                },
//                fetch: function (limit, offset) {
//                    return $http.get(url + "/fetch/" + limit + "/" + offset);
//                },
//                fetchFiltro: function (limit, offset, descr) {
//                    return $http.get(url + "/fetchFiltro/" + limit + "/" + offset + "/" + descr);
//                },
//                count: function () {
//                    return $http.get(url + "/count/");
//                },
//                countFiltro: function (descr) {
//                    return $http.get(url + "/countFiltro/" + descr);
//                }
            };
        });