'use strict'
app
        .factory('ControlNodoService', function ($http, config) {
            var url = config.backend + "/controlNodo";
            return {
                listar: function () {
                    return $http.get(url);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                listarPorControl: function (id) {
                    return $http.get(url + "/listarPorControl/" + id);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                crearPorArrayNodo: function (idControlAcceso, arrayNodo) {
                    return $http.get(url + "/crearPorArrayNodo/" + idControlAcceso + "/" + arrayNodo);
                },
                eliminarPorArrayNodo: function (idControlAcceso, arrayNodo) {
                    return $http.get(url + "/eliminarPorArrayNodo/" + idControlAcceso + "/" + arrayNodo);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                }
            };
        });