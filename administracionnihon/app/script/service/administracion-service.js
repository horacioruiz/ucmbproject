'use strict'
app
        .factory('AdminService', function ($http, config) {
            var url = config.backend + "/administracion";
            return {
                listar: function () {
                    return $http.get(url);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                generarGrafico: function (id, fec) {
                    return $http.get(url + "/generarGrafico/" + id + "/" + fec);
                },
                fetch: function (limit, offset) {
                    return $http.get(url + "/fetch/" + limit + "/" + offset);
                },
                fetchFiltro: function (limit, offset, descri, fecIni, fecFin) {
                    console.log("fetchFiltro -->> " + url + "/fetchFiltro/" + limit + "/" + offset + "/" + descri + "/" + fecIni + "/" + fecFin);
                    return $http.get(url + "/fetchFiltro/" + limit + "/" + offset + "/" + descri + "/" + fecIni + "/" + fecFin);
                },
                count: function () {
                    return $http.get(url + "/count/");
                },
                cambEstadoActuador: function (ip, pin) {
                    return $http.get(url + "/cambEstadoActuador/" + ip + "/" + pin);
                },
                cambEstadoAct: function (idNodo, temp, humedad, humo, mov, usu) {
                    return $http.get(url + "/cambEstadoAct/" + idNodo + "/" + temp + "/" + humedad + "/" + humo + "/" + mov + "/" + usu);
                },
                getEstadoByIpPin: function (ip, pin) {
                    return $http.get(url + "/getEstadoByIpPin/" + ip + "/" + pin);
                },
                getEstadoByIdNodo: function (idNodo) {
                    return $http.get(url + "/getEstadoByIdNodo/" + idNodo);
                },
                listarPorIpPin: function (ip, pin) {
                    return $http.get(url + "/listarPorIpPin/" + ip + "/" + pin);
                },
                countFiltro: function (descripcion, fecIni, fecFin) {
                    console.log("fetchFiltro -->> " + url + "/countFiltro/" + descripcion + "/" + fecIni + "/" + fecFin);
                    return $http.get(url + "/countFiltro/" + descripcion + "/" + fecIni + "/" + fecFin);
                },
                reportarPorDescripcion: function (descripcion, usuario) {
                    console.log("reportarPorDescripcion -->> " + url + "/reportarPorDescripcion/" + descripcion + "/" + usuario);
                    return $http.get(url + "/reportarPorDescripcion/" + descripcion + "/" + usuario);
                },
                reportarPorFecha: function (fecIni, fecFin, usu) {
                    console.log("reportarPorFecha -->> " + url + "/reportarPorFecha/" + fecIni + "/" + fecFin + "/" + usu);
                    return $http.get(url + "/reportarPorFecha/" + fecIni + "/" + fecFin + "/" + usu);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                }
            };
        });