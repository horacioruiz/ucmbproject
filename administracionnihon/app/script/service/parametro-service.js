'use strict'
app
        .factory('ParametroService', function ($http, config) {
            var url = config.backend + "/parametro";
            return {
                listar: function () {
                    return $http.get(url);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                deleteUpdate: function (id) {
                    return $http.get(url + "/deleteUpdate/" + id);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                },
                fetch: function (limit, offset) {
                    return $http.get(url + "/fetch/" + limit + "/" + offset);
                },
                fetchFiltro: function (limit, offset, descr) {
                    return $http.get(url + "/fetchFiltro/" + limit + "/" + offset + "/" + descr);
                },
                count: function () {
                    return $http.get(url + "/count/");
                },
                countFiltro: function (descr) {
                    return $http.get(url + "/countFiltro/" + descr);
                }
            };
        });