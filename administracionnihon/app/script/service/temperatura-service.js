'use strict'
app
        .factory('TemperaturaService', function ($http, config) {
            var url = config.backend + "/temperatura";
            return {
                listar: function () {
                    return $http.get(url);
                },
                generarGrafico: function (id, fec) {
                    return $http.get(url + "/generarGrafico/" + id + "/" + fec);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                getLastIdNodo: function (datos) {
                    return $http.get(url + "/getLastIdNodo/" + datos);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                bajas: function (data) {
                    return $http.put(url + "/bajas", data);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                },
                modEliminar: function (data) {
                    return $http.put(url + "/eliminar", data);
                },
                fetch: function (limit, offset) {
                    return $http.get(url + "/fetch/" + limit + "/" + offset);
                },
                fetchFiltro: function (limit, offset, descri, fecha) {
                    return $http.get(url + "/fetchFiltro/" + limit + "/" + offset + "/" + descri + "/" + fecha);
                },
                count: function () {
                    return $http.get(url + "/count/");
                },
                countFiltro: function (descri, fecha) {
                    return $http.get(url + "/countFiltro/" + descri + "/" + fecha);
                }
            };
        });