package py.com.local.control.money.web.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import py.com.local.control.money.core.model.JsonMaterias;
import py.com.local.control.money.core.services.JsonMateriasService;
import py.com.local.control.money.util.DBConnection;

@Component
public class ScheduledTasks {

	@Autowired
	private JsonMateriasService jsonMateriaService;

	private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	@Scheduled(fixedRate = 200000)
	public void reportCurrentTime() throws IOException {
		URL url;
		try {
			url = new URL(
					"http://moodle.ucmb.edu.py:8800/webservice/rest/server.php?wstoken=54a338b30a217b76c9a88a6acbb89dcf&wsfunction=core_course_get_courses&moodlewsrestformat=json	");
			URLConnection con = url.openConnection();
			InputStream in = con.getInputStream();
			String encoding = con.getContentEncoding();
			encoding = encoding == null ? "UTF-8" : encoding;
			String body = IOUtils.toString(in, encoding);
			actualizarDatos(body);
			log.info("The time is now {}", dateFormat.format(new Date()));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void actualizarDatos(String body) {
		DBConnection dBConnection = new DBConnection();
		try {
			try {
				Statement statement = dBConnection.getConnection().createStatement();
				String sql = "UPDATE jsonmaterias SET json='" + body.replace("'","") + "' WHERE id=1";
				System.out.println(sql);
				int i = statement.executeUpdate(sql);
			} catch (SQLException ex) {
				System.err.println(ex.getMessage());
			}
			dBConnection.getConnection().close();
		} catch (Exception e) {
			System.out.println("ERROR: " + e.getLocalizedMessage());
			System.out.println("ERROR: " + e.fillInStackTrace());
		} finally {

		}

	}
}