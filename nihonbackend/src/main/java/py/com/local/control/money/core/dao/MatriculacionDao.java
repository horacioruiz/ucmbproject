package py.com.local.control.money.core.dao;

import java.util.List;
import java.util.Optional;

import py.com.local.control.money.core.model.Matriculaciones;

public interface MatriculacionDao {

	List<Matriculaciones> listarTodos(String cod);

	Optional<Matriculaciones> getById(long id);

	List<Matriculaciones> getByUsuario(Long idUsuario);
	
	List<Matriculaciones> getByUsuarioAndEstado(Long idUsuario, boolean valor);

	void insertar(Matriculaciones usu);

	void actualizar(Matriculaciones usu);

	void borrarPorId(Long id);

	Matriculaciones getByUsuarioAndMateria(long idusuario, long idmateria, boolean estado);

	List<Matriculaciones> listarPendientes(String codigo);

}
