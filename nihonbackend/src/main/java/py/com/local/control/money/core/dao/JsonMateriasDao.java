package py.com.local.control.money.core.dao;

import java.util.List;
import java.util.Optional;

import py.com.local.control.money.core.model.JsonMaterias;

public interface JsonMateriasDao {

	List<JsonMaterias> listarTodos();

	Optional<JsonMaterias> getById(long id);
	
	void actualizar(JsonMaterias json);

}
