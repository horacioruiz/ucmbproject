'use strict'
app
        .factory('EdificioService', function ($http, config) {
            var url = config.backend + "/edificio";
            return {
                listar: function () {
                    return $http.get(url);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                listarPorDescri: function (descri) {
                    return $http.get(url + "/listarPorDescri/" + descri);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                bajas: function (data) {
                    return $http.put(url + "/bajas", data);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                },
                modEliminar: function (data) {
                    return $http.put(url + "/eliminar", data);
                },
                fetch: function (limit, offset) {
                    return $http.get(url + "/fetch/" + limit + "/" + offset);
                },
                fetchFiltro: function (limit, offset, descri, tipo, ciu) {
                    return $http.get(url + "/fetchFiltro/" + limit + "/" + offset + "/" + descri + "/" + tipo + "/" + ciu);
                },
                count: function () {
                    return $http.get(url + "/count/");
                },
                countFiltro: function (descri, tipo, ciu) {
                    return $http.get(url + "/countFiltro/" + descri + "/" + tipo + "/" + ciu);
                }
            };
        });