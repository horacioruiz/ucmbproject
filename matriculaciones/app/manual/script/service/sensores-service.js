'use strict'
app
        .factory('SensoresService', function ($http, config) {
            var url = config.backend + "/sensor";
            return {
                listar: function () {
                    return $http.get(url);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                listarSensores: function (idEdi) {
                    return $http.get(url + "/listarSensores/" + idEdi);
                },
                update: function (id, estado, valor) {
                    return $http.get(url + "/update/" + id + "/" + estado + "/" + valor);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                deleteUpdate: function (id) {
                    return $http.get(url + "/deleteUpdate/" + id);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                },
                fetch: function (limit, offset) {
                    return $http.get(url + "/fetch/" + limit + "/" + offset);
                },
                fetchFiltro: function (limit, offset, descr, fecha) {
                    return $http.get(url + "/fetchFiltro/" + limit + "/" + offset + "/" + descr + "/" + fecha);
                },
                count: function () {
                    return $http.get(url + "/count/");
                },
                countFiltro: function (descr, fecha) {
                    return $http.get(url + "/countFiltro/" + descr + "/" + fecha);
                }
            };
        });