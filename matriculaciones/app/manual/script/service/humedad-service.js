'use strict'
app
        .factory('HumedadService', function ($http, config) {
            var url = config.backend + "/humedad";
            return {
                listar: function () {
                    return $http.get(url);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                ultimosRegistrados: function (idEdi) {
                    return $http.get(url + "/ultimosRegistrados/" + idEdi);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                bajas: function (data) {
                    return $http.put(url + "/bajas", data);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                },
                modEliminar: function (data) {
                    return $http.put(url + "/eliminar", data);
                },
                fetch: function (limit, offset) {
                    return $http.get(url + "/fetch/" + limit + "/" + offset);
                },
                fetchFiltro: function (limit, offset, descri, fecha) {
                    return $http.get(url + "/fetchFiltro/" + limit + "/" + offset + "/" + descri + "/" + fecha);
                },
                count: function () {
                    return $http.get(url + "/count/");
                },
                countFiltro: function (descri, fecha) {
                    return $http.get(url + "/countFiltro/" + descri + "/" + fecha);
                }
            };
        });