'use strict'
app
        .factory('PlanService', function ($http, config) {
            var url = config.backend + "/plan";
            return {
                listar: function () {
                    return $http.get(url);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                recuperarPlan: function (id) {
                    return $http.get(url + "/recuperarPlan/" + id);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                },
                modEliminar: function (id, usu) {
                    return $http.put(url + "/" + id + "/" + usu);
                },
                fetch: function (limit, offset) {
                    return $http.get(url + "/fetch/" + limit + "/" + offset);
                },
                fetchFiltro: function (limit, offset, descri, dep, comp, para, edi) {
                    return $http.get(url + "/fetchFiltro/" + limit + "/" + offset + "/" + descri + "/" + dep + "/" + comp + "/" + para + "/" + edi);
                },
                count: function () {
                    return $http.get(url + "/count/");
                },
                countFiltro: function (descri, dep, comp, para, edi) {
                    return $http.get(url + "/countFiltro/" + descri + "/" + dep + "/" + comp + "/" + para + "/" + edi);
                }
            };
        });