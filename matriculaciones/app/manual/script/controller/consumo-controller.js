'use strict'
app
        .controller('listConsumoCtrl', ['$state', '$scope', 'config', 'ConsumoService', '$cookies', '$base64', '$filter', '$location', '$rootScope', '$http', function ($state, $scope, config, ConsumoService, $cookies, $base64, $filter, $location, $rootScope, $http) {
                $scope.consumo = {};
                $scope.consumoAgregar = {};
                $scope.consumoModificar = {};

                $scope.consumoAgregar.andeCategoria = {};
                $scope.consumoAgregar.tipoEdificio = {};
                $scope.consumoAgregar.persona = {};
                $scope.consumoAgregar.ciudad = {};

                $scope.consumoModificar.andeCategoria = {};
                $scope.consumoModificar.tipoEdificio = {};
                $scope.consumoModificar.persona = {};
                $scope.consumoModificar.ciudad = {};

                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };

                $scope.aModificar = "";

                var myMap = new Map();

                $scope.limpiar = function () {
                    document.getElementById("fecIni").valueAsDate = null;
//                    document.getElementById("fecFin").valueAsDate = null;
                    $scope.tipoEdificio = "";
                    $scope.componente = "";
                    $scope.edificio = "";
                };

                $scope.listar = function () {
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    ConsumoService.fetch(config.cantPaginacion, offSet).success(function (data) {
                        $scope.consumo = data;
                        $scope.paginar();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.paginar = function () {
                    ConsumoService.count().success(function (data) {
                        $scope.catidad = data;

                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $rootScope.$on("$locationChangeStart", function (event, data) {
                    var pag = parseInt(data.replace("http://" + config.ipFront + ":8017/menu.html#/:pagina=", ""));
                    if (!isNaN(pag)) {
                        $("#paginaActual").val(pag);
                        $scope.buscarSegunPagina();
                    }
                });

                $scope.paginarConFiltro = function () {

                    if ($scope.tipoEdificio === "" || $scope.tipoEdificio === undefined) {
                        $scope.tipoEdif = null;
                    } else {
                        $scope.tipoEdif = $scope.tipoEdificio;
                    }
                    if ($scope.componente === "" || $scope.componente === undefined) {
                        $scope.comp = null;
                    } else {
                        $scope.comp = $scope.componente;
                    }
                    if ($scope.edificio === "" || $scope.edificio === undefined) {
                        $scope.edi = null;
                    } else {
                        $scope.edi = $scope.edificio;
                    }
                    if ($scope.fecIni === "" || $scope.fecIni === undefined) {
                        $scope.fIni = null;
                    } else {
                        var date = new Date($('#fecIni').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();

                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');
                        $scope.fIni = fecha;
                        if ($scope.fIni === "NaN-aN-aN") {
                            $scope.fIni = null;
                        }
                    }
//                    if ($scope.fecFin === "" || $scope.fecFin === undefined) {
//                        $scope.fFin = null;
//                    } else {
//                        var date = new Date($('#fecFin').val());
//                        var day = date.getDate() + 1;
//                        var month = date.getMonth() + 1;
//                        var year = date.getFullYear();
//
//                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');
//                        $scope.fFin = fecha;
//                        if ($scope.fFin === "NaN-aN-aN") {
//                            $scope.fFin = null;
//                        }
//                    }
                    ConsumoService.countFiltro($scope.tipoEdif, $scope.comp, $scope.fIni, $scope.edi).success(function (data) {
                        $scope.catidad = data;

                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.listarConFiltro = function () {
                    if ($scope.tipoEdificio === "" || $scope.tipoEdificio === undefined) {
                        $scope.tipoEdif = null;
                    } else {
                        $scope.tipoEdif = $scope.tipoEdificio;
                    }
                    if ($scope.componente === "" || $scope.componente === undefined) {
                        $scope.comp = null;
                    } else {
                        $scope.comp = $scope.componente;
                    }
                    if ($scope.edificio === "" || $scope.edificio === undefined) {
                        $scope.edi = null;
                    } else {
                        $scope.edi = $scope.edificio;
                    }
                    if ($scope.fecIni === "" || $scope.fecIni === undefined) {
                        $scope.fIni = null;
                    } else {
                        var date = new Date($('#fecIni').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();

                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');
                        $scope.fIni = fecha;
                        if ($scope.fIni === "NaN-aN-aN") {
                            $scope.fIni = null;
                        }
                    }
//                    if ($scope.fecFin === "" || $scope.fecFin === undefined) {
//                        $scope.fFin = null;
//                    } else {
//                        var date = new Date($('#fecFin').val());
//                        var day = date.getDate() + 1;
//                        var month = date.getMonth() + 1;
//                        var year = date.getFullYear();
//
//                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');
//                        $scope.fFin = fecha;
//                        if ($scope.fFin === "NaN-aN-aN") {
//                            $scope.fFin = null;
//                        }
//                    }

                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    ConsumoService.fetchFiltro(config.cantPaginacion, offSet, $scope.tipoEdif, $scope.comp, $scope.fIni, $scope.edi).success(function (data) {
                        $scope.consumo = data;
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.tipoEdificio === "" || $scope.tipoEdificio === undefined) {
                        $scope.tipoEdif = null;
                    } else {
                        $scope.tipoEdif = $scope.tipoEdificio;
                    }
                    if ($scope.componente === "" || $scope.componente === undefined) {
                        $scope.comp = null;
                    } else {
                        $scope.comp = $scope.componente;
                    }
                    if ($scope.edificio === "" || $scope.edificio === undefined) {
                        $scope.edi = null;
                    } else {
                        $scope.edi = $scope.edificio;
                    }
                    if ($scope.fecIni === "" || $scope.fecIni === undefined) {
                        $scope.fIni = null;
                    } else {
                        var date = new Date($('#fecIni').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();

                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');
                        $scope.fIni = fecha;
                        if ($scope.fIni === "NaN-aN-aN") {
                            $scope.fIni = null;
                        }
                    }
                    $http.get(config.backend + '/consumo/generarReporte/' + $scope.comp + '/' + $scope.tipoEdif + '/' + $scope.edi + '/' + $scope.fIni + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                };

                $scope.buscar = function () {
                    $scope.consumo = [];
                    location.href = "http://" + config.ipFront + ":8017/menu.html#/:pagina=1";
                    if ($scope.tipoEdificio !== "" || $scope.componente !== "" || $scope.fecIni !== "" || $scope.edificio !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
//                    }
                };

                $scope.buscarSegunPagina = function () {
                    $scope.consumo = [];
                    if ($scope.componente !== "" || $scope.tipoEdificio !== "" || $scope.fecIni !== "" || $scope.edificio !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };

                //FIN DE PAGINACION

                $scope.reporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.dependencia === "" || $scope.dependencia === undefined) {
                        $scope.dep = null;
                    } else {
                        $scope.dep = $scope.dependencia;
                    }
                    if ($scope.componente === "" || $scope.componente === undefined) {
                        $scope.comp = null;
                    } else {
                        $scope.comp = $scope.componente;
                    }
                    if ($scope.edificio === "" || $scope.edificio === undefined) {
                        $scope.edi = null;
                    } else {
                        $scope.edi = $scope.edificio;
                    }
                    if ($scope.fecIni === "" || $scope.fecIni === undefined) {
                        $scope.fecI = null;
                    } else {
                        var date = new Date($('#fecIni').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();

                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');
                        $scope.fecI = fecha;
                        if ($scope.fecI === "NaN-aN-aN") {
                            $scope.fecI = null;
                        }
                    }
//                    if ($scope.fecFin === "" || $scope.fecFin === undefined) {
//                        $scope.fecF = null;
//                    } else {
//                        var date = new Date($('#fecFin').val());
//                        var day = date.getDate() + 1;
//                        var month = date.getMonth() + 1;
//                        var year = date.getFullYear();
//
//                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');
//                        $scope.fecF = fecha;
//                        if ($scope.fecF === "NaN-aN-aN") {
//                            $scope.fecF = null;
//                        }
//                    }

                    $scope.result = {};
                    $http.get(config.backend + '/consumo/reportar/' + $scope.filt + '/' + $scope.dep + '/' + $scope.comp + '/' + $scope.fecI + '/' + $scope.userLog + "/" + $scope.edi, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                }

                $scope.$watch('filterText', function () {
                    $("#paginaActual").val("1");
                })
                $scope.$watch('componente', function () {
                    $("#paginaActual").val("1");
                })
                $scope.$watch('dependencia', function () {
                    $("#paginaActual").val("1");
                })
                $scope.$watch('fecIni', function () {
                    $("#paginaActual").val("1");
                })
//                $scope.$watch('fecFin', function () {
//                    $("#paginaActual").val("1");
//                })
                $scope.atras = function () {
                    location.reload();
                };
                $scope.limpiar = function () {
                    document.getElementById("fecIni").valueAsDate = null;
//                    document.getElementById("fecFin").valueAsDate = null;
                    $scope.filterText = "";
                    $scope.dependencia = "";
                    $scope.componente = "";
                };
                $scope.buscar();
            }])
        .controller('listPruebaConsumoCtrl', ['$state', '$scope', 'config', 'ConsumoService', 'UsuarioEdificioService', 'EdificioService', '$cookies', '$base64', '$filter', '$location', '$rootScope', '$http', function ($state, $scope, config, ConsumoService, UsuarioEdificioService, EdificioService, $cookies, $base64, $filter, $location, $rootScope, $http) {
                $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                $scope.userLog = $scope.jsonUsu.usuarioUsuario;
                var myMapEdificio = new Map();
                $scope.componente = "";
                $scope.idDato = 0;

                //CRONOMETRO

                var vueltas = 0;
                var cron;
                var sv_min = 0;
                var sv_hor = 0;
                var sv_seg = 0;
                var seg = document.getElementById('seg');
                var min = document.getElementById('min');
                var hor = document.getElementById('hor');
                var iniciado = false; //init status of cron

                $("#btn_play").click(function () {
                    if (!iniciado) {
                        if ($scope.componente !== "" && $scope.idEdificio !== 0) {
                            $scope.agregar = {};
                            $scope.agregar.componente = "";
                            $scope.agregar.edificio = "";
                            $scope.agregar.tipoEdificio = "";
                            $scope.agregar = {};

//                            alert("-->> " + $scope.idEdificio);
                            EdificioService.listarPorDescri($scope.idEdificio).success(function (data) {
                                ConsumoService.arduinoIniciar(data.ipConsumo).success(function (d) {
                                    if (d.ESTADO === "ON") {
                                        $scope.agregar.componente = $scope.componente;
                                        $scope.agregar.edificio = $scope.idEdificio;
                                        $scope.agregar.tipoEdificio = myMapEdificio.get($scope.idEdificio);
                                        ConsumoService.crear($scope.agregar).success(function (data) {
                                            if (parseInt(data) !== 0) {
                                                $scope.idDato = parseInt(data);
                                                iniciado = true;
                                                start_cron();
                                            }
                                        });
                                    } else {
                                        swal("Mensaje del Sistema!", "No se ha establecido comunicación con el módulo Arduino", "error");
                                    }
                                }).error(function (e) {
                                    swal("Mensaje del Sistema!", "ERROR: No se ha establecido comunicación con el módulo Arduino", "error");
                                });
                                ;
                            });
                        } else {
                            swal("Mensaje del Sistema!", "Los campos no deben quedar vacío para iniciar el control de consumo", "error");
                        }
                    }
                });

                function start_cron() {
                    cron = setInterval(function () {
                        sv_seg++;
                        if (sv_seg < 60) {
                            if (sv_seg < 10) {
                                seg.innerHTML = "0" + sv_seg;
                            } else {
                                seg.innerHTML = sv_seg;
                            }
                        } else {
                            sv_seg = 0;
                            seg.innerHTML = "00";
                            sv_min++;
                            if (sv_min < 60) {
                                if (sv_min < 10) {
                                    min.innerHTML = "0" + sv_min;
                                } else {
                                    min.innerHTML = sv_min;
                                }
                            } else {
                                sv_min = 0;
                                min.innerHTML = "00";
                                sv_hor++;
                                if (sv_hor < 10) {
                                    hor.innerHTML = "0" + sv_hor;
                                } else {
                                    hor.innerHTML = sv_hor;
                                }
                            }
                        }
                    }, 1000);
                }

                $("#btn_pause").click(function () {
                    clearInterval(cron);
                    iniciado = false;
                });

                $("#btn_stop").click(function () {
                    sv_min = 0;
                    sv_hor = 0;
                    sv_seg = 0;
                    seg.innerHTML = "00";
                    min.innerHTML = "00";
                    hor.innerHTML = "00";
                    clearInterval(cron);
                    iniciado = false;
                });

                $scope.pausar = function () {
                    clearInterval(cron);
                    iniciado = false;
                };

                $scope.limpiarCronometro = function () {
                    sv_min = 0;
                    sv_hor = 0;
                    sv_seg = 0;
                    seg.innerHTML = "00";
                    min.innerHTML = "00";
                    hor.innerHTML = "00";
                    clearInterval(cron);
                    iniciado = false;
                };

                $("#btn_lap").click(function () {
//                    $scope.pausar();
                    $scope.potencia = 0;
                    if (hor.innerHTML + ":" + min.innerHTML + ":" + seg.innerHTML !== "00:00:00") {
                        EdificioService.listarPorDescri($scope.idEdificio).success(function (data) {
                            ConsumoService.arduinoGetPotencia(data.ipConsumo).success(function (da) {
                                if (da.ESTADO !== " NAN") {
                                    $scope.potencia = da.ESTADO;
                                    ConsumoService.actualizarDatos($scope.idDato, hor.innerHTML + ":" + min.innerHTML + ":" + seg.innerHTML, $scope.potencia).success(function (data) {
                                        if (data.costo !== "{}") {
                                            vueltas++;
                                            consola('<li class="list-group-item"><center><small>' + vueltas + ') </small>          ' + hor.innerHTML + ":" + min.innerHTML + ":" + seg.innerHTML + '     |     <small>Gs. ' + Number(data.costo).toFixed(2) + '</small></center></li>');
                                            $scope.limpiarCronometro();
                                        }
                                    });
                                }
                            });
                        });
                    }
                });

                function consola(msg) {
                    $("#log").prepend(msg);
                }

                $("#btn_clear").click(function () {
                    $("#log").html("");
                    vueltas = 0;
                });

                //FIN DEL CRONOMETRO

                $scope.atras = function () {
                    location.reload();
                };

                $scope.agregarEdificio = function () {
                    $scope.nestedItemsLevelEdificio = [];
                    $scope.idEdificio = 0;
                    UsuarioEdificioService.getByIdUsu($scope.jsonUsu.idUsuario).success(function (data) {
                        angular.forEach(data, function (value, key) {
                            $scope.nestedItemsLevelEdificio.push(value.edificio.descriEdificio);
                            myMapEdificio.set(value.edificio.descriEdificio, value.edificio.tipoEdificio.descriTipoEdi);
                        });
                    });
                };

                $scope.onChangeEdificio = function () {
                    $scope.tipoEdificio = "Tipo de Edificio: " + myMapEdificio.get($scope.idEdificio);
                };

                $scope.agregarEdificio();
            }]);
