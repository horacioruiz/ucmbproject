'use strict'
app
        .controller('controlAccesoCtrl', ['$state', '$scope', '$http', 'config', 'ControlAccesoService', 'NodoService', 'NodoPinService', 'ControlNodoService', 'UsuarioService', '$cookies', '$base64', function ($state, $scope, $http, config, ControlAccesoService, NodoService, NodoPinService, ControlNodoService, UsuarioService, $cookies, $base64, $log) {
                $scope.controlAcceso = {};
//               pagination
                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.nomUsu = "";
                $scope.apeUsu = "";
                $scope.controlNodoAgregar = {};
                $scope.controlNodoEliminar = {};
                $scope.controlNodo = {};

                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.limpiar = function () {
                    document.getElementById("fecha").valueAsDate = null;
//                    document.getElementById("fecFin").valueAsDate = null;
                    $scope.filterText = "";
                };
                $scope.listarByStatus = function () {
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    ControlAccesoService.fetch(config.cantPaginacion, offSet).success(function (data) {
                        $scope.controlAcceso = data;
                        $scope.paginar();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.paginar = function () {
                    ControlAccesoService.count().success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());
                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.fecha === "" || $scope.fecha === undefined) {
                        $scope.fec = null;
                    } else {
                        var date = new Date($('#fecha').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();
                        var fechas = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');
                        $scope.fec = fechas;
                        if ($scope.fec === "NaN-aN-aN") {
                            $scope.fec = null;
                        }
                    }
                    if ($scope.nomUsu === undefined || $scope.nomUsu === "") {
                        $scope.nom = null;
                    } else {
                        $scope.nom = $scope.nomUsu;
                    }
                    if ($scope.apeUsu === undefined || $scope.apeUsu === "") {
                        $scope.ape = null;
                    } else {
                        $scope.ape = $scope.apeUsu;
                    }
                    ControlAccesoService.countFiltro($scope.filt, $scope.fec, $scope.nom, $scope.ape).success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());
                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.listarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.fecha === "" || $scope.fecha === undefined) {
                        $scope.fec = null;
                    } else {
                        var date = new Date($('#fecha').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();
                        var fechas = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');
                        $scope.fec = fechas;
                        if ($scope.fec === "NaN-aN-aN") {
                            $scope.fec = null;
                        }
                    }
                    if ($scope.nomUsu === undefined || $scope.nomUsu === "") {
                        $scope.nom = null;
                    } else {
                        $scope.nom = $scope.nomUsu;
                    }
                    if ($scope.apeUsu === undefined || $scope.apeUsu === "") {
                        $scope.ape = null;
                    } else {
                        $scope.ape = $scope.apeUsu;
                    }

                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    ControlAccesoService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt, $scope.fec, $scope.nom, $scope.ape).success(function (data) {
                        $scope.controlAcceso = data;
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.nomUsu === "" || $scope.nomUsu === undefined) {
                        $scope.nom = null;
                    } else {
                        $scope.nom = $scope.nomUsu;
                    }
                    if ($scope.apeUsu === "" || $scope.apeUsu === undefined) {
                        $scope.ape = null;
                    } else {
                        $scope.ape = $scope.apeUsu;
                    }
                    $http.get(config.backend + '/controlNodo/generarReporte/' + $scope.filt + '/' + $scope.nom + '/' + $scope.ape + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                };

                $scope.buscarSegunPagina = function () {
                    $scope.controlAcceso = [];
                    if ($scope.filterText !== "" || $scope.fecha !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listarByStatus();
                    }
                };
                $scope.buscar = function () {
                    $scope.controlAcceso = [];
                    if ($scope.filterText !== "" || $scope.fecha !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listarByStatus();
                    }
//                    }
                };
//                FIN DE PAGINACION


                $scope.reload = function () {
                    location.reload();
                };
                $scope.eliminar = function (ca) {
                    $scope.controlNodoEliminar = {};
                    $("#idControlEliminar").val(ca.idControl);
                    ControlNodoService.listarPorControl(ca.idControl).success(function (d) {
                        $scope.controlNodo = d;
                        if (d.length !== 0) {
                            $("#delete").modal("show");
                        } else {
                            swal("Mensaje del Sistema!", "No se ha asignado dependencias para el usuario", "error");
                        }
                    });
                };
                $scope.eliminarControlNodo = function () {
                    $scope.controlNodo = {};
                    $scope.controlNodo.controlAcceso = {};
                    $scope.controlNodo.nodo = {};
                    $scope.controlNodo.controlAcceso.idControl = $("#idControlEliminar").val();
                    var arrayDatos = "";
                    $("input:checkbox:checked", "#tableControlNodoEliminar").map(function () {
                        arrayDatos += $(this).val() + "-";
                    });
                    var res = arrayDatos.substring(0, (arrayDatos.length - 1));
//                    for (var i = 0; i < val; i++) {
//                    $scope.controlNodo.nodo.idNodo = arrayDatos[i];
//                    $scope.controlNodo.estado = false;
                    ControlNodoService.eliminarPorArrayNodo($scope.controlNodo.controlAcceso.idControl, res).success(function (data) {
                        if (data === true) {
                            swal("Mensaje del Sistema!", "Datos Eliminados exitosamente", "success");
                        }
                        $("#delete").modal('hide');
                        $scope.listarByStatus();
                    }).error(function (e) {
                        $("#delete").modal('hide');
                        $scope.listarByStatus();
                        console.log("NO SE PUDO CONECTAR CON EL SERVIDOR....");
                    });
                };
                $scope.modificarDatos = function (ca) {
                    $("#idControlInsertar").val(ca.idControl);
                    NodoService.getNodoByControlAcceso(config.idCompControlAcceso, ca.idControl).success(function (d) {
                        $scope.nodoInsertar = d;
                        if (d.length !== 0) {
                            $('#add').modal('show');
                        } else {
                            swal("Mensaje del Sistema!", "El Usuario tiene todos los Accesos a las dependencias existentes.", "error");
                        }
                    });
                };
                $scope.reenvio = function (ca) {
//                    $("#idControlInsertar").val(ca.idControl);
                    UsuarioService.reenviar(ca.usuario.idUsuario).success(function (d) {
//                        $scope.nodoInsertar = d;
                        if (d === true) {
                            swal("Mensaje del Sistema!", "Datos reenviados correctamente !.", "success");
//                            $('#add').modal('show');
                        } /*else {
                            swal("Mensaje del Sistema!", "El Usuario tiene todos los Accesos a las dependencias existentes.", "error");
                        }*/
                    });
                };

                $scope.registrarControlNodo = function () {
                    $scope.controlNodo = {};
                    $scope.controlNodo.controlAcceso = {};
                    $scope.controlNodo.nodo = {};
                    $scope.controlNodo.controlAcceso.idControl = $("#idControlInsertar").val();
                    var arrayDatos = "";
                    $("input:checkbox:checked", "#tableControlNodoInsertar").map(function () {
                        arrayDatos += $(this).val() + "-";
                    });
                    var res = arrayDatos.substring(0, (arrayDatos.length - 1));
                    ControlNodoService.crearPorArrayNodo($scope.controlNodo.controlAcceso.idControl, res).success(function (data) {
                        if (data === true) {
                            swal("Mensaje del Sistema!", "Datos actualizados exitosamente", "success");
                        }
                        $("#add").modal('hide');
                        $scope.listar();
                    }).error(function (e) {
                        $("#add").modal('hide');
                        $scope.listar();
                        console.log("NO SE PUDO CONECTAR CON EL SERVIDOR....");
                    });
                };
                $scope.detalle = function (ca) {
                    ControlAccesoService.listarPorControl(ca.idControl).success(function (data) {
                        if (parseInt(data.length) !== 0) {
                            $scope.controlNodo = data;
                            $('#modalDetalles').modal('show');
                        } else {
                            swal("Mensaje del Sistema!", "El Usuario no dispone de ningún acceso a edificios", "error");
                        }
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.salir = function () {
                    $('#modalDetalles').modal('hide');
                };
                $scope.listarByStatus();
            }]);