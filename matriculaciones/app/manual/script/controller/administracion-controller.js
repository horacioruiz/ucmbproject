'use strict'
app
        .controller('listAdminCtrl', ['$state', '$scope', 'config', 'AdminService', 'UsuarioEdificioService', 'NodoService', '$cookies', '$base64', '$filter', '$location', '$rootScope', '$http', function ($state, $scope, config, AdminService, UsuarioEdificioService, NodoService, $cookies, $base64, $filter, $location, $rootScope, $http) {
                $scope.filterText = "";
                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;

                $scope.dataY = [];
                $scope.dataX = [];
                var valorMin = 9999;
                var valorMax = 0;

                $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                $scope.userLog = $scope.jsonUsu.usuarioUsuario;

                var myMapEdificio = new Map();
                var myMapNodo = new Map();

                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };

                $scope.limpiar = function () {
                    document.getElementById("startDate").valueAsDate = null;
                    document.getElementById("endDate").valueAsDate = null;
                    $scope.filterText = "";
                };

                $scope.generarGrafico = function () {
                    $scope.dataX = [];
                    $scope.dataY = [];
                    if (!isNaN(new Date($scope.fechaDesde).getTime())) {
//                        valorMin = 9999;
//                        valorMax = 0;
                        $("#chart01").css("display", "none");
//                        $("#edificioPlaca").css("display", "none");
//                        $("#DependenciaComponente").css("display", "none");
                        var date = new Date($('#fechaDesde').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();

                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');

                        $scope.fec = fecha;
                        if ($scope.fec === "NaN-aN-aN") {
                            $scope.fec = null;
                        }
                        AdminService.generarGrafico(myMapNodo.get($scope.idNodo), $scope.fec).success(function (data) {
                            $scope.dataX = [];
                            $scope.dataY = [];
                            if (data.length === 0) {
                                swal("Mensaje del Sistema!", "No se encontraron resultados de la búsqueda.", "error");
                            } else {
                                var tamanho = data.length;
                                var count = 0;
                                angular.forEach(data, function (value, key) {
                                    $scope.estado = 0;
                                    if (value.temperatura.toUpperCase() === "A") {
                                        $scope.estado = 1;
                                    }
                                    $scope.sample = value.horaInicioAdmin;
                                    $scope.dataX.push($scope.sample);
//                                final
                                    $scope.dataX.push($scope.estado);
                                    $scope.dataY.push($scope.dataX);
                                    $scope.dataX = [];
//                                final
                                    count++;
                                    if (count === tamanho) {
                                        $scope.generarlo();
                                    }
                                });
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo conectar con el Servidor..", "error");
                        });
                    } else if ($scope.idNodo === 0) {
                        swal("Mensaje del Sistema!", "El campo Nodo no debe quedar vacío", "error");
                    } else {
                        swal("Mensaje del Sistema!", "El campo Fecha no debe quedar vacío", "error");
                    }
                };

                $scope.generarlo = function () {
//                    console.log("MIN: " + valorMin + " MAX: " + valorMax + "->> " + "'" + valorMin + ":" + valorMax + ":" + (valorMax / 10) + "'", );
                    if ($scope.idNodo === 0 || $scope.idEdificio === 0) {
                        swal("Mensaje del Sistema!", "El campo Edificio y Nodo no deben quedar vacío.", "error");
                    } else if (!isNaN(new Date($scope.fechaDesde).getTime())) {
                        $scope.myJson = {
                            type: 'line',
//                            type: 'bar',
                            title: {
                                text: 'Gráfico generado',
                                align: 'left',
                                fontColor: '#FFFFFF'
                            },
                            backgroundColor: "#30a5ff",
                            plot: {
                                lineWidth: "2px",
                                lineColor: "#FFFFFF",
                                aspect: "spline",
                                marker: {
                                    visible: true
                                }
                            },
                            plotarea: {
                            },
                            crosshairX: {
                                lineStyle: 'dashed',
                                lineWidth: 2,
                                lineColor: '#FFFFFF',
                                plotLabel: {
                                    visible: false
                                },
                                marker: {
                                    type: 'triangle',
                                    size: 5,
                                    visible: true
                                }
                            },
                            preview: {
                                live: true
                            },
                            scaleY: {
                                "values": "0:1:1",
//                                values: "'" + valorMin + ":" + valorMax + ":" + (valorMax / 10) + "'",
                                lineColor: "#000000",
                                lineWidth: "1px",
                                tick: {
                                    lineColor: "white",
                                    lineWidth: "1px"
                                },
                                guide: {
                                    lineStyle: "solid",
                                    lineColor: "#249178"
                                },
                                item: {
                                    fontColor: "white"
                                }
                            },
                            scaleX: {
                                zooming: true,
                                zoomTo: [0, 10],
                                lineWidth: "1px",
                                lineColor: '#000000',
                                itemsOverlap: true,
                                tick: {
                                    lineColor: '#FFFFFF',
                                    alpha: 1,
                                    lineWidth: 2
                                },
                                item: {
                                    fontColor: "#FFFFFF"
                                },
                                guide: {
                                    visible: false
                                },
                                values: $scope.dataX
                            },
                            'scale-y-2': {
                            },
                            series: [
                                {
                                    values: $scope.dataY
                                }]
                        };
//                        $scope.myJson = {
//
//                            "type": "line",
//                            title: {
//                                text: 'Gráfico generado',
//                                align: 'left',
//                                fontColor: '#FFFFFF'
//                            },
//                            backgroundColor: "#30a5ff",
//                            plot: {
//                                lineWidth: "2px",
//                                lineColor: "#FFFFFF",
//                                aspect: "spline",
//                                marker: {
//                                    visible: true
//                                }
//                            },
//                            plotarea: {
//                            },
//                            crosshairX: {
//                                lineStyle: 'dashed',
//                                lineWidth: 2,
//                                lineColor: '#FFFFFF',
//                                plotLabel: {
//                                    visible: false
//                                },
//                                marker: {
//                                    type: 'triangle',
//                                    size: 5,
//                                    visible: true
//                                }
//                            },
//                            preview: {
//                                live: true
//                            },
////                            "scale-x": {
////                                "min-value": "1457101800000",
////                                "max-value": "1457125200000",
////                                "step": "30minute",
////                                "transform": {
////                                    "type": "date"
////                                },
////                                "label": {
////                                    "text": "Trading Day"
////                                }
////                                "item": {
////                                    "font-size": 10
////                                },
////                                "max-items": 14
////                            },
//                            scaleX: {
//                                zooming: true,
//                                zoomTo: [0, 30],
//                                lineWidth: "1px",
//                                lineColor: '#000000',
//                                itemsOverlap: true,
//                                tick: {
//                                    lineColor: '#FFFFFF',
//                                    alpha: 1,
//                                    lineWidth: 2
//                                },
//                                item: {
//                                    fontColor: "#FFFFFF"
//                                },
//                                guide: {
//                                    visible: false
//                                }
//                            },
//                            scaleY: {
//                                "values": "0:1:1",
////                                "values": "ENCENDIDO:APAGADO",
////                                values: "'" + valorMin + ":" + valorMax + ":" + (valorMax / 10) + "'",
//                                lineColor: "#000000",
//                                lineWidth: "1px",
//                                tick: {
//                                    lineColor: "white",
//                                    lineWidth: "1px"
//                                },
//                                guide: {
//                                    lineStyle: "solid",
//                                    lineColor: "#249178"
//                                },
//                                item: {
//                                    fontColor: "white"
//                                }
//                            },
//                            "series": [
//                                {
////                                    values: $scope.dataY
//                                    "values": [
//                                        ['12:25', 1], //03/04/2016 at 9:30 a.m. EST (which is 14:30 in GMT)
//                                        ['12:28', 0], //10:00 a.m.
//                                        ['12:50', 1], //10:30 a.m.
//                                        ['06/04/2016', 1], //11:00 a.m.
//                                        ['0/04/2016', 0], //11:30 a.m.
//                                        ['07/04/2016', 1], //12:00 p.m.
//                                        ['08/04/2016', 0], //12:30 p.m.
//                                        ['09/04/2016', 1], //1:00 p.m.
//                                        ['10/04/2016', 0], //1:30 p.m.
//                                        ['11/04/2016', 1], //2:00 p.m.
//                                        ['12/04/2016', 0], //2:30 p.m.
//                                        ['01/06/2016', 1], //3:00 p.m.
//                                        ['02/06/2016', 0], //3:30 p.m.
//                                        ['03/06/2016', 1] //at 4:00 p.m. EST (which is 21:00 GMT)
//                                    ]
//                                }
//                            ]
//                        };
                        $("#chart01").fadeIn("slow");
                    } else {
                        swal("Mensaje del Sistema!", "El campo Fecha no debe quedar vacío", "error");
                    }
                };
                $scope.atrasite = function () {
                    $("#addChart").css("display", "none");
                    $("#menuInicial").css("display", "inline");
                };
                $scope.generar = function () {
                    $("#menuInicial").css("display", "none");
                    $("#addChart").css("display", "inline");
                    $("#chart01").css("display", "none");
                    $("#edificioPlaca").css("display", "none");
                    $("#DependenciaComponente").css("display", "none");
                    $scope.nestedItemsLevelEdificio = [];
                    $scope.idEdificio = 0;
                    $scope.fechaDesde = "";
                    $scope.fechaHasta = "";
                    UsuarioEdificioService.getByIdUsu($scope.jsonUsu.idUsuario).success(function (data) {
                        angular.forEach(data, function (value, key) {
                            $scope.nestedItemsLevelEdificio.push(value.edificio.descriEdificio);
                            myMapEdificio.set(value.edificio.descriEdificio, value.edificio.idEdificio);
                        });
                    });
                };

                $scope.onChangeEdificio = function () {
                    $scope.nestedItemsLevel1 = [];
                    $scope.idNodo = 0;
                    $("#edificioPlaca").css("display", "none");
                    $("#DependenciaComponente").css("display", "none");
                    NodoService.listarPorEdificio(myMapEdificio.get($scope.idEdificio)).success(function (data) {
                        angular.forEach(data, function (value, key) {
                            $scope.nestedItemsLevel1.push(value.descriNodo);
                            myMapNodo.set(value.descriNodo, value.idNodo);
                        });
                    });
                };

                $scope.onChange = function () {
                    $("#edificioPlaca").css("display", "none");
                    $scope.nodoPinAgregar = {};
                    $("#DependenciaComponente").css("display", "none");
                    NodoService.listarPorId(myMapNodo.get($scope.idNodo)).success(function (data) {
                        $scope.nodoPinAgregar.ipPlaca = data.placa.ipPlaca;
                        $scope.nodoPinAgregar.descriPlaca = data.placa.descriPlaca;
                        $scope.nodoPinAgregar.descriTipoPlaca = data.placa.tipoPlaca.descriTipo;
                        $scope.nodoPinAgregar.descriDependencia = data.dependencia.descriDependencia;
                        $scope.nodoPinAgregar.descriComponente = data.componente.descriComponente;
                        $("#edificioPlaca").css("display", "inline");
                        $("#DependenciaComponente").css("display", "inline");
                    });
                };


                $scope.paginar = function () {
                    AdminService.count().success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.startDate === "" || $scope.startDate === undefined) {
                        $scope.start = null;
                    } else {
                        var date = new Date($('#startDate').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();

                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');

                        $scope.start = fecha;
                        if ($scope.start === "NaN-aN-aN") {
                            $scope.start = null;
                        }
                    }
                    if ($scope.endDate === "" || $scope.endDate === undefined) {
                        $scope.end = null;
                    } else {
                        var date = new Date($('#endDate').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();

                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');

                        $scope.end = fecha;
                        if ($scope.end === "NaN-aN-aN") {
                            $scope.end = null;
                        }
                    }
                    AdminService.countFiltro($scope.filt, $scope.start, $scope.end).success(function (data) {
                        $scope.catidad = data;

                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }

                    if ($scope.startDate === "" || $scope.startDate === undefined) {
                        $scope.start = null;
                    } else {
                        var date = new Date($('#startDate').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();

                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');

                        $scope.start = fecha;
                        if ($scope.start === "NaN-aN-aN") {
                            $scope.start = null;
                        }
                    }
                    if ($scope.endDate === "" || $scope.endDate === undefined) {
                        $scope.end = null;
                    } else {
                        var date = new Date($('#endDate').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();

                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');

                        $scope.end = fecha;
                        if ($scope.end === "NaN-aN-aN") {
                            $scope.end = null;
                        }
                    }
                    $http.get(config.backend + '/administracion/generarReporte/' + $scope.filt + '/' + $scope.start + '/' + $scope.end + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                };

                $scope.reporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }

                    if ($scope.startDate === "" || $scope.startDate === undefined) {
                        $scope.start = null;
                    } else {
                        var date = new Date($('#startDate').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();

                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');

                        $scope.start = fecha;
                        if ($scope.start === "NaN-aN-aN") {
                            $scope.start = null;
                        }
                    }
                    if ($scope.endDate === "" || $scope.endDate === undefined) {
                        $scope.end = null;
                    } else {
                        var date = new Date($('#endDate').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();

                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');

                        $scope.end = fecha;
                        if ($scope.end === "NaN-aN-aN") {
                            $scope.end = null;
                        }
                    }

                    $scope.result = {};

                    if ($scope.start === null && $scope.end === null) {
                        $http.get(config.backend + '/administracion/reportarPorDescripcion/' + $scope.filt + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                                .success(function (data) {
                                    var file = new Blob([data], {type: 'application/pdf'});
                                    var fileURL = URL.createObjectURL(file);
                                    window.open(fileURL);
                                });
                    } else if ($scope.start === null || $scope.end === null) {
                        swal("Mensaje del Sistema!", "Los campos de fecha deben tener un inicio y un fin para generar Reporte", "error");
                    } else {
                        $http.get(config.backend + '/administracion/reportarPorFecha/' + $scope.filt + "/" + $scope.start + "/" + $scope.end + "/" + $scope.userLog, {responseType: 'arraybuffer'})
                                .success(function (data) {
                                    var file = new Blob([data], {type: 'application/pdf'});
                                    var fileURL = URL.createObjectURL(file);
                                    window.open(fileURL);
                                });
                    }
                }

                $rootScope.$on("$locationChangeStart", function (event, data) {
                    var pag = parseInt(data.replace("http://" + config.ipFront + ":8017/menu.html#/:pagina=", ""));
                    if (!isNaN(pag)) {
                        $("#paginaActual").val(pag);
                        $scope.buscarSegunPagina();
                    }
                });

                $scope.listar = function () {
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    AdminService.fetch(config.cantPaginacion, offSet).success(function (data) {
                        $scope.admin = data;
                        $scope.paginar();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.listarConFiltro = function () {
                    if ($scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.startDate === "" || $scope.startDate === undefined) {
                        $scope.start = null;
                    } else {
                        var date = new Date($('#startDate').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();

                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');

                        $scope.start = fecha;
                        if ($scope.start === "NaN-aN-aN") {
                            $scope.start = null;
                        }
                    }
                    if ($scope.endDate === "" || $scope.endDate === undefined) {
                        $scope.end = null;
                    } else {
                        var date = new Date($('#endDate').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();

                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');

                        $scope.end = fecha;
                        if ($scope.end === "NaN-aN-aN") {
                            $scope.end = null;
                        }
                    }
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    AdminService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt, $scope.start, $scope.end).success(function (data) {
                        $scope.admin = data;
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.buscar = function () {
                    $scope.admin = [];
                    location.href = "http://" + config.ipFront + ":8017/menu.html#/:pagina=1";
                    if ($scope.filterText !== "" || $scope.startDate !== "" || $scope.endDate !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };

                $scope.buscarSegunPagina = function () {
                    $scope.admin = [];
//                    location.href = "http://" + config.ipFront + ":8017/menu.html#/:pagina=1";
                    if ($scope.filterText !== "" || $scope.startDate !== "" || $scope.endDate !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };

                $scope.atras = function () {
                    location.reload();
                };

                $scope.$watch('filterText', function () {
                    $("#paginaActual").val("1");
                })
                $scope.$watch('startDate', function () {
                    $("#paginaActual").val("1");
                })
                $scope.$watch('endDate', function () {
                    $("#paginaActual").val("1");
                })
                $scope.buscar();
            }])
