'use strict'
app
        .controller('listModulosCtrl', ['$state', '$scope', 'config', 'ModuloService', '$cookies', '$base64', function ($state, $scope, config, ModuloService, $cookies, $base64) {
//                console.log("HOLAAA");

                $scope.modulos = {};
                $scope.moduloAgregar = {};
                $scope.moduloModificar = {};

                $scope.page = 1;
                $scope.pageChanged = function () {
                    var startPos = ($scope.page - 1) * 10;
                    $scope.displayItems = $scope.totalItems.slice(startPos, startPos + 10);
                    console.log($scope.page);
                };
//                FIN DE PAGINACION

                $scope.listar = function () {
                    ModuloService.listar().success(function (data) {
                        $scope.modulos = data;
                        $scope.displayItems = $scope.modulos.slice(0, 5);
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.registrar = function () {
//                    alert($scope.rolAgregar.descripcionRolAgregar);
                    if ($scope.moduloAgregar.descripcionModulo === "" || $scope.moduloAgregar.descripcionModulo === undefined) {
                        swal("Mensaje del Sistema!", "Descripción no debe quedar vacío", "error");
                    } else {
                        ModuloService.crear($scope.moduloAgregar).success(function (data) {
                            if (data === true) {
                                swal("Mensaje del Sistema!", "Datos Agregados exitosamente", "success");
                                $('#modalAgregar').modal('hide');
                                $scope.moduloAgregar = {};
                                $scope.listar();
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Agregados", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };
                $scope.actualizar = function () {
//                    alert($scope.rolAgregar.descripcionRolAgregar);
                    if ($scope.moduloModificar.descripcionModulo === "" || $scope.moduloModificar.descripcionModulo === undefined || $scope.moduloModificar.descripcionModulo === null) {
                        swal("Mensaje del Sistema!", "Descripción no debe quedar vacío", "error");
                    } else {
                        ModuloService.actualizar($scope.moduloModificar).success(function (data) {
                            if (data === true) {
                                swal("Mensaje del Sistema!", "Datos Modificados exitosamente", "success");
                                $('#modalModificar').modal('hide');
                                $scope.moduloModificar = {};
                                $scope.listar();
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Modificados", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };

                $scope.agregar = function () {
                    $scope.moduloAgregar.descripcionModulo = "";
                };

                $scope.modificar = function (modulos) {
                    $scope.moduloModificar.descripcionModulo = modulos.descripcionModulo;
                    $scope.moduloModificar.idModulo = modulos.idModulo;
                    $("#modalModificar").modal("show");
                };

//                $scope.eliminar = function (mod) {
////                    var txt;
//                    var r = confirm("Seguro que quiere eliminar el módulo " + mod.descripcionModulo + "?");
//                    if (r === true) {
//                        ModuloService.eliminar(mod.idModulo).success(function (data) {
//                            if (data === true) {
//                                swal('Eliminado!',
//                                        'Haz eliminado exitosamente.',
//                                        'success'
//                                        );
//                                $scope.listar();
//                            }
//                        });
//                    } else {
//                        swal('Cancelado',
//                                'Haz cancelado la eliminación',
//                                'error'
//                                );
//                    }
//                };

                $scope.eliminar = function (mod) {
                    swal({
                        title: "Mensaje del Sistema",
                        text: "Seguro que quiere eliminar el módulo " + mod.descripcionModulo + "?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    ModuloService.eliminar(mod.idModulo).success(function (data) {
                                        if (data === true) {
                                            swal('Eliminado!',
                                                    'Haz eliminado exitosamente.',
                                                    'success'
                                                    );
                                            $scope.listar();
                                        }
                                    }).error(function (e) {
                                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                    });
                                } else {
                                    swal("Mensaje del Sistema", "Haz cancelado la operación!", "error");
                                }
                            });
                };

                $scope.listar();
            }]);