'use strict'
app
        .controller('listSensorCtrl', ['$state', '$scope', 'config', 'SensorNodoService', 'SensoresService', 'NodoService', '$cookies', '$base64', '$http', '$rootScope', function ($state, $scope, config, SensorNodoService, SensoresService, NodoService, $cookies, $base64, $http, $rootScope) {
                $scope.sensores = {};
                $scope.sensorAgregar = {};
                $scope.sensorModificar = {};
                $scope.sensorNodoDetalle = [];
                $scope.nodos = [];
                $scope.idNodo = "";
                $("#mainMenu").css("display", "inline");
                $("#addSensores").css("display", "none");
                $("#updateSensores").css("display", "none");

                var myMap = new Map();
//                var myMapPersona = new Map();
//                var myMapTipo = new Map();
//                var myMapCiudad = new Map();

                $scope.sensorAgregar.componente = {};
                $scope.sensorAgregar.dependencia = {};
                $scope.sensorAgregar.placa = {};
                $scope.sensorAgregar.edificio = {};
                $scope.sensorAgregar.entidad = {};

                $scope.sensorModificar.componente = {};
                $scope.sensorModificar.dependencia = {};
                $scope.sensorModificar.placa = {};
                $scope.sensorModificar.edificio = {};
                $scope.sensorModificar.entidad = {};
                var myMapPin = new Map();

                $scope.nestedItemsLevelDependencia = [];

                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };

                $scope.aModificar = "";

                $scope.limpiar = function () {
                    $scope.filterText = "";
                    document.getElementById("fecha").valueAsDate = null;
                };

//               pagination

                $scope.paginar = function () {
                    SensoresService.count().success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $rootScope.$on("$locationChangeStart", function (event, data) {
                    var pag = parseInt(data.replace("http://" + config.ipFront + ":8017/menu.html#/:pagina=", ""));
                    if (!isNaN(pag)) {
                        $("#paginaActual").val(pag);
                        $scope.buscarSegunPagina();
                    }
                });

                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.fecha === "" || $scope.fecha === undefined) {
                        $scope.fec = null;
                    } else {
                        var date = new Date($('#fecha').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();
                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');
                        $scope.fec = fecha;
                        if ($scope.fec === "NaN-aN-aN") {
                            $scope.fec = null;
                        }
                    }
                    SensoresService.countFiltro($scope.filt, $scope.fec).success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
//                    $("#paginacionesEdificios").html('<ul class="pagination"><li class="disabled"><a href="#">&laquo;</a></li><li class="active"><a href="#">1</a></li><li ><a href="#">2</a></li><li><a href="#">&raquo;</a></li></ul>');
                };

                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.fecha === "" || $scope.fecha === undefined) {
                        $scope.fec = null;
                    } else {
                        var date = new Date($('#fecha').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();
                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');
                        $scope.fec = fecha;
                        if ($scope.fec === "NaN-aN-aN") {
                            $scope.fec = null;
                        }
                    }
                    $http.get(config.backend + '/sensorNodo/generarReporte/' + $scope.filt + '/' + $scope.fec + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                };

                $scope.listarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.fecha === "" || $scope.fecha === undefined) {
                        $scope.fec = null;
                    } else {
                        var date = new Date($('#fecha').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();
                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');
                        $scope.fec = fecha;
                        if ($scope.fec === "NaN-aN-aN") {
                            $scope.fec = null;
                        }
                    }
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    SensoresService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt, $scope.fec).success(function (data) {
                        $scope.sensores = data;
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.buscar = function () {
                    $scope.sensores = [];
                    location.href = "http://" + config.ipFront + ":8017/menu.html#/:pagina=1";
                    if ($scope.filterText !== "" || $scope.fecha !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
//                    }
                };

                $scope.buscarSegunPagina = function () {
                    $scope.sensores = [];
                    if ($scope.filterText !== "" || $scope.fecha !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };

//                FIN DE PAGINACION

                $scope.listar = function () {
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    SensoresService.fetch(config.cantPaginacion, offSet).success(function (data) {
                        $scope.sensores = data;
                        $scope.paginar();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

//                ------------------------------------------------------------------------------------
                $scope.agregar = function () {
                    $("#guardando").css("display", "none");
                    $("#saliendo").css("display", "none");
                    $("#mainMenu").css("display", "none");
                    $("#addSensores").css("display", "inline");
                    $("#listBox").css("display", "none");
                    $scope.nodos = [];

                    $scope.sensorAgregar.edificio = "";
                    $scope.sensorAgregar.dependencia = "";
                    $scope.sensorAgregar.componente = "";

                    $scope.idNodo = ""
                    $scope.sensorAgregar.nodo = {};
                    $scope.sensorAgregar.valor = "";
                    myMap = new Map();
                    $scope.nestedItemsLevelNodo = [];
                    NodoService.listarPorSensores().success(function (data) {
                        angular.forEach(data, function (value, key) {
                            $scope.nestedItemsLevelNodo.push(value.descriNodo);
                            myMap.set(value.descriNodo, value.idNodo);
                        });
                    });
                };

                $scope.modificar = function (rl) {
                    $("#guardando").css("display", "none");
                    $("#saliendo").css("display", "none");
                    $("#mainMenu").css("display", "none");
                    $("#updateSensores").css("display", "inline");
                    $("#listBox").css("display", "none");
                    $("#none").fadeOut("slow");
                    $("#idSensor").val(rl.idSensor);
                    $scope.nodos = [];
                    var dato = false;

                    $scope.sensorModificar.nodo = rl.nodo.descriNodo;
                    $scope.sensorModificar.edificio = rl.nodo.edificio.descriEdificio;
                    $scope.sensorModificar.dependencia = rl.nodo.dependencia.descriDependencia;
                    $scope.sensorModificar.componente = rl.nodo.componente.descriComponente;
                    $scope.sensorModificar.valor = rl.valor;
                    $("#modIdNodo").val(rl.nodo.edificio.idEdificio);

                    if (rl.nodo.componente.descriComponente.toUpperCase() === "MOVIMIENTO" ||
                            rl.nodo.componente.descriComponente.toUpperCase() === "MAGNETICO") {
//                        $("#valorUpdate").css("disabled", true);
                        document.getElementById('valorUpdate').readOnly = true;
                        $("#valorUpdate").val("1");
                    } else {
//                        $("#valorUpdate").css("disabled", false);
                        document.getElementById('valorUpdate').readOnly = false;
                        $("#valorUpdate").val("");
                    }

                    if (rl.estado === true) {
                        $("#estadoSensor").prop("checked", true);
                    } else {
                        $("#estadoSensor").prop("checked", false);
                    }

                    SensorNodoService.listarPorSensor(rl.idSensor).success(function (d) {
                        $scope.listaDeSeleccionados = "";
                        angular.forEach(d, function (value, key) {

                            var val = ""
                            for (var i = 0; i < value.nodo.descriNodo.length; i++) {
                                val += value.nodo.descriNodo.charAt(i).replace(" ", "_");
                            }

                            $scope.listaDeSeleccionados += "<li class=\"list-group-item ng-binding \">" + value.nodo.descriNodo +
                                    "<input type=\"hidden\" id=\"datoUpdate" + val + "\" value=\"" + value.nodo.idNodo
                                    + "\"></li>";
                            dato = true;
                        });
                        $("#listSeleccionadoUpdate").html($scope.listaDeSeleccionados);
                        if (dato === false) {
//                            $("#listBoxModificar").hide();
                            $scope.listaBoxSinSelecionar();
                        } else {
//                            $("#listBoxModificar").show();
                            $scope.listaBoxSinSelecionar();
                        }
//                        $("#primeraLista").hide();
                    });
                };

                $scope.listaBoxSinSelecionar = function () {
                    var myMapSelect = new Map();
                    $scope.nodos = [];
                    $("#checkAllUpdate1").removeClass();
                    $("#checkAllUpdate2").removeClass();
                    $("#checkAllUpdate1").addClass("glyphicon glyphicon-unchecked");
                    $("#checkAllUpdate2").addClass("glyphicon glyphicon-unchecked");
                    $("#listSeleccionadoUpdate li").each(function (index) {
                        var valorSeleccionado = $.trim($(this).text());
                        var val = "";
                        for (var i = 0; i < valorSeleccionado.length; i++) {
                            val += valorSeleccionado.charAt(i).replace(" ", "_");
                        }
                        myMapSelect.set(val, val);
                        $("#listBoxModificar").show();
                    });
//                    alert($("#modIdNodo").val());
                    NodoService.getByEdificiOnlyComp($("#modIdNodo").val()).success(function (d) {
                        angular.forEach(d, function (value, key) {
                            var descri = value.descriNodo;
                            var val = ""
                            for (var i = 0; i < descri.length; i++) {
                                val += descri.charAt(i).replace(" ", "_");
                            }
                            var mapeo = myMapSelect.get(val);
                            if (mapeo === undefined) {
                                var newstr = val;

                                value.descriNod = newstr;
                                $scope.nodos.push(value);
                            }
                        });
                    });
                };

                $scope.backUpdate = function () {
                    var myMap = new Map();
                    $scope.nodos = [];
                    $("#listSeleccionadoUpdate li").each(function (index) {
                        var valorSeleccionado = $.trim($(this).text());
                        var val = "";
                        for (var i = 0; i < valorSeleccionado.length; i++) {
                            val += valorSeleccionado.charAt(i).replace(" ", "_");
                        }
                        myMap.set(val, val);
                        $("#listBoxModificar").show();
                    });

                    NodoService.getByEdificiOnlyComp($("#modIdNodo").val()).success(function (d) {
                        angular.forEach(d, function (value, key) {
                            var descri = value.descriNodo;
                            var val = "";
                            for (var i = 0; i < descri.length; i++) {
                                val += descri.charAt(i).replace(" ", "_");
                            }
                            var mapeo = myMap.get(val);
                            var newstr = val;
                            if (mapeo === undefined) {
                                value.descriNod = newstr;
                                $scope.nodos.push(value);
                                $("#listBoxModificar").show();
                            }
                        });
                    });
                };


                $scope.change = function () {
                    $scope.nodos = [];
                    $("#listBox").css("display", "inline");
                    $scope.sensorAgregar.nodo = {};
                    $scope.sensorAgregar.nodo.idNodo = myMap.get($scope.idNodo);
                    NodoService.listarPorId($scope.sensorAgregar.nodo.idNodo).success(function (data) {
                        $("#idEdi").val(data.edificio.idEdificio);
                        $scope.sensorAgregar.edificio = data.edificio.descriEdificio;
                        $scope.sensorAgregar.dependencia = data.dependencia.descriDependencia;
                        $scope.sensorAgregar.componente = data.componente.descriComponente;
                        if (data.componente.descriComponente.toUpperCase() === "MOVIMIENTO" ||
                                data.componente.descriComponente.toUpperCase() === "MAGNETICO" ||
                                data.componente.descriComponente.toUpperCase() === "CONTROL ACCESO") {
//                            $("#valorAdd").readOnly = true;
                            document.getElementById('valorAdd').readOnly = true;
                            $("#valorAdd").val("1");
                        } else {
//                            $("#valorAdd").readOnly = false;
                            document.getElementById('valorAdd').readOnly = false;
                            $("#valorAdd").val("");
                        }

                        NodoService.getByEdificiOnlyComp(data.edificio.idEdificio).success(function (d) {
                            angular.forEach(d, function (value, key) {
                                var descri = value.descriNodo;
                                var val = ""
                                for (var i = 0; i < descri.length; i++) {
                                    val += descri.charAt(i).replace(" ", "_");
                                }
                                var newstr = val;

                                value.descriNod = newstr;
                                $scope.nodos.push(value);
                            });
                        });
                    });
                };

                $scope.atrasite = function () {
                    $("#guardando").css("display", "inline");
                    $("#saliendo").css("display", "inline");
//                    $scope.rolAgregar.descripcionRol = $scope.descripcion;
                    $scope.buscar();
                    $("#mainMenu").css("display", "inline");
                    $("#addSensores").hide();
                    $("#updateSensores").hide();

                };
                $scope.detalle = function (rl) {
                    SensorNodoService.listarPorSensor(rl.idSensor).success(function (d) {
                        $scope.sensorNodoDetalle = d;
                        $("#modalDetalles").modal('show');
                    });
                };
                $scope.salirModal = function () {
                    $("#modalDetalles").modal('hide');
                };
                $scope.registrar = function () {
                    $scope.sensorAgregar.nodo = {};
                    if ($scope.idNodo === "") {
                        swal("Mensaje del Sistema!", "Debes seleccionar un Nodo para el registro", "error");
                    } else {
                        $scope.sensorAgregar.nodo.idNodo = myMap.get($scope.idNodo);
                        $scope.sensorAgregar.descripcion = $scope.idNodo;
                        $scope.sensorAgregar.estado = true;
                        $scope.sensorAgregar.valor = $("#valorAdd").val();
                        $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                        $scope.sensorAgregar.usuario = $scope.jsonUsu.usuarioUsuario;

                        SensoresService.crear($scope.sensorAgregar).success(function (data) {
                            if (parseInt(data) !== 0) {
                                swal("Mensaje del Sistema!", "Datos Agregados exitosamente", "success");
                                var element = $('#listSeleccionado li').length;
                                $scope.sensorAgregar = {};
                                $("#listSeleccionado li").each(function (index) {
                                    var valorSeleccionado = $.trim($(this).text());

                                    var val = ""
                                    for (var i = 0; i < valorSeleccionado.length; i++) {
                                        val += valorSeleccionado.charAt(i).replace(" ", "_");
                                    }


                                    var id = $("#dato" + val).val();
                                    $scope.sensorNodo = {};
                                    $scope.sensorNodo.sensor = {};
                                    $scope.sensorNodo.nodo = {};
                                    $scope.sensorNodo.nodo.idNodo = id;
                                    $scope.sensorNodo.sensor.idSensor = parseInt(data);
                                    SensorNodoService.crear($scope.sensorNodo).success(function (data) {
                                        console.log("DATOS INSERTADO EN USUARIO_ROL....");
                                    }).error(function (e) {
                                        console.log("ERROR DE CONEXION CON EL SERVIDOR...");
                                    });
                                });
//                                    $("#listBox").hide();
                                $("#listSeleccionado li").each(function (index) {
                                    $(this).remove();
                                });
//                                $scope.cargarPersonas();
//                                $scope.back();
                                $("#listBox").css("display", "none");
                                $("#mainMenu").css("display", "inline");
                                $("#addSensores").hide();
                                $("#updateSensores").hide();
                                $("#guardando").css("display", "inline");
                                $("#saliendo").css("display", "inline");
                                $scope.buscar();
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Agregados o Cambie la descripción usuario", "error");
                                $("#listSeleccionado li").each(function (index) {
                                    $(this).remove();
                                });
//                                $scope.cargarPersonas();
//                                $scope.back();
                            }
                            //                                limpiar
                            $scope.sensorAgregar.edificio = "";
                            $scope.sensorAgregar.dependencia = "";
                            $scope.sensorAgregar.componente = "";

                            $scope.idNodo = ""
                            $scope.sensorAgregar.nodo = {};
                            $scope.sensorAgregar.valor = "";
                            myMap = new Map();
                            $scope.nestedItemsLevelNodo = [];

                        }).error(function (e) {
                        });
                    }
                };

                $scope.actualizar = function () {
                    $("#idSensor").val();
                    var valor = 0;
                    if ($scope.sensorModificar.valor === null || $scope.sensorModificar.valor === undefined) {
                        valor = 0;
                    } else {
                        valor = $scope.sensorModificar.valor;
                    }
                    SensoresService.update($("#idSensor").val(), $("#estadoSensor").prop("checked"), valor).success(function (d) {
                        if (d === true) {
                            SensorNodoService.deleteData($("#idSensor").val()).success(function (data) {
                                if (data === true) {
                                    swal("Mensaje del Sistema!", "Datos no Actualizados exitosamente", "success");
                                    $scope.volverRegistro();
                                }
                            });
                        }
                    }).error(function (e) {
                    });
                };

                $scope.volverRegistro = function () {
                    var element = $('#listSeleccionadoUpdate li').length;
                    $scope.sensorAgregar = {};
                    $("#listSeleccionadoUpdate li").each(function (index) {
                        var valorSeleccionado = $.trim($(this).text());

                        var value = "";
                        for (var i = 0; i < valorSeleccionado.length; i++) {
                            value += valorSeleccionado.charAt(i).replace(" ", "_");
                        }
                        var id = $("#datoUpdate" + value).val();
                        $scope.sensorNodo = {};
                        $scope.sensorNodo.sensor = {};
                        $scope.sensorNodo.nodo = {};
                        $scope.sensorNodo.nodo.idNodo = id;
                        $scope.sensorNodo.sensor.idSensor = parseInt($("#idSensor").val());
                        SensorNodoService.crear($scope.sensorNodo).success(function (data) {
                            console.log("DATOS INSERTADO EN USUARIO_ROL....");
                        }).error(function (e) {
                            console.log("ERROR DE CONEXION CON EL SERVIDOR...");
                        });
                    });
//                                    $("#listBox").hide();
                    $("#listSeleccionadoUpdate li").each(function (index) {
                        $(this).remove();
                    });
//                                $scope.cargarPersonas();
//                                $scope.back();
                    $("#listBoxModificar").css("display", "none");
                    $("#mainMenu").css("display", "inline");
                    $("#addSensores").hide();
                    $("#updateSensores").hide();
                    $("#guardando").css("display", "inline");
                    $("#saliendo").css("display", "inline");
                    $scope.buscar();
                };

                $scope.$watch('filterText', function () {
                    $("#paginaActual").val("1");
                })
                $scope.$watch('fecha', function () {
                    $("#paginaActual").val("1");
                })
                $scope.atras = function () {
                    location.reload();
                };
                $scope.buscar();

                $scope.back = function () {
                    var myMap = new Map();
                    $scope.nodos = [];
                    $("#listSeleccionado li").each(function (index) {
                        var valorSeleccionado = $.trim($(this).text());
                        myMap.set(valorSeleccionado, valorSeleccionado);
                        $("#listBox").show();
                    });
                    NodoService.getByEdificiOnlyComp($("#idEdi").val()).success(function (d) {
                        angular.forEach(d, function (value, key) {

                            var descri = value.descriNodo;
                            var mapeo = myMap.get(descri);
                            if (mapeo === undefined) {
                                var val = ""
                                for (var i = 0; i < descri.length; i++) {
                                    val += descri.charAt(i).replace(" ", "_");
                                }
                                var newstr = val;
//                                var newstr = descri.replace(" ", "_");
                                value.descriNod = newstr;
                                $scope.nodos.push(value);
                                $("#listBox").show();
                            }
                        });
                    });
                };


                $(function () {

                    $('#listBox').on('click', '.list-group .list-group-item', function () {
                        $(this).toggleClass('active');
                    });
                    $('#listBoxModificar').on('click', '.list-group .list-group-item', function () {
                        $(this).toggleClass('active');
                    });
                    $('.list-arrows button').click(function () {
                        var $button = $(this), actives = '';
                        if ($button.hasClass('move-left')) {
                            actives = $('.list-right ul li.active');
//                            actives.clone().appendTo('.list-left ul');
                            actives.remove();
                        } else if ($button.hasClass('move-right')) {
                            actives = $('.list-left ul li.active');
                            actives.clone().appendTo('.list-right ul');
                            actives.remove();
                        }
                    });
                    $('.dual-list .selector').click(function () {
                        var $checkBox = $(this);
                        if (!$checkBox.hasClass('selected')) {
                            $checkBox.addClass('selected').closest('.well').find('ul li:not(.active)').addClass('active');
                            $checkBox.children('i').removeClass('glyphicon-unchecked').addClass('glyphicon-check');
                        } else {
                            $checkBox.removeClass('selected').closest('.well').find('ul li.active').removeClass('active');
                            $checkBox.children('i').removeClass('glyphicon-check').addClass('glyphicon-unchecked');
                        }
                    });
                    $('[name="SearchDualList"]').keyup(function (e) {
                        var code = e.keyCode || e.which;
                        if (code === '9')
                            return;
                        if (code === '27')
                            $(this).val(null);
                        var $rows = $(this).closest('.dual-list').find('.list-group li');
                        var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
                        $rows.show().filter(function () {
                            var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
                            return !~text.indexOf(val);
                        }).hide();
                    });
                });
            }]);