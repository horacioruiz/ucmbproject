'use strict'
app
        .controller('listPersonasCtrl', ['$state', '$scope', 'config', 'PersonaService', '$cookies', '$base64', "$filter", '$http', '$rootScope', function ($state, $scope, config, PersonaService, $cookies, $base64, $filter, $http, $rootScope) {
//                console.log("HOLAAA");

                $scope.personas = {};
                $scope.personaAgregar = {};
                $scope.personaModificar = {};

                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };


                //               pagination
                $scope.listar = function () {
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    PersonaService.fetch(config.cantPaginacion, offSet).success(function (data) {
                        $scope.personas = data;
                        $scope.paginar();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.paginar = function () {
                    PersonaService.count().success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $rootScope.$on("$locationChangeStart", function (event, data) {
                    var pag = parseInt(data.replace("http://" + config.ipFront + ":8017/menu.html#/:pagina=", ""));
                    if (!isNaN(pag)) {
                        $("#paginaActual").val(pag);
                        $scope.buscarSegunPagina();
                    }
                });

                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.apellido === "" || $scope.apellido === undefined) {
                        $scope.ape = null;
                    } else {
                        $scope.ape = $scope.apellido;
                    }
                    if ($scope.ci === "" || $scope.ci === undefined) {
                        $scope.ced = null;
                    } else {
                        $scope.ced = $scope.ci;
                    }
                    PersonaService.countFiltro($scope.filt, $scope.ape, $scope.ced).success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
//                    $("#paginacionesPersonas").html('<ul class="pagination"><li class="disabled"><a href="#">&laquo;</a></li><li class="active"><a href="#">1</a></li><li ><a href="#">2</a></li><li><a href="#">&raquo;</a></li></ul>');
                };

                $scope.listarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.apellido === "" || $scope.apellido === undefined) {
                        $scope.ape = null;
                    } else {
                        $scope.ape = $scope.apellido;
                    }
                    if ($scope.ci === "" || $scope.ci === undefined) {
                        $scope.ced = null;
                    } else {
                        $scope.ced = $scope.ci;
                    }

                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    PersonaService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt, $scope.ape, $scope.ced).success(function (data) {
                        $scope.personas = data;
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.buscar = function () {
                    $scope.personas = [];
                    location.href = "http://" + config.ipFront + ":8017/menu.html#/:pagina=1";
                    if ($scope.filterText !== "" || $scope.apellido !== "" || $scope.ci !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
//                    }
                };

                $scope.buscarSegunPagina = function () {
                    $scope.personas = [];
                    if ($scope.filterText !== "" || $scope.apellido !== "" || $scope.ci !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };
//                FIN DE PAGINACION

                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.apellido === "" || $scope.apellido === undefined) {
                        $scope.ape = null;
                    } else {
                        $scope.ape = $scope.apellido;
                    }
                    if ($scope.ci === "" || $scope.ci === undefined) {
                        $scope.ced = null;
                    } else {
                        $scope.ced = $scope.ci;
                    }

                    $http.get(config.backend + '/personas/generarReporte/' + $scope.ced + '/' + $scope.filt + '/' + $scope.ape + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });

                }

                $scope.limpiar = function () {
                    $scope.filterText = "";
                    $scope.apellido = "";
                    $scope.ci = "";
                };

//                $scope.personaModificar.fechaNac = new Date();
                $scope.personaAgregar.fechaNac = new Date();
                $scope.listar = function () {
                    PersonaService.listar().success(function (data) {
                        $scope.personas = data;
                        //               pagination
                        $scope.paginar();
//               FIN DE pagination
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.registrar = function () {
//                    alert($scope.rolAgregar.descripcionRolAgregar);
                    if ($scope.personaAgregar.nombrePersona === "" || $scope.personaAgregar.nombrePersona === undefined || $scope.personaAgregar.nombrePersona === null ||
                            $scope.personaAgregar.apellidoPersona === "" || $scope.personaAgregar.apellidoPersona === undefined || $scope.personaAgregar.apellidoPersona === null ||
                            $scope.personaAgregar.emailPersona === "" || $scope.personaAgregar.emailPersona === undefined || $scope.personaAgregar.emailPersona === null) {
                        swal("Mensaje del Sistema!", "Los campos Nombre, Apellido y Email no debe quedar vacío", "error");
                    } else {
                        PersonaService.crear($scope.personaAgregar).success(function (data) {
                            if (data === 1) {
                                swal("Mensaje del Sistema!", "Datos Agregados exitosamente", "success");
                                $('#modalAgregar').modal('hide');
                                $scope.personaAgregar = {};
                                $scope.listar();
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Agregados", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };
                $scope.agregar = function () {
                    $scope.personaAgregar.nombrePersona = "";
                    $scope.personaAgregar.apellidoPersona = "";
                    $scope.personaAgregar.ciPersona = "";
                    $scope.personaAgregar.telefonoPersona = "";
                    $scope.personaAgregar.emailPersona = "";
                    $scope.personaAgregar.fechaNac = new Date();
                };
                $scope.atras = function () {
                    location.reload();
                };
                $scope.actualizar = function () {
//                    alert($scope.rolAgregar.descripcionRolAgregar);
                    if ($scope.personaModificar.nombrePersona === "" || $scope.personaModificar.nombrePersona === undefined || $scope.personaModificar.nombrePersona === null ||
                            $scope.personaModificar.apellidoPersona === "" || $scope.personaModificar.apellidoPersona === undefined || $scope.personaModificar.apellidoPersona === null ||
                            $scope.personaModificar.emailPersona === "" || $scope.personaModificar.emailPersona === undefined || $scope.personaModificar.emailPersona === null) {
                        swal("Mensaje del Sistema!", "Los campos Nombre, Apellido y Email no debe quedar vacío", "error");
                    } else {
                        PersonaService.actualizar($scope.personaModificar).success(function (data) {
                            if (data === true) {
                                swal("Mensaje del Sistema!", "Datos Modificados exitosamente", "success");
                                $('#modalModificar').modal('hide');
                                $scope.personaModificar = {};
                                $scope.listar();
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Modificados", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };
                $scope.modificar = function (personas) {
                    $scope.personaModificar.idPersona = personas.idPersona;
                    $scope.personaModificar.nombrePersona = personas.nombrePersona;
                    $scope.personaModificar.apellidoPersona = personas.apellidoPersona;
                    $scope.personaModificar.emailPersona = personas.emailPersona;
                    $scope.personaModificar.ciPersona = personas.ciPersona;
                    $scope.personaModificar.telefonoPersona = personas.telefonoPersona;
                    var dat = new Date(personas.fechaNac);
                    dat.setDate(dat.getDate() + 1);
                    $scope.personaModificar.fechaNac = new Date(dat);// | date: "yyyy-MM-dd";//$filter('date')(personas.fechaNac, "yyyy-MM-dd");

                    $("#modalModificar").modal("show");
                };

//                $scope.eliminar = function (roles) {
////                    var txt;
//                    var r = confirm("Seguro que quiere eliminar el rol " + roles.nombrePersona + " " + roles.apellidoPersona + "?");
//                    if (r === true) {
//                        PersonaService.eliminar(roles.idPersona).success(function (data) {
//                            if (data === true) {
//                                swal('Eliminado!',
//                                        'Haz eliminado exitosamente.',
//                                        'success'
//                                        );
//                                $scope.listar();
//                            }
//                        });
//                    } else {
//                        swal('Cancelado',
//                                'Haz cancelado la eliminación',
//                                'error'
//                                );
//                    }
//                };

                $scope.eliminar = function (roles) {
                    swal({
                        title: "Mensaje del Sistema",
                        text: "Seguro que quiere eliminar la Persona " + roles.nombrePersona + " " + roles.apellidoPersona + "?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    PersonaService.eliminar(roles.idPersona).success(function (data) {
                                        if (data === true) {
                                            swal('Eliminado!',
                                                    'Haz eliminado exitosamente.',
                                                    'success'
                                                    );
                                            $scope.listar();
                                        } else {
                                            swal("Mensaje del Sistema!", "No se pudo a la Persona, verifique existencia en Usuarios", "error");
                                        }
                                    }).error(function (e) {
                                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                    });
                                } else {
                                    swal("Mensaje del Sistema", "Haz cancelado la operación!", "error");
                                }
                            });
                };
                $scope.$watch('filterText', function () {
                    $("#paginaActual").val("1");
                })
                $scope.$watch('apellido', function () {
                    $("#paginaActual").val("1");
                })
                $scope.$watch('ci', function () {
                    $("#paginaActual").val("1");
                })
                $scope.buscar();
            }]);