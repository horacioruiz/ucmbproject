'use strict'
app
        .controller('movimientoCtrl', ['$state', '$scope', 'config', 'MovimientoService', '$cookies', '$base64', 'AndeCategoriaService', 'PersonaService', 'CiudadService', '$http', '$rootScope', function ($state, $scope, config, MovimientoService, $cookies, $base64, AndeCategoriaService, PersonaService, CiudadService, $http, $rootScope) {
                $scope.movimiento = {};
                $scope.movimientoAgregar = {};
                $scope.movimientoModificar = {};

                $scope.movimientoAgregar.andeCategoria = {};
                $scope.movimientoAgregar.tipoEdificio = {};
                $scope.movimientoAgregar.persona = {};
                $scope.movimientoAgregar.ciudad = {};

                $scope.movimientoModificar.andeCategoria = {};
                $scope.movimientoModificar.tipoEdificio = {};
                $scope.movimientoModificar.persona = {};
                $scope.movimientoModificar.ciudad = {};

                var valorMin = 9999;
                var valorMax = 0;
                var myMapEdificio = new Map();
                var myMapNodo = new Map();
                $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                $scope.userLog = $scope.jsonUsu.usuarioUsuario;

                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };

                $scope.aModificar = "";

                $scope.limpiar = function () {
                    $scope.filterText = "";
                    document.getEleme

                    $scope.aModificar = "";
                    ntById("fecha").valueAsDate = null;
                    $scope.tipo = "";
                    $scope.ciudad = "";
                };

//               pagination

                $scope.paginar = function () {
                    MovimientoService.count().success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $rootScope.$on("$locationChangeStart", function (event, data) {
                    var pag = parseInt(data.replace("http://" + config.ipFront + ":8017/menu.html#/:pagina=", ""));
                    if (!isNaN(pag)) {
                        $("#paginaActual").val(pag);
                        $scope.buscarSegunPagina();
                    }
                });

                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.fecha === "" || $scope.fecha === undefined) {
                        $scope.fec = null;
                    } else {
                        var date = new Date($('#fecha').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();
                        var fechas = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');
                        $scope.fec = fechas;
                        if ($scope.fec === "NaN-aN-aN") {
                            $scope.fec = null;
                        }
                    }
                    MovimientoService.countFiltro($scope.filt, $scope.fec).success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
//                    $("#paginacionesEdificios").html('<ul class="pagination"><li class="disabled"><a href="#">&laquo;</a></li><li class="active"><a href="#">1</a></li><li ><a href="#">2</a></li><li><a href="#">&raquo;</a></li></ul>');
                };

                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.fecha === "" || $scope.fecha === undefined) {
                        $scope.fec = null;
                    } else {
                        var date = new Date($('#fecha').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();
                        var fechas = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');
                        $scope.fec = fechas;
                        if ($scope.fec === "NaN-aN-aN") {
                            $scope.fec = null;
                        }
                    }
                    $http.get(config.backend + '/movimiento/generarReporte/' + $scope.filt + '/' + $scope.fec + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                };

                $scope.listarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.fecha === "" || $scope.fecha === undefined) {
                        $scope.fec = null;
                    } else {
                        var date = new Date($('#fecha').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();
                        var fechas = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');
                        $scope.fec = fechas;
                        if ($scope.fec === "NaN-aN-aN") {
                            $scope.fec = null;
                        }
                    }
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    MovimientoService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt, $scope.fec).success(function (data) {
                        $scope.movimiento = data;
                        if ($scope.movimiento.length === 0) {
                            if ($scope.fec === null) {
                                $("#detalleMov").html("<tr><td colspan='7'><center> NINGUN SENSOR DE MOVIMIENTO DETECTADO EL DIA DE HOY... </center></td></tr>");
                            } else {
                                $("#detalleMov").html("<tr><td colspan='7'><center> NINGUN SENSOR DE MOVIMIENTO DETECTADO... </center></td></tr>");
                            }
                        } else {
                            $("#detalleMov").html("");
                            $("#detalleMovim").fadeIn("fast");
                        }
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.buscar = function () {
                    $scope.movimiento = [];
                    location.href = "http://" + config.ipFront + ":8017/menu.html#/:pagina=1";
                    if ($scope.filterText !== "" || $scope.fecha !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
//                    }
                };

                $scope.buscarSegunPagina = function () {
                    $scope.movimiento = [];
                    if ($scope.filterText !== "" || $scope.fecha !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };

//                FIN DE PAGINACION

                var myMap = new Map();
                var myMapPersona = new Map();
                var myMapTipo = new Map();
                var myMapCiudad = new Map();

                $scope.listar = function () {
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    MovimientoService.fetch(config.cantPaginacion, offSet).success(function (data) {
                        $scope.movimiento = data;
                        $scope.paginar();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.atras = function () {
                    location.reload();
                };

                //SEPARADOR DE MILES

                $('input.number').keyup(function (event) {
                    // skip for arrow keys
                    if (event.which >= 37 && event.which <= 40) {
                        event.preventDefault();
                    }

                    $(this).val(function (index, value) {
                        return value
                                .replace(/\D/g, "")
                                //.replace(/([0-9])([0-9]{2})$/, '$1.$2')  
                                .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
                    });
                });

                //FIN DE SEPARADOR DE MILES

                $scope.$watch('filterText', function () {
                    $("#paginaActual").val("1");
                })

                $scope.buscar();
            }]);