angular.module('myapp', ['ui.bootstrap'])
  .controller('MainCtrl', function($scope, $filter) {
    $scope.people = [{
      "name": "name 1",
      "age": "16",
      "state": "virginia"
    }, {
      "name": "name 2",
      "age": "21",
      "state": "new york"
    }, {
      "name": "name 3",
      "age": "21",
      "state": "virginia"
    }, {
      "name": "name 4",
      "age": "18",
      "state": "california"
    }, {
      "name": "name 5",
      "age": "23",
      "state": "california"
    }, {
      "name": "name 6",
      "age": "20",
      "state": "virginia"
    }, {
      "name": "name 7",
      "age": "19",
      "state": "florida"
    }, {
      "name": "name 8",
      "age": "24",
      "state": "louisiana"
    }, {
      "name": "name 9",
      "age": "21",
      "state": "north carolina"
    }, {
      "name": "name 10",
      "age": "25",
      "state": "south carolina"
    }, {
      "name": "name 11",
      "age": "17",
      "state": "maryland"
    }, {
      "name": "name 12",
      "age": "22",
      "state": "virginia"
    }];
    
    $scope.nameFilter = '';
    $scope.ageFilter = '';
    $scope.stateFilter = '';

    $scope.itemsPerPage = 4;



    $scope.currentPage = 1;
    console.debug($scope.totalItems);
    console.debug(Math.ceil($scope.people.length / $scope.itemsPerPage));
    $scope.noOfPages = Math.ceil($scope.people.length / $scope.itemsPerPage);
    console.debug($scope.noOfPages)


  });