'use strict'
app
        .factory('TipoEdificioDepeService', function ($http, config) {
            var url = config.backend + "/tipoEdificioDependencia";
            return {
                listar: function () {
                    return $http.get(url);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                listarPorIdTipoEdificio: function (id) {
                    return $http.get(url + "/listarPorIdTipoEdificio/" + id);
                },
                listarPorIdDependencia: function (id) {
                    return $http.get(url + "/listarPorIdDependencia/" + id);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                },
                modEliminar: function (data) {
                    return $http.put(url + "/eliminar", data);
                }
            };
        });