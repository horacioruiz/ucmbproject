'use strict'
app
        .factory('ComponenteService', function ($http, config) {
            var url = config.backend + "/componente";
            return {
                listar: function () {
                    return $http.get(url);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                },
                modEliminar: function (data) {
                    return $http.put(url + "/eliminar", data);
                },
                fetch: function (limit, offset) {
                    return $http.get(url + "/fetch/" + limit + "/" + offset);
                },
                fetchFiltro: function (limit, offset, descri, marca, tipo) {
                    return $http.get(url + "/fetchFiltro/" + limit + "/" + offset + "/" + descri + "/" + marca + "/" + tipo);
                },
                count: function () {
                    return $http.get(url + "/count/");
                },
                countFiltro: function (descri, marca, tipo) {
                    return $http.get(url + "/countFiltro/" + descri + "/" + marca + "/" + tipo);
                }
            };
        });