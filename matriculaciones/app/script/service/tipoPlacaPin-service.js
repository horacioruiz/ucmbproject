'use strict'
app
        .factory('TipoPlacaPinService', function ($http, config) {
            var url = config.backend + "/tipoPlacaPin";
            return {
                listar: function () {
                    return $http.get(url);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                listarPorPlaca: function (id) {
                    return $http.get(url + "/placa/" + id);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                },
                modEliminar: function (data) {
                    return $http.put(url + "/eliminar", data);
                },
                fetch: function (limit, offset) {
                    return $http.get(url + "/fetch/" + limit + "/" + offset);
                },
                fetchFiltro: function (limit, offset, descri) {
                    return $http.get(url + "/fetchFiltro/" + limit + "/" + offset + "/" + descri);
                },
                count: function () {
                    return $http.get(url + "/count/");
                },
                countFiltro: function (descri) {
                    return $http.get(url + "/countFiltro/" + descri);
                }
            };
        });