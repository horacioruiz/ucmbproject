'use strict'
app
        .factory('NodoPinService', function ($http, config) {
            var url = config.backend + "/nodoPin";
            return {
                listar: function () {
                    return $http.get(url);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                getPines: function (idNodo) {
                    return $http.get(url + "/getPines/" + idNodo);
                },
                fetch: function (limit, offset) {
                    return $http.get(url + "/fetch/" + limit + "/" + offset);
                },
                fetchFiltro: function (limit, offset, nodo, depe, comp, edi) {
                    return $http.get(url + "/fetchFiltro/" + limit + "/" + offset + "/" + nodo + "/" + depe + "/" + comp + "/" + edi);
                },
                count: function () {
                    return $http.get(url + "/count/");
                },
                countFiltro: function (nodo, depe, comp, edi) {
                    return $http.get(url + "/countFiltro/" + nodo + "/" + depe + "/" + comp + "/" + edi);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                },
                eliminarPorNodo: function (id) {
                    return $http.delete(url + "/eliminar/" + id);
                },
                modEliminar: function (data) {
                    return $http.put(url + "/eliminar", data);
                },
                reportarPorDescripcion: function (nodo, dep, comp, usuario, edi) {
                    return $http.get(url + "/reportarPorDescripcion/" + nodo + "/" + dep + "/" + comp + "/" + usuario + "/" + edi);
                }
            };
        });