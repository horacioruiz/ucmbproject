'use strict'
app
        .factory('UsuarioEdificioService', function ($http, config) {
            var url = config.backend + "/usuarioEdificio";
            return {
                listar: function () {
                    return $http.get(url);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                getByIdUsu: function (id) {
                    return $http.get(url + "/getByIdUsu/" + id);
                },
                recuperarCantEdificio: function (id) {
                    return $http.get(url + "/recuperarCantEdificio/" + id);
                },
                generarReporte: function (usu, nom, ape, usuLogin) {
                    return $http.get(url + "/generarReporte/" + usu + "/" + nom + "/" + ape + "/" + usuLogin);
                },
                deleteByIdUsuario: function (id) {
                    return $http.get(url + "/deleteByIdUsuario/" + id);
                },
                fetch: function (limit, offset) {
                    return $http.get(url + "/fetch/" + limit + "/" + offset);
                },
                fetchFiltro: function (limit, offset, usu, nom, ape) {
                    return $http.get(url + "/fetchFiltro/" + limit + "/" + offset + "/" + usu + "/" + nom + "/" + ape);
                },
                count: function () {
                    return $http.get(url + "/count/");
                },
                countFiltro: function (usu, nom, ape) {
                    return $http.get(url + "/countFiltro/" + usu + "/" + nom + "/" + ape);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                }
            };
        });