'use strict'
app
        .factory('PersonaService', function ($http, config) {
            var url = config.backend + "/personas";
            return {
                listar: function () {
                    return $http.get(url);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                generarReporte: function (ci, nom, ape, usuLogin) {
                    return $http.get(url + "/generarReporte/" + ci + "/" + nom + "/" + ape + "/" + usuLogin);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                crearReturnObj: function (data) {
                    return $http.post(url + "/returnObj", data);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                },
                fetch: function (limit, offset) {
                    return $http.get(url + "/fetch/" + limit + "/" + offset);
                },
                fetchFiltro: function (limit, offset, nom, ape, ci) {
                    return $http.get(url + "/fetchFiltro/" + limit + "/" + offset + "/" + nom + "/" + ape + "/" + ci);
                },
                count: function () {
                    return $http.get(url + "/count/");
                },
                countFiltro: function (nom, ape, ci) {
                    return $http.get(url + "/countFiltro/" + nom + "/" + ape + "/" + ci);
                }
            };
        });