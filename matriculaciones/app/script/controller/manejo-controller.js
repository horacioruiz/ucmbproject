'use strict'
app
        .controller('listManejoCtrl', ['$state', '$scope', 'config', 'AdminService', '$cookies', '$base64', '$filter', '$location', '$rootScope', '$http', 'NodoService', '$timeout', function ($state, $scope, config, AdminService, $cookies, $base64, $filter, $location, $rootScope, $http, NodoService, $timeout) {

                $scope.estado = false;
                $scope.dato = "";
                $scope.cambiarEstado = function () {
//                    NodoService.cambEstadoActuador(8).success(function (data) {
//                        $scope.estado = data;
//                    });
                    AdminService.cambEstadoActuador("192.168.0.15", "IOREF").success(function (data) {
                        if (data === true) {
                            console.log("DATOS INSERTADOS...");
                        } else {
                            console.log("DATOS NO INSERTADOS...");
                        }
                    }).error(function (e) {
                        console.log("NO SE PUDO CONECTAR CON EL SERVIDOR.... " + e);
                    });
                };
                $scope.getByIpPin = function () {
                    AdminService.listarPorIpPin("192.168.0.15", "IOREF").success(function (data) {
                        $scope.dato = data.descriNodo + " - " + data.componente.descriComponente;
                    });
                };
                $scope.atras = function () {
                    $state.reload();
                };
                $scope.intervalFunction = function () {
                    $timeout(function () {
                        $scope.getEstadoActual();
                        $scope.getByIpPin();
                        $scope.intervalFunction();
                    }, 1000)
                };
                $scope.intervalFunction();
                $scope.getEstadoActual = function () {
                    AdminService.getEstadoByIpPin("192.168.0.15", "IOREF").success(function (data) {
                        $scope.estado = data;
                    });
                };
                $scope.getEstadoActual();
                $scope.getByIpPin();
            }]);
