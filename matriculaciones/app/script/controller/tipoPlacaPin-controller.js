'use strict'
app
        .controller('listTipoPlacaPinCtrl', ['$state', '$scope', 'config', 'TipoPlacaPinService', '$cookies', '$base64', 'TipoPlacaService', '$http', '$rootScope', function ($state, $scope, config, TipoPlacaPinService, $cookies, $base64, TipoPlacaService, $http, $rootScope) {
//                console.log("HOLAAA");

                $scope.placaPin = {};
                $scope.placaPinAgregar = {};
//                $scope.placaModificar = {};
                $scope.idTipoPlaca = 0;
                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };

                $scope.placaPinAgregar.tipoPlaca = {};
                var myMap = new Map();

                $scope.limpiar = function () {
                    $scope.filterText = "";
                };

                $scope.listar = function () {
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    TipoPlacaPinService.fetch(config.cantPaginacion, offSet).success(function (data) {
                        $scope.placaPin = data;
                        $scope.paginar();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                //INICIO PAGINATION

                $scope.paginar = function () {
                    TipoPlacaPinService.count().success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $rootScope.$on("$locationChangeStart", function (event, data) {
                    var pag = parseInt(data.replace("http://" + config.ipFront + ":8017/menu.html#/:pagina=", ""));
                    if (!isNaN(pag)) {
                        $("#paginaActual").val(pag);
                        $scope.buscarSegunPagina();
                    }
                });

                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    TipoPlacaPinService.countFiltro($scope.filt).success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    $http.get(config.backend + '/tipoPlacaPin/generarReporte/' + $scope.filt + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                };

                $scope.listarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }

                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    TipoPlacaPinService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt).success(function (data) {
                        $scope.placaPin = data;
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.buscar = function () {
                    $scope.placaPin = [];
                    location.href = "http://" + config.ipFront + ":8017/menu.html#/:pagina=1";
                    if ($scope.filterText !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };

                $scope.buscarSegunPagina = function () {
                    $scope.placaPin = [];
                    if ($scope.filterText !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };

//                FIN DE PAGINACION

                $scope.registrar = function () {
                    $scope.placaPinAgregar.tipoPlaca = {};
//                    alert($scope.rolAgregar.descripcionRolAgregar);
                    if ($scope.placaPinAgregar.pin === "" || $scope.placaPinAgregar.pin === undefined || $scope.placaPinAgregar.pin === null) {
                        swal("Mensaje del Sistema!", "Los datos no deben quedar vacío", "error");
                    } else {
                        $scope.placaPinAgregar.tipoPlaca.idTipoPlaca = myMap.get($scope.idTipoPlaca);

                        TipoPlacaPinService.crear($scope.placaPinAgregar).success(function (data) {
                            if (data === true) {
                                swal("Mensaje del Sistema!", "Datos Agregados exitosamente", "success");
                                $('#modalAgregar').modal('hide');
//                            $scope.placaPinAgregar = {};
                                $scope.listar();
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Agregados, Verificar que no exista el PIN ingresado.", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };

                $scope.atras = function () {
                    location.reload();
                };


                $scope.cargarCombo = function () {
                    //PERSONA FILTRO
                    $scope.nestedItemsLevel1 = [];

                    TipoPlacaService.listar().success(function (d) {
                        angular.forEach(d, function (value, key) {
                            $scope.nestedItemsLevel1.push(value.descriTipo);
                            myMap.set(value.descriTipo, value.idTipoPlaca);
                        });
                    });
                };

                $scope.agregar = function () {
                    $scope.placaPinAgregar = {};
                    $scope.idTipoPlaca = "";
                    $scope.cargarCombo();
                };

                $scope.eliminar = function (marc) {
                    $scope.datoEliminar = {};
                    swal({
                        title: "Mensaje del Sistema",
                        text: "Seguro que quiere eliminar " + marc.tipoPlaca.descriTipo + "?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    TipoPlacaPinService.eliminar(marc.idTipoPlacaPin).success(function (data) {
                                        if (data === true) {
                                            swal("Mensaje del Sistema!", "Datos Eliminados exitosamente", "success");
//                                $('#modalModificar').modal('hide');
//                                $scope.placaModificar = {};
                                            $scope.listar();
                                        } else {
                                            swal("Mensaje del Sistema!", "No se pudo eliminar el Pin, verifique Dependencias", "error");
                                        }
                                    }).error(function (e) {
                                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                    });
                                } else {
                                    swal("Mensaje del Sistema", "Haz cancelado la operación!", "error");
                                }
                            });
                };

                $scope.$watch('filterText', function () {
                    $("#paginaActual").val("1");
                })
                $scope.buscar();
            }]);