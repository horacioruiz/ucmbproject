'use strict'
app
        .controller('listTarifaCtrl', ['$state', '$scope', 'config', 'TarifaService', '$cookies', '$base64', 'TipoEdificioService', '$http', '$rootScope', function ($state, $scope, config, TarifaService, $cookies, $base64, TipoEdificioService, $http, $rootScope) {

                $scope.tarifas = {};
                $scope.tarifaAgregar = {};
                $scope.tarifaModificar = {};
                $scope.tarifaAgregar.tipoEdificio = {};
                $scope.aModificar = "";
                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };

                var myMap = new Map();

                $scope.listar = function () {
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    TarifaService.fetch(config.cantPaginacion, offSet).success(function (data) {
                        $scope.tarifas = data;
                        $scope.paginar();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.limpiar2 = function () {
                    $scope.filterText = "";
                };

                //PAGINACION

                $scope.paginar = function () {
                    TarifaService.count().success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $rootScope.$on("$locationChangeStart", function (event, data) {
                    var pag = parseInt(data.replace("http://" + config.ipFront + ":8017/menu.html#/:pagina=", ""));
                    if (!isNaN(pag)) {
                        $("#paginaActual").val(pag);
                        $scope.buscarSegunPagina();
                    }
                });

                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }

                    TarifaService.countFiltro($scope.filt).success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    $http.get(config.backend + '/tarifas/generarReporte/' + $scope.filt + '/' + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                };

                $scope.listarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    TarifaService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt).success(function (data) {
                        $scope.tarifas = data;
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.buscar = function () {
                    $scope.tarifas = [];
                    location.href = "http://" + config.ipFront + ":8017/menu.html#/:pagina=1";
                    if ($scope.filterText !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };


                $scope.buscarSegunPagina = function () {
                    $scope.tarifas = [];
//                    location.href = "http://" + config.ipFront + ":8017/menu.html#/:pagina=1";
                    if ($scope.filterText !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };

                //FIN PAGINACION


                $scope.reporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }

                    $scope.result = {};
                    $http.get(config.backend + '/tarifas/reportarPorDescripcion/' + $scope.filt + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                };

                $scope.registrar = function () {
                    $scope.tarifaAgregar.tipoEdificio.idTipoEdi = myMap.get($scope.idAndeCat);

                    $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                    $scope.tarifaAgregar.tarifaUsuAlta = $scope.jsonUsu.usuarioUsuario;
                    $scope.tarifaAgregar.tarifaUsuMod = $scope.jsonUsu.usuarioUsuario;

                    if ($scope.tarifaAgregar.tarifaGsKwh === "" || $scope.tarifaAgregar.tarifaGsKwh === undefined || $scope.tarifaAgregar.tarifaGsKwh === null) {
                        swal("Mensaje del Sistema!", "El campo tarifa no debe quedar vacío", "error");
                    } else {
                        var res = $scope.tarifaAgregar.tarifaGsKwh.split(".");
                        var respuesta = "";
                        for (var i = 0; i < res.length; i++) {
                            respuesta += res[i];
                        }
                        $scope.tarifaAgregar.tarifaGsKwh = respuesta;
                        TarifaService.crear($scope.tarifaAgregar).success(function (data) {
                            if (data === true) {
                                swal("Mensaje del Sistema!", "Datos Agregados exitosamente", "success");
                                $('#modalAgregar').modal('hide');
//                            $scope.tarifaAgregar = {};
                                $scope.listar();
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Agregados, verifique que no quede campos vacíos ", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };
                $scope.atras = function () {
                    location.reload();
                };

                //SEPARADOR DE MILES

                $('input.number').keyup(function (event) {

                    // skip for arrow keys
                    if (event.which >= 37 && event.which <= 40) {
                        event.preventDefault();
                    }

                    $(this).val(function (index, value) {
                        return value
                                .replace(/\D/g, "")
                                //.replace(/([0-9])([0-9]{2})$/, '$1.$2')  
                                .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
                    });
                });

                //FIN DE SEPARADOR DE MILES


                $scope.actualizar = function () {

                    $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                    $scope.tarifaModificar.tarifaUsuMod = $scope.jsonUsu.usuarioUsuario;

                    if ($scope.tarifaModificar.tarifaGsKwh === "" || $scope.tarifaModificar.tarifaGsKwh === undefined || $scope.tarifaModificar.tarifaGsKwh === null) {
                        swal("Mensaje del Sistema!", "El campo tarifa no debe quedar vacío", "error");
                    } else {
                        var res = $scope.tarifaModificar.tarifaGsKwh.split(".");
                        var respuesta = "";
                        for (var i = 0; i < res.length; i++) {
                            respuesta += res[i];
                        }
                        $scope.tarifaModificar.tarifaGsKwh = respuesta;
                        TarifaService.actualizar($scope.tarifaModificar).success(function (data) {
                            if (data === true) {
                                swal("Mensaje del Sistema!", "Datos Modificados exitosamente", "success");
                                $('#modalModificar').modal('hide');
                                $scope.tarifaModificar = {};
                                $scope.listar();
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Modificados", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };

                $scope.limpiar = function () {
                    $scope.tarifaAgregar.trafiaRangoMin = "";
                    $scope.tarifaAgregar.tarifaRangoMax = "";
                    $scope.tarifaAgregar.tarifaGsKwh = "";

                    $scope.tarifaModificar.trafiaRangoMin = "";
                    $scope.tarifaModificar.tarifaRangoMax = "";
                    $scope.tarifaModificar.tarifaGsKwh = "";

                    $scope.nestedItemsLevel1 = [];
                };

                $scope.agregar = function () {
                    //PERSONA FILTRO
                    $scope.limpiar();
                    $scope.idAndeCat = "";
                    $scope.nestedItemsLevel1 = [];
                    TipoEdificioService.listarDistintosTarifa().success(function (d) {
                        angular.forEach(d, function (value, key) {
                            $scope.nestedItemsLevel1.push(value.descriTipoEdi);
                            myMap.set(value.descriTipoEdi, value.idTipoEdi);
                        });
                    });
                };
                $scope.modificar = function (tarifas) {
                    $scope.tarifaModificar.trafiaRangoMin = tarifas.trafiaRangoMin;
                    $scope.tarifaModificar.tarifaRangoMax = tarifas.tarifaRangoMax;
                    $scope.tarifaModificar.tarifaGsKwh = tarifas.tarifaGsKwh;
//                            .replace(/\D/g, "")
//                            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");

                    $scope.tarifaModificar.idTarifaAnde = tarifas.idTarifaAnde;
                    $("#modalModificar").modal("show");
                };

                $scope.eliminar = function (tarifas) {

                    swal({
                        title: "Mensaje del Sistema",
                        text: "Seguro que quiere eliminar esta tarifa?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    TarifaService.eliminar(tarifas.idTarifaAnde).success(function (data) {
                                        if (data === true) {
                                            swal('Eliminado!',
                                                    'Haz eliminado exitosamente.',
                                                    'success'
                                                    );
                                            $scope.listar();
                                        }
                                    }).error(function (e) {
                                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                    });
                                } else {
                                    swal("Mensaje del Sistema", "Haz cancelado la operación!", "error");
                                }
                            });
                };

                $scope.$watch('filterText', function () {
                    $("#paginaActual").val("1");
                })
                $scope.buscar();
            }]);