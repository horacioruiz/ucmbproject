'use strict'
app
        .controller('humoCtrl', ['$state', '$scope', 'config', 'HumoService', 'NodoService', '$cookies', '$base64', 'AndeCategoriaService', 'PersonaService', 'TipoEdificioService', 'CiudadService', 'UsuarioEdificioService', '$http', '$rootScope', function ($state, $scope, config, HumoService, NodoService, $cookies, $base64, AndeCategoriaService, PersonaService, TipoEdificioService, CiudadService, UsuarioEdificioService, $http, $rootScope) {
                $scope.humo = {};
                $scope.humoAgregar = {};
                $scope.humoModificar = {};

                $scope.humoAgregar.andeCategoria = {};
                $scope.humoAgregar.tipoEdificio = {};
                $scope.humoAgregar.persona = {};
                $scope.humoAgregar.ciudad = {};

                $scope.humoModificar.andeCategoria = {};
                $scope.humoModificar.tipoEdificio = {};
                $scope.humoModificar.persona = {};
                $scope.humoModificar.ciudad = {};

                var valorMin = 9999;
                var valorMax = 0;
                var myMapEdificio = new Map();
                var myMapNodo = new Map();
                $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                $scope.userLog = $scope.jsonUsu.usuarioUsuario;


                $scope.generar = function () {
                    $("#menuInicial").css("display", "none");
                    $("#addChart").css("display", "inline");
                    $("#chart01").css("display", "none");
                    $("#edificioPlaca").css("display", "none");
                    $("#DependenciaComponente").css("display", "none");
                    $scope.nestedItemsLevelEdificio = [];
                    $scope.idEdificio = 0;
                    $scope.fechaDesde = "";
                    $scope.fechaHasta = "";
                    UsuarioEdificioService.getByIdUsu($scope.jsonUsu.idUsuario).success(function (data) {
                        angular.forEach(data, function (value, key) {
                            $scope.nestedItemsLevelEdificio.push(value.edificio.descriEdificio);
                            myMapEdificio.set(value.edificio.descriEdificio, value.edificio.idEdificio);
                        });
                    });
                };

                $scope.onChangeEdificio = function () {
                    $scope.nestedItemsLevel1 = [];
                    $scope.idNodo = 0;
                    $("#edificioPlaca").css("display", "none");
                    $("#DependenciaComponente").css("display", "none");
                    NodoService.listarSensoresDescri(myMapEdificio.get($scope.idEdificio), "HUMO", "null", "null").success(function (data) {
                        angular.forEach(data, function (value, key) {
                            $scope.nestedItemsLevel1.push(value.descriNodo);
                            myMapNodo.set(value.descriNodo, value.idNodo);
                        });
                    });
                };

                $scope.onChange = function () {
                    $("#edificioPlaca").css("display", "none");
                    $scope.nodoPinAgregar = {};
                    $("#DependenciaComponente").css("display", "none");
                    NodoService.listarPorId(myMapNodo.get($scope.idNodo)).success(function (data) {
                        $scope.nodoPinAgregar.ipPlaca = data.placa.ipPlaca;
                        $scope.nodoPinAgregar.descriPlaca = data.placa.descriPlaca;
                        $scope.nodoPinAgregar.descriTipoPlaca = data.placa.tipoPlaca.descriTipo;
                        $scope.nodoPinAgregar.descriDependencia = data.dependencia.descriDependencia;
                        $scope.nodoPinAgregar.descriComponente = data.componente.descriComponente;
                        $("#edificioPlaca").css("display", "inline");
                        $("#DependenciaComponente").css("display", "inline");
                    });
                };

                $scope.generarGrafico = function () {
                    $scope.dataX = [];
                    $scope.dataY = [];
                    if (!isNaN(new Date($scope.fechaDesde).getTime())) {
                        valorMin = 9999;
                        valorMax = 0;
                        $("#chart01").css("display", "none");
//                        $("#edificioPlaca").css("display", "none");
//                        $("#DependenciaComponente").css("display", "none");
                        var date = new Date($('#fechaDesde').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();

                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');

                        $scope.fec = fecha;
                        if ($scope.fec === "NaN-aN-aN") {
                            $scope.fec = null;
                        }
                        HumoService.generarGrafico(myMapNodo.get($scope.idNodo), $scope.fec).success(function (data) {
                            if (data.length === 0) {
                                swal("Mensaje del Sistema!", "No se encontraron resultados de la búsqueda.", "error");
                            } else {
                                var tamanho = data.length;
                                var count = 0;
                                angular.forEach(data, function (value, key) {
                                    if (valorMin > value.humo) {
                                        valorMin = value.humo;
                                    }
                                    if (valorMax < value.humo) {
                                        valorMax = value.humo;
                                    }
                                    if (valorMin === valorMax) {
                                        valorMin = 0;
                                    }
//                                $scope.dataY.push(parseInt(value.corrienteConsu));
                                    $scope.sample = value.horaInicioHumo;
                                    $scope.dataX.push($scope.sample);
//                                final
                                    $scope.dataX.push(parseFloat(value.humo));
                                    $scope.dataY.push($scope.dataX);
                                    $scope.dataX = [];
//                                final
                                    count++;
                                    if (count === tamanho) {
                                        $scope.generarlo();
                                    }
                                });
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo conectar con el Servidor..", "error");
                        });
                    } else if ($scope.idNodo === 0) {
                        swal("Mensaje del Sistema!", "El campo Nodo no debe quedar vacío", "error");
                    } else {
                        swal("Mensaje del Sistema!", "El campo Fecha no debe quedar vacío", "error");
                    }
                };

                $scope.generarlo = function () {
                    console.log("MIN: " + valorMin + " MAX: " + valorMax + "->> " + "'" + valorMin + ":" + valorMax + ":" + (valorMax / 10) + "'", );
                    if ($scope.idEdificio === 0 || $scope.idNodo === 0) {
                        swal("Mensaje del Sistema!", "El campo Edificio y Nodo no deben quedar vacío.", "error");
                    } else if (!isNaN(new Date($scope.fechaDesde).getTime())) {
                        $scope.myJson = {
                            type: 'line',
                            title: {
                                text: 'Gráfico generado',
                                align: 'left',
                                fontColor: '#FFFFFF'
                            },
                            backgroundColor: "#30a5ff",
                            plot: {
                                lineWidth: "2px",
                                lineColor: "#FFFFFF",
                                aspect: "spline",
                                marker: {
                                    visible: true
                                }
                            },
                            plotarea: {
                            },
                            crosshairX: {
                                lineStyle: 'dashed',
                                lineWidth: 2,
                                lineColor: '#FFFFFF',
                                plotLabel: {
                                    visible: false
                                },
                                marker: {
                                    type: 'triangle',
                                    size: 5,
                                    visible: true
                                }
                            },
                            preview: {
                                live: true
                            },
                            scaleY: {
                                values: "'" + valorMin + ":" + Math.ceil(valorMax) + ":" + ((Math.ceil(valorMax)) / 10) + "'",
                                lineColor: "#000000",
                                lineWidth: "1px",
                                tick: {
                                    lineColor: "white",
                                    lineWidth: "1px"
                                },
                                guide: {
                                    lineStyle: "solid",
                                    lineColor: "#249178"
                                },
                                item: {
                                    fontColor: "white"
                                }
                            },
                            scaleX: {
                                zooming: true,
                                zoomTo: [0, 10],
                                lineWidth: "1px",
                                lineColor: '#000000',
                                itemsOverlap: true,
                                tick: {
                                    lineColor: '#FFFFFF',
                                    alpha: 1,
                                    lineWidth: 2
                                },
                                item: {
                                    fontColor: "#FFFFFF"
                                },
                                guide: {
                                    visible: false
                                },
                                values: $scope.dataX
                            },
                            'scale-y-2': {
                            },
                            series: [
                                {
                                    values: $scope.dataY
                                }]
                        };
//                        $scope.myJson = {
//
//                            "type": "line",
//                            title: {
//                                text: 'Gráfico generado',
//                                align: 'left',
//                                fontColor: '#FFFFFF'
//                            },
//                            backgroundColor: "#30a5ff",
//                            plot: {
//                                lineWidth: "2px",
//                                lineColor: "#FFFFFF",
//                                aspect: "spline",
//                                marker: {
//                                    visible: true
//                                }
//                            },
//                            plotarea: {
//                            },
//                            crosshairX: {
//                                lineStyle: 'dashed',
//                                lineWidth: 2,
//                                lineColor: '#FFFFFF',
//                                plotLabel: {
//                                    visible: false
//                                },
//                                marker: {
//                                    type: 'triangle',
//                                    size: 5,
//                                    visible: true
//                                }
//                            },
//                            preview: {
//                                live: true
//                            },
//
////                            "scale-x": {
////                                "min-value": "1457101800000",
////                                "max-value": "1457125200000",
////                                "step": "30minute",
////                                "transform": {
////                                    "type": "date"
////                                },
////                                "label": {
////                                    "text": "Trading Day"
////                                },
////                                "item": {
////                                    "font-size": 10
////                                },
////                                "max-items": 14
////                            },
//                            scaleX: {
//                                zooming: true,
//                                zoomTo: [0, 30],
//                                lineWidth: "1px",
//                                lineColor: '#000000',
//                                itemsOverlap: true,
//                                tick: {
//                                    lineColor: '#FFFFFF',
//                                    alpha: 1,
//                                    lineWidth: 2
//                                },
//                                item: {
//                                    fontColor: "#FFFFFF"
//                                },
//                                guide: {
//                                    visible: false
//                                }
//                            },
////                            "scale-y": {
////                                "values": "30:34:1",
////                                "format": "$%v",
////                                "label": {
////                                    "text": "Price"
////                                },
////                                "item": {
////                                    "font-size": 10
////                                },
////                                "guide": {
////                                    "line-style": "solid"
////                                }
////                            },
//                            scaleY: {
////                                "values": "0:50:10",
//                                values: "'" + valorMin + ":" + valorMax + ":" + (valorMax / 10) + "'",
//                                lineColor: "#000000",
//                                lineWidth: "1px",
//                                tick: {
//                                    lineColor: "white",
//                                    lineWidth: "1px"
//                                },
//                                guide: {
//                                    lineStyle: "solid",
//                                    lineColor: "#249178"
//                                },
//                                item: {
//                                    fontColor: "white"
//                                }
//                            },
//                            "series": [
//                                {
//                                    values: $scope.dataY
////                                    "values": [
////                                        ['03/04/2016', 30.34], //03/04/2016 at 9:30 a.m. EST (which is 14:30 in GMT)
////                                        ['04/04/2016', 31.30], //10:00 a.m.
////                                        ['05/04/2016', 30.95], //10:30 a.m.
////                                        ['06/04/2016', 30.99], //11:00 a.m.
////                                        ['0/04/2016', 32.33], //11:30 a.m.
////                                        ['07/04/2016', 33.34], //12:00 p.m.
////                                        ['08/04/2016', 33.01], //12:30 p.m.
////                                        ['09/04/2016', 34], //1:00 p.m.
////                                        ['10/04/2016', 33.64], //1:30 p.m.
////                                        ['11/04/2016', 32.59], //2:00 p.m.
////                                        ['12/04/2016', 32.60], //2:30 p.m.
////                                        ['01/06/2016', 31.99], //3:00 p.m.
////                                        ['02/06/2016', 31.14], //3:30 p.m.
////                                        ['03/06/2016', 32.34] //at 4:00 p.m. EST (which is 21:00 GMT)
////                                    ]
//                                }
//                            ]
//                        };
                        $("#chart01").fadeIn("slow");
                    } else {
                        swal("Mensaje del Sistema!", "El campo Fecha no debe quedar vacío", "error");
                    }
                };

                $scope.atrasite = function () {
                    $("#addChart").css("display", "none");
                    $("#menuInicial").css("display", "inline");
                };

                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };

                $scope.aModificar = "";

                $scope.limpiar = function () {
                    $scope.filterText = "";
                    document.getElementById("fecha").valueAsDate = null;
                };

//               pagination

                $scope.paginar = function () {
                    HumoService.count().success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $rootScope.$on("$locationChangeStart", function (event, data) {
                    var pag = parseInt(data.replace("http://" + config.ipFront + ":8017/menu.html#/:pagina=", ""));
                    if (!isNaN(pag)) {
                        $("#paginaActual").val(pag);
                        $scope.buscarSegunPagina();
                    }
                });

                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.fecha === "" || $scope.fecha === undefined) {
                        $scope.fec = null;
                    } else {
                        var date = new Date($('#fecha').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();
                        var fechas = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');
                        $scope.fec = fechas;
                        if ($scope.fec === "NaN-aN-aN") {
                            $scope.fec = null;
                        }
                    }
                    HumoService.countFiltro($scope.filt, $scope.fec).success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
//                    $("#paginacionesEdificios").html('<ul class="pagination"><li class="disabled"><a href="#">&laquo;</a></li><li class="active"><a href="#">1</a></li><li ><a href="#">2</a></li><li><a href="#">&raquo;</a></li></ul>');
                };

                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.fecha === "" || $scope.fecha === undefined) {
                        $scope.fec = null;
                    } else {
                        var date = new Date($('#fecha').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();
                        var fechas = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');
                        $scope.fec = fechas;
                        if ($scope.fec === "NaN-aN-aN") {
                            $scope.fec = null;
                        }
                    }
                    $http.get(config.backend + '/humo/generarReporte/' + $scope.filt + '/' + $scope.fec + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                };

                $scope.listarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.fecha === "" || $scope.fecha === undefined) {
                        $scope.fec = null;
                    } else {
                        var date = new Date($('#fecha').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();
                        var fechas = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');
                        $scope.fec = fechas;
                        if ($scope.fec === "NaN-aN-aN") {
                            $scope.fec = null;
                        }
                    }
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    HumoService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt, $scope.fec).success(function (data) {
                        $scope.humo = data;
                        if ($scope.humo.length === 0) {
                            if ($scope.fec === null) {
                                $("#detalleHumo").html("<tr><td colspan='8'><center> NINGUN SENSOR DE HUMO DETECTADO EL DIA DE HOY... </center></td></tr>");
                            } else {
                                $("#detalleHumo").html("<tr><td colspan='8'><center> NINGUN SENSOR DE HUMO DETECTADO... </center></td></tr>");
                            }
                        } else {
                            $("#detalleHumo").html("");
                            $("#detalleHumosidad").fadeIn("fast");
                        }
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.buscar = function () {
                    $scope.humo = [];
                    location.href = "http://" + config.ipFront + ":8017/menu.html#/:pagina=1";
                    if ($scope.filterText !== "" || $scope.fecha !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
//                    }
                };

                $scope.buscarSegunPagina = function () {
                    $scope.humo = [];
                    if ($scope.filterText !== "" || $scope.fecha !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };

//                FIN DE PAGINACION

                var myMap = new Map();
                var myMapPersona = new Map();
                var myMapTipo = new Map();
                var myMapCiudad = new Map();

                $scope.listar = function () {
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    HumoService.fetch(config.cantPaginacion, offSet).success(function (data) {
                        $scope.humo = data;
                        $scope.paginar();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.atras = function () {
                    location.reload();
                };

                //SEPARADOR DE MILES

                $('input.number').keyup(function (event) {
                    // skip for arrow keys
                    if (event.which >= 37 && event.which <= 40) {
                        event.preventDefault();
                    }

                    $(this).val(function (index, value) {
                        return value
                                .replace(/\D/g, "")
                                //.replace(/([0-9])([0-9]{2})$/, '$1.$2')  
                                .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
                    });
                });

                //FIN DE SEPARADOR DE MILES

                $scope.$watch('filterText', function () {
                    $("#paginaActual").val("1");
                })

                $scope.buscar();
            }]);