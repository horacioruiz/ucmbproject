'use strict'
app
        .controller('listRolesCtrl', ['$state', '$scope', 'config', 'RolService', '$cookies', '$base64', '$rootScope', 'ModuloService', 'FuncionService', 'RolFuncionService', '$http', function ($state, $scope, config, RolService, $cookies, $base64, $rootScope, ModuloService, FuncionService, RolFuncionService, $http) {
                $scope.roles = {};
                $scope.rolAgregar = {};
                $scope.aModificar = "";
                $scope.funciones = [];
                $scope.idModulo = "";
                $scope.listSelect = [];
                $scope.rolAgregar.modulo = {};

                //MODIFICAR
                $scope.rolModificar = {};
                $scope.rolModificar.modulo = {};
                $scope.rolModificar.descriRolMod = "";
                //MODIFICAR
                var myMapModulo = new Map();
                var idRol = 0;
//               pagination

                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $("#addRol").hide();
                $("#updateRol").hide();


                $scope.listar = function () {
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    RolService.fetch(config.cantPaginacion, offSet).success(function (data) {
                        $scope.roles = data;
                        $scope.paginar();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.paginar = function () {
                    RolService.count().success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $rootScope.$on("$locationChangeStart", function (event, data) {
                    var pag = parseInt(data.replace("http://" + config.ipFront + ":8017/menu.html#/:pagina=", ""));
                    if (!isNaN(pag)) {
                        $("#paginaActual").val(pag);
                        $scope.buscarSegunPagina();
                    }
                });
                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    RolService.countFiltro($scope.filt).success(function (data) {
                        $scope.catidad = data;

                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.listarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }

                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    RolService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt).success(function (data) {
                        $scope.roles = data;
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.buscar = function () {
                    $scope.roles = [];
//                    location.href = "http://" + config.ipFront + ":8017/menu.html#/:pagina=1";
                    if ($scope.filterText !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
//                    }
                };
                $scope.buscarSegunPagina = function () {
                    $scope.roles = [];
                    if ($scope.filterText !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };

                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }

                    $http.get(config.backend + '/usuariosRoles/generarReporte/' + $scope.filt + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                };

                //REGISTRAR
                $scope.registrar = function () {
                    $scope.descripcion = $scope.rolAgregar.descripcionRol;
                    if ($scope.rolAgregar.descripcionRol === "" || $scope.rolAgregar.descripcionRol === undefined) {
                        swal("Mensaje del Sistema!", "Descripción no debe quedar vacío", "error");
                    } else {
                        RolService.crearAdd($scope.rolAgregar).success(function (data) {
                            if (parseInt(data) !== 0) {
                                idRol = parseInt(data);
                                swal("Mensaje del Sistema!", "Datos Agregados exitosamente", "success");
                                $scope.rolAgregar = {};
                                var element = $('#listSeleccionado li').length; //obtienes el total de elementos dentro del tag ul con clase caja
                                $("#listSeleccionado li").each(function (index) {
                                    var valorSeleccionado = $.trim($(this).text());
                                    var val = "";
                                    for (var i = 0; i < valorSeleccionado.length; i++) {
                                        val += valorSeleccionado.charAt(i).replace(" ", "_");
                                    }
                                    var id = $("#dato" + val).val();
                                    $scope.rolFuncion = {};
                                    $scope.rolFuncion.rol = {};
                                    $scope.rolFuncion.funcion = {};

                                    $scope.rolFuncion.rol.idRol = idRol;
                                    $scope.rolFuncion.funcion.idFuncion = id;
                                    RolFuncionService.crear($scope.rolFuncion).success(function (data) {
                                        console.log("DATOS INSERTADO EN ROL_FUNCION....");
                                    }).error(function (e) {
                                        console.log("ERROR DE CONEXION CON EL SERVIDOR...");
                                    });
                                });
                                $scope.descripcionRol = "";
                                $scope.idModulo = 0;
                                $("#listBox").hide();
                                $("#listSeleccionado li").each(function (index) {
                                    $(this).remove();
                                });
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Agregados", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };

                $scope.agregar = function () {
                    $("#guardando").css("display", "none");
                    $("#saliendo").css("display", "none");
                    $("#menuRol").hide();
                    $("#updateRol").hide();
                    $("#addRol").show();
                    $("#descriRol").prop('disabled', false);
                    $("#listBox").hide();
                    $scope.rolAgregar.descripcionRol = "";
                    $scope.nestedItemsLevelModulo = [];
                    $scope.idModulo = 0;
                    ModuloService.listar().success(function (d) {
                        angular.forEach(d, function (value, key) {
                            $scope.nestedItemsLevelModulo.push(value.descripcionModulo);
                            myMapModulo.set(value.descripcionModulo, value.idModulo);
                        });
                    });
                };

                $scope.onChanged = function () {
                    var myMap = new Map();
                    $scope.funciones = [];
                    $("#checkAll1").removeClass();
                    $("#checkAll2").removeClass();
                    $("#checkAll1").addClass("glyphicon glyphicon-unchecked");
                    $("#checkAll2").addClass("glyphicon glyphicon-unchecked");
                    $("#listSeleccionado li").each(function (index) {
                        var valorSeleccionado = $.trim($(this).text());
                        var val = "";
                        for (var i = 0; i < valorSeleccionado.length; i++) {
                            val += valorSeleccionado.charAt(i).replace(" ", "_");
                        }
                        myMap.set(val, val);
                        $("#listBox").show();
                    });
                    $scope.rolAgregar.idModulo = myMapModulo.get($scope.idModulo);
                    FuncionService.listarPorIdModulo($scope.rolAgregar.idModulo).success(function (d) {
                        angular.forEach(d, function (value, key) {
                            var descriFun = value.descripcionFuncion;
                            var val = "";
                            for (var i = 0; i < descriFun.length; i++) {
                                val += descriFun.charAt(i).replace(" ", "_");
                            }
                            var mapeo = myMap.get(val);
                            var newstr = val;
                            if (mapeo === undefined) {
                                value.descripFunc = newstr;
                                $scope.funciones.push(value);
                                $("#listBox").show();
                            }
                        });
                    }).error(function (e) {
                        swal("Mensaje del Sistema", "Haz cancelado la operación!", "error");
                    });
                };

                $scope.back = function () {
                    var myMap = new Map();
                    $scope.funciones = [];
                    $("#listSeleccionado li").each(function (index) {
                        var valorSeleccionado = $.trim($(this).text());
                        var val = "";
                        for (var i = 0; i < valorSeleccionado.length; i++) {
                            val += valorSeleccionado.charAt(i).replace(" ", "_");
                        }
                        myMap.set(val, val);
                    });
                    $scope.rolAgregar.idModulo = myMapModulo.get($scope.idModulo);
                    FuncionService.listarPorIdModulo($scope.rolAgregar.idModulo).success(function (d) {
                        angular.forEach(d, function (value, key) {
                            var descriFun = value.descripcionFuncion;
                            var val = "";
                            for (var i = 0; i < descriFun.length; i++) {
                                val += descriFun.charAt(i).replace(" ", "_");
                            }
                            var mapeo = myMap.get(val);
                            var newstr = val;
                            if (mapeo === undefined) {
                                value.descripFunc = newstr;
                                $scope.funciones.push(value);
                            }
                        });
                    }).error(function (e) {
                        swal("Mensaje del Sistema", "Haz cancelado la operación!", "error");
                    });
                };

                //FIN REGISTRAR
                $scope.atras = function () {
                    location.reload();
                };
                $scope.atrasite = function () {
//                    $("#listSeleccionado li").each(function (index) {
//                        $(this).remove();
//                    });
                    $("#guardando").css("display", "inline");
                    $("#saliendo").css("display", "inline");
                    $("#descriRol").prop('disabled', true);
//                    $scope.rolAgregar.descripcionRol = $scope.descripcion;
                    $scope.buscar();
                    $("#menuRol").show();
                    $("#addRol").hide();
                    $("#updateRol").hide();

                };

                //MODIFICAR
                $scope.actualizar = function () {
                    if ($scope.rolModificar.descripcionRol === "" || $scope.rolModificar.descripcionRol === undefined) {
                        swal("Mensaje del Sistema!", "Descripción no debe quedar vacío", "error");
                    } else {
                        RolService.actualizar($scope.rolModificar).success(function (data) {
                            if (data === true) {
                                swal("Mensaje del Sistema!", "Datos Modificados exitosamente", "success");
//                                $('#modalModificar').modal('hide');
                                $scope.rolModificar = {};
                                $scope.atrasite();
//                                $scope.listar();
//                                $("#menuRol").show();
//                                $("#addRol").hide();
//                                $("#updateRol").hide();
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Modificados", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };

                $scope.modificar = function (roles) {
                    $("#guardando").css("display", "none");
                    $("#saliendo").css("display", "none");
                    $scope.rolModificar.descripcionRol = roles.descripcionRol;
                    $scope.rolModificar.descriRolMod = roles.descripcionRol;
                    $scope.rolModificar.idRol = roles.idRol;
                    var dato = false;
//                    alert("-> ID ROL " + $scope.rolModificar.idRol);
                    $("#menuRol").hide();
                    $("#addRol").hide();
                    $("#updateRol").show();
                    $scope.nestedItemsLevelModuloModificar = [];
                    $scope.idModulo = 0;
                    ModuloService.listar().success(function (d) {
                        angular.forEach(d, function (value, key) {
                            $scope.nestedItemsLevelModuloModificar.push(value.descripcionModulo);
                            myMapModulo.set(value.descripcionModulo, value.idModulo);
                        });
                    });
                    RolFuncionService.listarPorIdRol($scope.rolModificar.idRol).success(function (d) {
                        $scope.listaDeSeleccionados = "";
                        angular.forEach(d, function (value, key) {
                            var val = ""
                            for (var i = 0; i < value.funcion.descripcionFuncion.length; i++) {
                                val += value.funcion.descripcionFuncion.charAt(i).replace(" ", "_");
                            }

                            $scope.listaDeSeleccionados += "<li class=\"list-group-item ng-binding \">" + value.funcion.descripcionFuncion +
                                    "<input type=\"hidden\" id=\"datoUpdate" + val + "\" value=\"" + value.funcion.idFuncion
                                    + "\"></li>";
                            dato = true;
                        });
                        $("#listSeleccionadoUpdate").html($scope.listaDeSeleccionados);
                        if (dato === false) {
                            $("#listBoxModificar").hide();
                        } else {
                            $("#listBoxModificar").show();
                        }
                        $("#primeraLista").hide();
//                        $("#listBox").hide(); // ORIGINAL CAMBIADO NOMAS
                    });

                };

                $scope.onChangedUpdate = function () {
                    var myMap = new Map();
                    $scope.funciones = [];
                    $("#primeraLista").show();
                    $("#checkAllUpdate1").removeClass();
                    $("#checkAllUpdate2").removeClass();
                    $("#checkAllUpdate1").addClass("glyphicon glyphicon-unchecked");
                    $("#checkAllUpdate2").addClass("glyphicon glyphicon-unchecked");
                    $("#listSeleccionadoUpdate li").each(function (index) {
                        var valorSeleccionado = $.trim($(this).text());
                        var val = "";
                        for (var i = 0; i < valorSeleccionado.length; i++) {
                            val += valorSeleccionado.charAt(i).replace(" ", "_");
                        }
                        myMap.set(val, val);
                        $("#listBoxModificar").show();
                    });
                    $scope.rolModificar.idModulo = myMapModulo.get($scope.idModulo);
                    FuncionService.listarPorIdModulo($scope.rolModificar.idModulo).success(function (d) {
                        angular.forEach(d, function (value, key) {
                            var descriFun = value.descripcionFuncion;
                            var val = "";
                            for (var i = 0; i < descriFun.length; i++) {
                                val += descriFun.charAt(i).replace(" ", "_");
                            }
                            var mapeo = myMap.get(val);
                            var newstr = val;
                            if (mapeo === undefined) {
                                value.descripFunc = newstr;
                                $scope.funciones.push(value);
                                $("#listBoxModificar").show();
                            }
                        });
                    }).error(function (e) {
                        swal("Mensaje del Sistema", "Haz cancelado la operación!", "error");
                    });
                };

                $scope.backUpdate = function () {
//                    $("#listBoxModificar").fadeOut("slow");
                    var myMap = new Map();
                    $scope.funciones = [];
                    $("#listSeleccionadoUpdate li").each(function (index) {
                        var valorSeleccionado = $.trim($(this).text());
                        var val = "";
                        for (var i = 0; i < valorSeleccionado.length; i++) {
                            val += valorSeleccionado.charAt(i).replace(" ", "_");
                        }
                        myMap.set(val, val);
                    });
                    $scope.rolModificar.idModulo = myMapModulo.get($scope.idModulo);
                    if ($scope.rolModificar.idModulo !== undefined) {
                        FuncionService.listarPorIdModulo($scope.rolModificar.idModulo).success(function (d) {
                            angular.forEach(d, function (value, key) {
                                var descriFun = value.descripcionFuncion;
                                var val = "";
                                for (var i = 0; i < descriFun.length; i++) {
                                    val += descriFun.charAt(i).replace(" ", "_");
                                }
                                var mapeo = myMap.get(val);
                                var newstr = val;
                                if (mapeo === undefined) {
                                    value.descripFunc = newstr;
                                    $scope.funciones.push(value);
                                }
                            });
//                            $("#listBoxModificar").fadeIn("slow");
                        }).error(function (e) {
                            swal("Mensaje del Sistema", "Haz cancelado la operación!", "error");
                        });
                    }
                };

                $scope.registrarUpdate = function () {

                    $scope.descripcion = $scope.rolModificar.descripcionRol;
                    if ($scope.rolModificar.descripcionRol === "" || $scope.rolModificar.descripcionRol === undefined) {
                        swal("Mensaje del Sistema!", "Descripción no debe quedar vacío", "error");
                    } else {
                        //UPDATE ROL
                        if ($scope.rolModificar.descripcionRol.toUpperCase() !== $scope.rolModificar.descriRolMod.toUpperCase()) {
                            RolService.update($scope.rolModificar.idRol, $scope.rolModificar.descripcionRol).success(function (data) {
                                if (data === true) {
//                                    swal("Mensaje del Sistema!", "Datos Actualizados exitosamente", "success");
                                }
                            }).error(function (e) {
                                swal("Mensaje del Sistema!", "Datos no Agregados", "error");
                            });
                        }

                        RolFuncionService.deleteByIdRol($scope.rolModificar.idRol).success(function (dat) {
                            if (dat === true) {
                                var element = $('#listSeleccionadoUpdate li').length; //obtienes el total de elementos dentro del tag ul con clase caja
                                $("#listSeleccionadoUpdate li").each(function (index) {
                                    var valorSeleccionado = $.trim($(this).text());

                                    var val = "";
                                    for (var i = 0; i < valorSeleccionado.length; i++) {
                                        val += valorSeleccionado.charAt(i).replace(" ", "_");
                                    }

                                    var id = $("#datoUpdate" + val).val();
                                    $scope.rolFuncion = {};
                                    $scope.rolFuncion.rol = {};
                                    $scope.rolFuncion.funcion = {};

                                    $scope.rolFuncion.rol.idRol = $scope.rolModificar.idRol;
                                    $scope.rolFuncion.funcion.idFuncion = id;
                                    RolFuncionService.crear($scope.rolFuncion).success(function (data) {
                                        console.log("DATOS INSERTADO EN ROL_FUNCION....");
                                        $scope.atrasite();
                                    }).error(function (e) {
                                        console.log("ERROR DE CONEXION CON EL SERVIDOR...");
                                    });
                                });
                                $scope.descripcionRol = "";
                                $scope.idModulo = 0;
                                $("#listBoxModificar").hide();
                                $("#listSeleccionadoUpdate li").each(function (index) {
                                    $(this).remove();
                                    if (index === 0) {
                                        swal("Mensaje del Sistema!", "Datos Actualizados exitosamente!!", "success");
//                                $scope.roles = {};
//                                $scope.atrasite();
                                    }
                                });
                                if (document.getElementById("listSeleccionadoUpdate").getElementsByTagName("li").length === 0) {
                                    swal("Mensaje del Sistema!", "Datos Actualizados exitosamente!", "success");
//                            $("#menuRol").show();
//                            $("#addRol").hide();
//                            $("#updateRol").hide();
//                            $scope.roles = {};
//                            $scope.atrasite();
                                }
//                        FIN UPDATE ROL
                            }
                            console.log("DATOS ELIMINADOS DE ROL_FUNCION....");
                        }).error(function (e) {
                            console.log("ERROR....");
                        });
                    }
                };

                //FIN MODIFICAR
                $scope.eliminar = function (roles) {
                    swal({
                        title: "Mensaje del Sistema",
                        text: "Seguro que quiere eliminar el rol " + roles.descripcionRol + "?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    RolFuncionService.deleteByIdRol(roles.idRol).success(function (data) {
                                        RolService.eliminar(roles.idRol).success(function (data) {
//                                    RolService.deleteUpdate(roles.idRol).success(function (data) {
                                            if (data === true) {
                                                swal('Eliminado!',
                                                        'Haz eliminado exitosamente.',
                                                        'success'
                                                        );
                                                $scope.buscar();
                                            } else {
                                                swal("Mensaje del Sistema!", "No se pudo eliminar el Rol, verifique Dependencias", "error");
                                            }
                                        }).error(function (e) {
                                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                        });
                                    }).error(function (e) {
                                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                    });

                                } else {
                                    swal("Mensaje del Sistema", "Haz cancelado la operación!", "error");
                                }
                            });
                };
                $scope.$watch('filterText', function () {
                    $("#paginaActual").val("1");
                });
                $scope.buscar();

                $(function () {

                    $('#listBox').on('click', '.list-group .list-group-item', function () {
                        $(this).toggleClass('active');
                    });
//                    $('#listBoxModificar').on('click', '.list-group .list-group-item', function () {
//                        $(this).toggleClass('active');
//                    });
                    $('.list-arrows button').click(function () {
                        var $button = $(this), actives = '';
                        if ($button.hasClass('move-left')) {
                            actives = $('.list-right ul li.active');
//                            actives.clone().appendTo('.list-left ul');
                            actives.remove();
                        } else
                        if ($button.hasClass('move-right')) {
                            actives = $('.list-left ul li.active');
                            actives.clone().appendTo('.list-right ul');
                            actives.remove();
                        }
                    });
                    $('.dual-list .selector').click(function () {
                        var $checkBox = $(this);
                        if (!$checkBox.hasClass('selected')) {
                            $checkBox.addClass('selected').closest('.well').find('ul li:not(.active)').addClass('active');
                            $checkBox.children('i').removeClass('glyphicon-unchecked').addClass('glyphicon-check');
                        } else {
                            $checkBox.removeClass('selected').closest('.well').find('ul li.active').removeClass('active');
                            $checkBox.children('i').removeClass('glyphicon-check').addClass('glyphicon-unchecked');
                        }
                    });
                    $('[name="SearchDualList"]').keyup(function (e) {
                        var code = e.keyCode || e.which;
                        if (code === '9')
                            return;
                        if (code === '27')
                            $(this).val(null);
                        var $rows = $(this).closest('.dual-list').find('.list-group li');
                        var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
                        $rows.show().filter(function () {
                            var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
                            return !~text.indexOf(val);
                        }).hide();
                    });

                });
            }])
