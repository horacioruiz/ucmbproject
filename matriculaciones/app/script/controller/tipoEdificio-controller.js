'use strict'
app
        .controller('listTipoEdificioCtrl', ['$state', '$scope', 'config', 'TipoEdificioService', '$cookies', '$base64', '$http', function ($state, $scope, config, TipoEdificioService, $cookies, $base64, $http) {
//                console.log("HOLAAA");

                $scope.tipoEdificio = {};
                $scope.tipoEdificioAgregar = {};
                $scope.tipoEdificioModificar = {};

                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
//                FIN DE PAGINACION

                $scope.listar = function () {
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    TipoEdificioService.fetch(config.cantPaginacion, offSet).success(function (data) {
                        $scope.tipoEdificio = data;
                        $scope.paginar();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.paginar = function () {
                    TipoEdificioService.count().success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    TipoEdificioService.countFiltro($scope.filt).success(function (data) {
                        $scope.catidad = data;

                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.listarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }

                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    TipoEdificioService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt).success(function (data) {
                        $scope.tipoEdificio = data;
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }

                    $http.get(config.backend + '/tipoedificio/generarReporte/' + $scope.filt + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                }

                $scope.buscar = function () {
                    $scope.tipoEdificio = [];
                    if ($scope.filterText !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
//                    }
                };
                $scope.buscarSegunPagina = function () {
                    $scope.tipoEdificio = [];
                    if ($scope.filterText !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };

                $scope.registrar = function () {
//                    alert($scope.rolAgregar.descripcionRolAgregar);
                    if ($scope.tipoEdificioAgregar.descriTipoEdi === "" || $scope.tipoEdificioAgregar.descriTipoEdi === undefined || $scope.tipoEdificioAgregar.descriTipoEdi === null) {
                        swal("Mensaje del Sistema!", "Los datos no deben quedar vacío", "error");
                    } else {
                        $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                        $scope.tipoEdificioAgregar.usuAltaTipoEdi = $scope.jsonUsu.usuarioUsuario;
                        $scope.tipoEdificioAgregar.usuModTipoEdi = $scope.jsonUsu.usuarioUsuario;
                        TipoEdificioService.crear($scope.tipoEdificioAgregar).success(function (data) {
                            if (data === true) {
                                swal("Mensaje del Sistema!", "Datos Agregados exitosamente", "success");
                                $('#modalAgregar').modal('hide');
                                $scope.tipoEdificioAgregar = {};
                                $scope.listar();
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Agregados", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };

                $scope.atras = function () {
                    location.reload();
                };

                $scope.agregar = function () {
                    $scope.tipoEdificioAgregar = {};
                };

                $scope.actualizar = function () {
//                    alert($scope.rolAgregar.descripcionRolAgregar);
                    if ($scope.tipoEdificioModificar.descriTipoEdi === "" || $scope.tipoEdificioModificar.descriTipoEdi === undefined || $scope.tipoEdificioModificar.descriTipoEdi === null) {
                        swal("Mensaje del Sistema!", "Los datos no deben quedar vacío", "error");
                    } else {
                        $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                        $scope.tipoEdificioModificar.usuModTipoEdi = $scope.jsonUsu.usuarioUsuario;
                        TipoEdificioService.actualizar($scope.tipoEdificioModificar).success(function (data) {
                            if (data === true) {
                                swal("Mensaje del Sistema!", "Datos Modificados exitosamente", "success");
                                $('#modalModificar').modal('hide');
                                $scope.tipoEdificioModificar = {};
                                $scope.listar();
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Modificados", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };

                $scope.modificar = function (tipoEdificio) {
                    $scope.tipoEdificioModificar.descriTipoEdi = tipoEdificio.descriTipoEdi;
                    $scope.tipoEdificioModificar.idTipoEdi = tipoEdificio.idTipoEdi;
                    $("#modalModificar").modal("show");
                };

//                $scope.eliminar = function (marc) {
//
//                    $scope.datoEliminar = {};
//                    $scope.datoEliminar.idTipoEdi = marc.idTipoEdi;
//
//                    var r = confirm("Seguro que quiere eliminar " + marc.descriTipoEdi + "?");
//                    if (r === true) {
//                        TipoEdificioService.modEliminar($scope.datoEliminar).success(function (data) {
//                            if (data === true) {
//                                swal("Mensaje del Sistema!", "Datos Modificados exitosamente", "success");
////                                $('#modalModificar').modal('hide');
//                                $scope.tipoEdificioModificar = {};
//                                $scope.listar();
//                            } else {
//                                swal("Mensaje del Sistema!", "Datos no Modificados", "error");
//                            }
//                        }).error(function (e) {
//                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
//                        });
//                    } else {
//                        swal('Cancelado',
//                                'Haz cancelado la eliminación',
//                                'error'
//                                );
//                    }
//
//                };

                $scope.eliminar = function (marc) {
                    $scope.datoEliminar = {};
                    $scope.datoEliminar.idTipoEdi = marc.idTipoEdi;
                    $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                    $scope.datoEliminar.usuModTipoEdi = $scope.jsonUsu.usuarioUsuario;
                    swal({
                        title: "Mensaje del Sistema",
                        text: "Seguro que quiere eliminar " + marc.descriTipoEdi + "?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    TipoEdificioService.eliminar(marc.idTipoEdi).success(function (data) {
//                                    TipoEdificioService.modEliminar($scope.datoEliminar).success(function (data) {
                                        if (data === true) {
                                            swal("Mensaje del Sistema!", "Datos Modificados exitosamente", "success");
//                                $('#modalModificar').modal('hide');
                                            $scope.tipoEdificioModificar = {};
                                            $scope.listar();
                                        } else {
                                            swal("Mensaje del Sistema!", "No se pudo eliminar el Tipo Edificio, verifique Dependencias", "error");
                                        }
                                    }).error(function (e) {
                                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                    });
                                } else {
                                    swal("Mensaje del Sistema", "Haz cancelado la operación!", "error");
                                }
                            });
                };

                $scope.listar();
            }]);